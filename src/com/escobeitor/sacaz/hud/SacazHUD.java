package com.escobeitor.sacaz.hud;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.util.adt.color.Color;

import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.manager.ResourceManager;
import com.escobeitor.sacaz.model.game.Player;

/**
 * Sacaz in-game HUD
 * @author Escobeitor
 *
 */
public class SacazHUD extends HUD implements OnClickListener {
	
	/**
	 * Finish turn button tag
	 */
	private static final int BUTTON_FINISH_TURN = 0;

	/**
	 * Create new character button tag
	 */
	private final int BUTTON_NEW_CHARACTER = 1;
	
	/**
	 * Attack with melee weapon button tag
	 */
	private final int BUTTON_MELEE_ATTACK = 2;
	
	/**
	 * Attack with firearm button tag
	 */
	private final int BUTTON_FIREARM_ATTACK = 3;
	
	/**
	 * Current player
	 */
	final private Player localPlayer = GameContextHolder.getInstance().game.getLocalPlayer();
	
	/**
	 * Finish turn button sprite
	 */
	private ButtonSprite finishTurnButton;
	
	/**
	 * Create new character button sprite
	 */
	private ButtonSprite newCharacterButton;
	
	/**
	 * Attack with melee weapon button
	 */
	private ButtonSprite attackMeleeButton;
	
	/**
	 * Attack with firearm button
	 */
	private ButtonSprite attackFirearmButton;

	/**
	 * Load the HUD
	 */
	public void loadHUD() {
		ResourceManager.loadGameHUD();
		final float positionY = ResourceManager.getInstance().cameraHeight - 20;
		float initialX = 10f;
		
		ResourceManager.hudAPLabel.setPosition(initialX, 5f);
		ResourceManager.hudAPLabel.setAnchorCenter(0, 0);
		this.attachChild(ResourceManager.hudAPLabel);
		
		ResourceManager.hudAP.setText(Integer.toString(localPlayer.actionPoints));
		ResourceManager.hudAP.setAnchorCenter(0, 0);
		ResourceManager.hudAP.setPosition(ResourceManager.hudAPLabel.getX() + ResourceManager.hudAPLabel.getWidth() + 5f, 5f);
		this.attachChild(ResourceManager.hudAP);
		
		ResourceManager.hudTurnLabel.setPosition(ResourceManager.hudAP.getX() + ResourceManager.hudAP.getWidth() + 25f, 5f);
		ResourceManager.hudTurnLabel.setAnchorCenter(0, 0);
		this.attachChild(ResourceManager.hudTurnLabel);
		
		ResourceManager.hudTurn.setAnchorCenter(0, 0);
		ResourceManager.hudTurn.setPosition(ResourceManager.hudTurnLabel.getX() + ResourceManager.hudTurnLabel.getWidth() + 5f, 5f);
		this.attachChild(ResourceManager.hudTurn);
		
		ResourceManager.hudFoodLabel.setPosition(initialX, positionY);
		ResourceManager.hudFoodLabel.setAnchorCenter(0, 0);
		this.attachChild(ResourceManager.hudFoodLabel);
		
		ResourceManager.hudFood.setText(Integer.toString(localPlayer.food));
		ResourceManager.hudFood.setAnchorCenter(0, 0);
		ResourceManager.hudFood.setPosition(ResourceManager.hudFoodLabel.getX() + ResourceManager.hudFoodLabel.getWidth() + 5f, positionY);
		this.attachChild(ResourceManager.hudFood);
		
		ResourceManager.hudElectricityLabel.setPosition(ResourceManager.hudFood.getX() + ResourceManager.hudFood.getWidth() + 25f, positionY);
		ResourceManager.hudElectricityLabel.setAnchorCenter(0, 0);
		this.attachChild(ResourceManager.hudElectricityLabel);
		
		ResourceManager.hudElectricity.setText(Integer.toString(localPlayer.electricity));
		ResourceManager.hudElectricity.setAnchorCenter(0, 0);
		ResourceManager.hudElectricity.setPosition(ResourceManager.hudElectricityLabel.getX() + ResourceManager.hudElectricityLabel.getWidth() + 5f, positionY);
		this.attachChild(ResourceManager.hudElectricity);
		
		ResourceManager.hudMaterialsLabel.setPosition(ResourceManager.hudElectricity.getX() + ResourceManager.hudElectricity.getWidth() + 25f, positionY);
		ResourceManager.hudMaterialsLabel.setAnchorCenter(0, 0);
		this.attachChild(ResourceManager.hudMaterialsLabel);
		
		ResourceManager.hudMaterials.setText(Integer.toString(localPlayer.materials));
		ResourceManager.hudMaterials.setAnchorCenter(0, 0);
		ResourceManager.hudMaterials.setPosition(ResourceManager.hudMaterialsLabel.getX() + ResourceManager.hudMaterialsLabel.getWidth() + 5f, positionY);
		this.attachChild(ResourceManager.hudMaterials);
		
		ResourceManager.hudWaterLabel.setPosition(ResourceManager.hudMaterials.getX() + ResourceManager.hudMaterials.getWidth() + 25f, positionY);
		ResourceManager.hudWaterLabel.setAnchorCenter(0, 0);
		this.attachChild(ResourceManager.hudWaterLabel);
		
		ResourceManager.hudWater.setText(Integer.toString(localPlayer.water));
		ResourceManager.hudWater.setAnchorCenter(0, 0);
		ResourceManager.hudWater.setPosition(ResourceManager.hudWaterLabel.getX() + ResourceManager.hudWaterLabel.getWidth() + 5f, positionY);
		this.attachChild(ResourceManager.hudWater);
		
		newCharacterButton = new ButtonSprite(5f, 150f, ResourceManager.hudCreateCharacterButton, ResourceManager.getInstance().engine.getVertexBufferObjectManager(), this);
		newCharacterButton.setTag(BUTTON_NEW_CHARACTER);
		newCharacterButton.setAnchorCenter(0, 0);
		
		this.registerTouchArea(newCharacterButton);

		finishTurnButton = new ButtonSprite(5f, 200f, ResourceManager.hudFinishTurnButton, ResourceManager.getInstance().engine.getVertexBufferObjectManager(), this);
		finishTurnButton.setTag(BUTTON_FINISH_TURN);
		finishTurnButton.setAnchorCenter(0, 0);
		
		this.registerTouchArea(finishTurnButton);
		
		attackMeleeButton = new ButtonSprite(5f, 100f, ResourceManager.hudAttackMeleeButton, ResourceManager.getInstance().engine.getVertexBufferObjectManager(), this);
		attackMeleeButton.setTag(BUTTON_MELEE_ATTACK);
		attackMeleeButton.setAnchorCenter(0, 0);
		
		this.registerTouchArea(attackMeleeButton);
		
		attackFirearmButton = new ButtonSprite(5f, 50f, ResourceManager.hudAttackFirearmButton, ResourceManager.getInstance().engine.getVertexBufferObjectManager(), this);
		attackFirearmButton.setTag(BUTTON_FIREARM_ATTACK);
		attackFirearmButton.setAnchorCenter(0, 0);
		
		this.registerTouchArea(attackFirearmButton);
		
		this.setTouchAreaBindingOnActionDownEnabled(true);
		
		updateTurnControls();
	}
	
	/**
	 * Unload the HUD
	 */
	public void unloadHUD() {
		ResourceManager.unloadGameHUD();
		detachChildren();
		detachSelf();
		dispose();
	}

	/**
	 * Update HUD controls depending on the current
	 * turn owner
	 */
	public void updateTurnControls() {
		final boolean isLocalPlayer = GameContextHolder.getInstance().game.isLocalPlayerTurn();
		ResourceManager.hudTurn.setText(GameContextHolder.getInstance().game.getCurrentTurnPlayer().username);
		
		if(isLocalPlayer) {
			ResourceManager.hudTurn.setColor(Color.BLUE);
			this.attachChild(newCharacterButton);
			this.attachChild(finishTurnButton);
			this.attachChild(attackFirearmButton);
			this.attachChild(attackMeleeButton);
		} else {
			ResourceManager.hudTurn.setColor(Color.RED);
			this.detachChild(newCharacterButton);
			this.detachChild(finishTurnButton);
			this.detachChild(attackMeleeButton);
			this.detachChild(attackFirearmButton);
		}
	}
	
	/**
	 * @see org.andengine.entity.sprite.ButtonSprite.OnClickListener#onClick(org.andengine.entity.sprite.ButtonSprite, float, float)
	 */
	@Override
	public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX, float pTouchAreaLocalY) {
		
		boolean isLocalPlayer = GameContextHolder.getInstance().game.isLocalPlayerTurn();
		
		//New character action
		if(pButtonSprite.getTag() == BUTTON_NEW_CHARACTER && isLocalPlayer) {
			localPlayer.createCharacter();
		}
		
		//Finish turn action
		if(pButtonSprite.getTag() == BUTTON_FINISH_TURN && isLocalPlayer) {
			GameContextHolder.getInstance().game.finishTurn();
		}
		
		//Attack action
		if((pButtonSprite.getTag() == BUTTON_MELEE_ATTACK || pButtonSprite.getTag() == BUTTON_FIREARM_ATTACK) && isLocalPlayer) {
			if(GameContextHolder.getInstance().game.selectedCharacter == null) {
				AndroidApplicationContextHolder.toastOnUiThread(AndroidApplicationContextHolder.getInstance().getContext().getString(R.string.error_no_character_selected), Toast.LENGTH_LONG);
				localPlayer.isAttacking = false;
				return;
			}
			
			if(localPlayer.isAttacking) {
				localPlayer.isAttacking = false;
				return;
			}
			
			localPlayer.isAttacking = true;
			localPlayer.isAttackingWithFirearm = pButtonSprite.getTag() == BUTTON_FIREARM_ATTACK;
		}
	}

}
