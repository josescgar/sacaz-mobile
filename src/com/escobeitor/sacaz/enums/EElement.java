package com.escobeitor.sacaz.enums;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import com.escobeitor.sacaz.exceptions.GameConfigurationInitializationException;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.json.JSONFileUtils;

/**
 * Buildable elements available
 * @author Escobeitor
 *
 */
/**
 * @author Escobeitor
 *
 */
public enum EElement {
	
	KURTZ(EElementType.CHARACTER),
	
	BRODY(EElementType.CHARACTER),
	
	CLEMENCE(EElementType.CHARACTER),
	
	MICKEY(EElementType.CHARACTER),
	
	RICHARD(EElementType.CHARACTER),
	
	MCCLANE(EElementType.CHARACTER),
	
	SAMMY(EElementType.CHARACTER),
	
	RICK(EElementType.CHARACTER),
	
	CHARLIE(EElementType.CHARACTER),
	
	ELISABETH(EElementType.CHARACTER),
	
	FOOD(EElementType.ITEM),
	
	WATER(EElementType.ITEM),
	
	MATERIALS(EElementType.ITEM),
	
	ENERGYCELL(EElementType.ITEM),
	
	MORTAR(EElementType.ITEM),
	
	SNIPER(EElementType.ITEM),
	
	REPAIRKIT(EElementType.ITEM),
	
	BOOBYTRAP(EElementType.ITEM),
	
	SURGERY(EElementType.ITEM),
	
	STRIKEFORCE(EElementType.ITEM),
	
	HANDGUN(EElementType.WEAPON_FIREARM),
	
	SHOTGUN(EElementType.WEAPON_FIREARM),
	
	RIFLE22(EElementType.WEAPON_FIREARM),
	
	MACHINEGUN(EElementType.WEAPON_FIREARM),
	
	KNIFE(EElementType.WEAPON_MELEE),
	
	BASEBALLBAT(EElementType.WEAPON_MELEE),
	
	MACHETE(EElementType.WEAPON_MELEE),
	
	CHAINSAW(EElementType.WEAPON_MELEE),
	
	ZOMBIEBOB(EElementType.NPC),
	
	ZOMBIELOHAN(EElementType.NPC),
	
	ZOMBIEROB(EElementType.NPC),
	
	ZOMBIEANNE(EElementType.NPC),
	
	NONE(EElementType.ITEM);
	
	/**
	 * Element type
	 */
	public EElementType type;
	
	/**
	 * Element faction (for characters)
	 */
	public EFaction faction;
	
	/**
	 * Additional element properties
	 */
	public Map<String, String> properties;
	
	/**
	 * Electricity resource cost
	 */
	public int electricityCost = 0;
	
	/**
	 * Food resource cost
	 */
	public int foodCost = 0;
	
	/**
	 * Materials resource cost
	 */
	public int materialsCost = 0;
	
	/**
	 * Water resource cost
	 */
	public int waterCost = 0;
	
	/**
	 * Constructor for elements without faction
	 * @param type
	 */
	private EElement(EElementType type) {
		this.type = type;
	}
	
	/**
	 * Returns a random NPC element
	 * @return
	 */
	public static EElement getRandomNPC() {
		List<EElement> zombies = EElement.getElemetsForType(EElementType.NPC);
		int index = new Random().nextInt(zombies.size());
		return zombies.get(index);
	}
	
	/**
	 * Get a string representing the cost of the element
	 * @return
	 */
	public String getCostString() {
		StringBuffer sb = new StringBuffer("F: ").append(this.foodCost)
				.append(" E: ").append(this.electricityCost)
				.append(" M: ").append(this.materialsCost)
				.append(" W: ").append(this.waterCost);
		return sb.toString();
	}
	
	/**
	 * Get elements of a given type
	 * @param type
	 * @return
	 */
	public static List<EElement> getElemetsForType(EElementType type) {
		List<EElement> elements = new ArrayList<EElement>();
		elements.add(EElement.NONE);
		for(EElement element : values()) {
			if(type.equals(element.type)) {
				elements.add(element);
			}
		}
		
		return elements;
	}
	
	/**
	 * Get character elements of the given faction
	 * @param faction
	 * @return
	 */
	public static List<EElement> getCharactersByFaction(EFaction faction) {
		List<EElement> elements = new ArrayList<EElement>();
		for(EElement element : values()) {
			if(faction.equals(element.faction)) {
				elements.add(element);
			}
		}

		return elements;
	}

	/**
	 * Initialize weapons values
	 * @throws GameConfigurationInitializationException 
	 */
	public static void initializeWeapons() throws GameConfigurationInitializationException {

		JSONObject weapon, properties, cost, root;
		JSONArray weapons;
		
		try {
			InputStream weaponsFile = AndroidApplicationContextHolder.getInstance().getContext().getAssets().open(GameConfiguration.ASSETS_CONFIG_WEAPONS_FILE_PATH);
			root = JSONFileUtils.getJSONFromFile(weaponsFile);
			weaponsFile.close();
			if(root == null) {
				throw new Exception();
			}
			
			weapons = root.getJSONArray("weapons");
			
			EElement element;
			for(int i = 0; i < weapons.length(); i++) {
				weapon = weapons.getJSONObject(i);
				
				element = valueOf(weapon.getString("constant"));
				element.properties = new HashMap<String, String>();
				element.properties.put("firearm", weapon.getString("firearm"));
				element.properties.put("name", weapon.getString("name"));
				
				properties = weapon.getJSONObject("properties");
				element.properties.put("damage", properties.getString("damage"));
				element.properties.put("accuracy", properties.getString("accuracy"));
				element.properties.put("ammo", properties.getString("ammo"));
				
				cost = weapon.getJSONObject("cost");
				element.electricityCost = cost.getInt("electricity");
				element.materialsCost = cost.getInt("materials");
			}
		} catch (Exception e) {
			throw new GameConfigurationInitializationException();
		} finally {
			//Free objects
			root = null;
			weapons = null;
			weapon = null;
			properties = null;
			cost = null;
		}
	}
	
	/**
	 * Initialize characters values
	 * @param charactersFile
	 * @throws GameConfigurationInitializationException
	 */
	public static void initializeCharacters() throws GameConfigurationInitializationException {
		
		JSONObject root, character, cost, skills;
		JSONArray characters;
		try {
			InputStream charactersFile = AndroidApplicationContextHolder.getInstance().getContext().getAssets().open(GameConfiguration.ASSETS_CONFIG_CHARACTERS_FILE_PATH);
			root = JSONFileUtils.getJSONFromFile(charactersFile);
			charactersFile.close();
			
			if(root == null) {
				throw new Exception();
			}
			
			characters = root.getJSONArray("characters");
			EElement element;
			for(int i = 0; i < characters.length(); i++) {
				character = characters.getJSONObject(i);
				element = EElement.valueOf(character.getString("constant"));
				element.faction = EFaction.valueOf(character.getString("faction").toUpperCase());
				
				element.properties = new HashMap<String, String>();
				element.properties.put("name", character.getString("name"));
				element.properties.put("maximum", character.getString("max"));
				element.properties.put("description", character.getString("description"));
				element.properties.put("ability", character.getString("ability"));
				
				skills = character.getJSONObject("skills");
				element.properties.put("movement", skills.getString("movement"));
				element.properties.put("health", skills.getString("health"));
				element.properties.put("defense", skills.getString("defense"));
				element.properties.put("firearm", skills.getString("firearm"));
				element.properties.put("melee", skills.getString("melee"));
				
				cost = character.getJSONObject("cost");
				element.foodCost = cost.getInt("food");
				element.electricityCost = cost.getInt("electricity");
				element.materialsCost = cost.getInt("materials");
				element.waterCost = cost.getInt("water");
			}
		} catch(Exception e) {
			throw new GameConfigurationInitializationException();
		} finally {
			//Free objects
			root = null;
			characters = null;
			character = null;
			cost = null;
			skills = null;
		}
	}
	
	/**
	 * Read items file
	 * @param itemFiles
	 * @throws GameConfigurationInitializationException
	 */
	public static void initializeItems() throws GameConfigurationInitializationException {
		JSONObject root, item;
		JSONArray items;
		try {
			InputStream itemFiles = AndroidApplicationContextHolder.getInstance().getContext().getAssets().open(GameConfiguration.ASSETS_CONFIG_ITEMS_FILE_PATH);
			root = JSONFileUtils.getJSONFromFile(itemFiles);
			itemFiles.close();
			
			if(root == null) {
				throw new Exception();
			}
			
			items = root.getJSONArray("items");
			EElement element;
			for(int i = 0; i < items.length(); i++) {
				item = items.getJSONObject(i);
				element = EElement.valueOf(item.getString("constant"));
				element.properties = new HashMap<String, String>();
				
				element.properties.put("name", item.getString("name"));
				element.properties.put("maxuses", item.getString("maxuses"));
				element.properties.put("description", item.getString("description"));
			}
		} catch(Exception e) {
			throw new GameConfigurationInitializationException();
		} finally {
			//Free objects
			root = null;
			item = null;
			items = null;
		}
	}
	
	/**
	 * Read items file
	 * @param itemFiles
	 * @throws GameConfigurationInitializationException
	 */
	public static void initializeNPC() throws GameConfigurationInitializationException {
		JSONObject root, npc;
		JSONArray npcs;
		try {
			InputStream npcFiles = AndroidApplicationContextHolder.getInstance().getContext().getAssets().open(GameConfiguration.ASSETS_CONFIG_NPC_FILE_PATH);
			root = JSONFileUtils.getJSONFromFile(npcFiles);
			npcFiles.close();
			
			if(root == null) {
				throw new Exception();
			}
			
			npcs = root.getJSONArray("npcs");
			EElement element;
			for(int i = 0; i < npcs.length(); i++) {
				npc = npcs.getJSONObject(i);
				element = EElement.valueOf(npc.getString("constant"));

				element.properties = new HashMap<String, String>();
				JSONObject skills = npc.getJSONObject("skills");
				
				element.properties.put("movement", skills.getString("movement"));
				element.properties.put("health", skills.getString("health"));
				element.properties.put("defense", skills.getString("defense"));
				element.properties.put("melee", skills.getString("melee"));
			}
		} catch(Exception e) {
			throw new GameConfigurationInitializationException();
		} finally {
			//Free objects
			root = null;
			npc = null;
			npcs = null;
		}
	}
}
