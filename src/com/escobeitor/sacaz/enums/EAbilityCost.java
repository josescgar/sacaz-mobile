package com.escobeitor.sacaz.enums;

import java.io.InputStream;

import org.json.JSONArray;
import org.json.JSONObject;

import com.escobeitor.sacaz.exceptions.GameConfigurationInitializationException;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.json.JSONFileUtils;

/**
 * Abilities execution costs
 * @author Escobeitor
 *
 */
public enum EAbilityCost {
	
	INSTILL_FEAR,
	
	BRAG,
	
	PANIC_SCREAM,
	
	CHARISMATIC_BOMB,
	
	STAB,
	
	POLICE_CHARGE,
	
	DOMESTIC_ECONOMY,
	
	TRUESHOT,
	
	HANDYMAN,
	
	ACCOUNTING,
	
	THERMAL_ENERGY,
	
	SURGERY,
	
	REPAIR_KIT,
	
	HUMIDITY,
	
	SNIPER,
	
	DEACTIVATE,
	
	STRIKE_FORCE,
	
	BOOBY_TRAP,
	
	LIGTH_ENERGY,
	
	MORTAR,
	
	SHAKE_OFF,
	
	RANGER_SUCESS;
	
	/**
	 * Electricity resource cost
	 */
	public int electricityCost;
	
	/**
	 * Food resource cost
	 */
	public int foodCost;
	
	/**
	 * Materials resource cost
	 */
	public int materialsCost;
	
	/**
	 * Water resource cost
	 */
	public int waterCost;
	
	/**
	 * Read all abilities properties
	 * @param abilitiesFile
	 * @throws GameConfigurationInitializationException
	 */
	public static void initialize() throws GameConfigurationInitializationException {
		JSONObject root, ability, cost;
		JSONArray abilities;
		try {
			InputStream abilitiesFile = AndroidApplicationContextHolder.getInstance().getContext().getAssets().open(GameConfiguration.ASSETS_CONFIG_ABILITIES_FILE_PATH);
			root = JSONFileUtils.getJSONFromFile(abilitiesFile);
			abilitiesFile.close();
			
			if(root == null) {
				throw new Exception();
			}
			
			abilities = root.getJSONArray("abilities");
			EAbilityCost element;
			for(int i = 0; i < abilities.length(); i++) {
				ability = abilities.getJSONObject(i);
				element = EAbilityCost.valueOf(ability.getString("constant"));
				
				cost = ability.getJSONObject("cost");
				element.foodCost = cost.getInt("food");
				element.electricityCost = cost.getInt("electricity");
				element.materialsCost = cost.getInt("materials");
				element.waterCost = cost.getInt("water");
			}
		} catch(Exception e) {
			throw new GameConfigurationInitializationException();
		} finally {
			//Free objects
			root = null;
			ability = null;
			abilities = null;
			cost = null;
		}
	}
}
