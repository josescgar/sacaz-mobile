package com.escobeitor.sacaz.enums;

/**
 * Possible results for a game
 * @author Escobeitor
 *
 */
public enum EGameResult {

	LOST,
	
	WON,
	
	RESIGNED;
}
