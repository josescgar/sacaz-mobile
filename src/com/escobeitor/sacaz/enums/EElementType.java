package com.escobeitor.sacaz.enums;

/**
 * Types of buildable elements
 * @author Escobeitor
 *
 */
public enum EElementType {
	
	WEAPON_FIREARM,
	
	WEAPON_MELEE,
	
	ITEM,
	
	NPC,
	
	CHARACTER;
}
