package com.escobeitor.sacaz.enums;

/**
 * Available game actions for logging purposes
 * @author Escobeitor
 *
 */
public enum EActionTypes {
	
	NEW_CHARACTER,
	
	NEW_NPC,
	
	NPC_STRIKE,
	
	ENERGY_DISTRIBUTION,
	
	RANGER_RESOURCES,
	
	CHARACTER_MOVEMENT,
	
	NPC_MOVEMENT,
	
	PICK_UP_OBJECT,
	
	STRIKE,
	
	RANGER_SWITCH,
	
	GAME_FINISHED,
	
	TURN_FINISHED,
	
	RANGER_RAFFLE,
	
	ABILITY_USED;
}
