package com.escobeitor.sacaz.enums;

/**
 * Player available factions
 * @author Escobeitor
 *
 */
public enum EFaction {
	
	CRIMINALS,
	
	SURVIVORS;
}
