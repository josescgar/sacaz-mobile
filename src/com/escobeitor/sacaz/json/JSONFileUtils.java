package com.escobeitor.sacaz.json;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;

/**
 * Utility class for reading .json files
 * @author Escobeitor
 *
 */
public class JSONFileUtils {
	
	/**
	 * Reads an input file and returns a JSONObject with its content
	 * @param pathToFile
	 * @return the {@link JSONObject}
	 */
	public static JSONObject getJSONFromFile(InputStream file) {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(file));
		
		    String line;
		    StringBuffer results = new StringBuffer();
		    while( ( line = reader.readLine() ) != null)
		    {
		        results.append(line);
		    }
		    reader.close();

		    return new JSONObject(results.toString());
		    
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Writes a JSON object into internal storage
	 * @param fileAssetsPath
	 * @param json
	 * @return
	 */
	public static boolean storeJSONInFile(String fileName, JSONObject json) {
		try {			
			FileOutputStream fos = AndroidApplicationContextHolder.getInstance().getContext().openFileOutput(fileName, Context.MODE_PRIVATE);
			fos.write(json.toString().getBytes());
			fos.close();
			return true;
		} catch(Exception e) {
			Log.d(GameConfiguration.TAG_ERROR, "Error storing game file: " + e.getMessage());
			return false;
		}
	}
}
