package com.escobeitor.sacaz.scene;

import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.enums.ELevel;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.global.GameObjectParser;
import com.escobeitor.sacaz.hud.SacazHUD;
import com.escobeitor.sacaz.json.JSONFileUtils;
import com.escobeitor.sacaz.logging.Log;
import com.escobeitor.sacaz.logging.LogProcessor;
import com.escobeitor.sacaz.manager.ResourceManager;
import com.escobeitor.sacaz.manager.SceneManager;
import com.escobeitor.sacaz.manager.managed.ManagedGameScene;
import com.escobeitor.sacaz.model.Game;
import com.escobeitor.sacaz.model.game.Character;
import com.escobeitor.sacaz.model.game.Generator;
import com.escobeitor.sacaz.model.game.NPC;
import com.escobeitor.sacaz.network.NetworkConfiguration;
import com.escobeitor.sacaz.network.StatusCode;
import com.escobeitor.sacaz.network.service.LogSvc;
import com.google.gson.GsonBuilder;

/**
 * Game level scene
 * @author Escobeitor
 *
 */
public class GameLevel extends ManagedGameScene {
	
	/**
	 * Game filename
	 */
	private String filename;
	
	/**
	 * Referecen to game HUD
	 */
	private SacazHUD hud;
	
	/**
	 * Flag to indicate if the scene ended with errors
	 */
	private boolean errorCondition = false;
	
	/**
	 * Game context
	 */
	private GameContextHolder gameContext = GameContextHolder.getInstance();
	
	/**
	 * Constructor. Retrieve and build game data.
	 * @param gameId
	 * @param level
	 * @param filename
	 */
	public GameLevel(String gameId, ELevel level, String filename) {
		super(1f);
		gameContext.game = new Game();
		gameContext.game.id = gameId;
		gameContext.game.map = level;
		this.filename = filename;
		gameContext.gameLevelScene = this;
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedGameScene#onLoadScene()
	 */
	@Override
	public void onLoadScene() {
		ResourceManager.loadGameResources();
		ResourceManager.loadGameConfigAssets();
		
		ResourceManager.gameMap.setOffsetCenter(0f, 0.5f);

		GameObjectParser.parseGameObject(filename);
		
		hud = new SacazHUD();
		hud.loadHUD();
		gameHud = hud;
		
		
		//Retrieve local log
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(AndroidApplicationContextHolder.getInstance().getContext());
		StringBuffer logName = new StringBuffer(gameContext.game.id).append(GameConfiguration.LOCAL_LOG_FILE_SUFFIX);
		String log = prefs.getString(logName.toString(), null);
		if(log != null) {
			gameContext.game.gameLog = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().fromJson(log, Log.class);
		} else {
			gameContext.game.gameLog = new Log();
		}
		
		placeCharacters();
		placeNPCs();
		placeGenerators();
		
		this.attachChild(ResourceManager.gameMap);
		
		this.setOnSceneTouchListener(new GameLevelSceneTouchedListener());
		
		super.onLoadScene();
	}


	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedGameScene#onShowScene()
	 */
	@Override
	public void onShowScene() {
		//If the registered turn belongs to the opponent, check if there is a log available
		if(!gameContext.game.currentTurn.equals(gameContext.user.id)) {
			LogSvc.getInstance().retrieveLog(gameContext.game.id, this, "retrieveLogCallback");
		}
		
		super.onShowScene();
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedGameScene#onUnloadScene()
	 */
	@Override
	public void onUnloadScene() {
		ResourceManager.unloadGameResources();
		
		if(!errorCondition) {
			//Save current log
			gameContext.game.gameLog.storeLocal();
			
			//Save current game state
			
			StringBuffer gameFileName = new StringBuffer(gameContext.game.id).append(GameConfiguration.GAME_FILE_EXTENSION);
			JSONFileUtils.storeJSONInFile(gameFileName.toString(), gameContext.gameJSON);
		}
		
		gameContext.game = null;
		gameContext.gameJSON = null;
		hud.unloadHUD();
		gameHud = null;
		ResourceManager.getInstance().camera.setHUD(null);
		gameContext.gameLevelScene = null;
		System.gc();
		super.onUnloadScene();
	}

	/**
	 * Player characters in the map
	 */
	private void placeCharacters() {
		
		for(int j = 0; j < gameContext.game.players.length; j++) {
			List<Character> characters = gameContext.game.players[j].characters;
			for(int i = 0; i < characters.size(); i++) {
				Character c = characters.get(i);
				c.setCurrentTileIndex(1);
				c.setAnchorCenter(0, 0);
				c.setPosition(c.position.x, c.position.y);
				
				ResourceManager.gameMap.attachChild(c);
				this.registerTouchArea(c);
			}
		}
	}

	/**
	 * Place NPCs in the map
	 */
	private void placeNPCs() {
		for(int i = 0; i < gameContext.game.npcs.size(); i++) {
			NPC npc = gameContext.game.npcs.get(i);
			npc.setCurrentTileIndex(1);
			npc.setAnchorCenter(0, 0);
			npc.setPosition(npc.position.x, npc.position.y);
			ResourceManager.gameMap.attachChild(npc);
			this.registerTouchArea(npc);
		}
	}

	/**
	 * Place generators in the map
	 */
	private void placeGenerators() {

		for(int j = 0; j < gameContext.game.players.length; j++) {
			List<Generator> generators = gameContext.game.players[j].generators;
			for(int i = 0; i < generators.size(); i++) {
				Generator g = generators.get(i);
				g.setAnchorCenter(0, 0);
				g.setPosition(g.position.x, g.position.y);
				ResourceManager.gameMap.attachChild(g);
				this.registerTouchArea(g);
			}
		}
	}
	
	/**
	 * Retrieve log callback. Start log processing.
	 * @param response
	 * @param failed
	 */
	public void retrieveLogCallback(JSONObject response, boolean failed) {
		Context context = AndroidApplicationContextHolder.getInstance().getContext();
		if(failed) {
			return;
		}
		
		try {
			
			if(response.getInt(NetworkConfiguration.RESPONSE_STATUS_KEY) != StatusCode.OK) {
				return;
			}
			
			final JSONObject payload = response.getJSONObject(NetworkConfiguration.RESPONSE_PAYLOAD_KEY);
			
			//Confirm that the log has been received successfuly 
			LogSvc.getInstance().confirmLog(payload.getJSONObject("_id").getString("$id"));
			
			LogProcessor.getInstance().process(payload);

			//Start player's new turn
			gameContext.game.startTurn();
			
		} catch(Exception e) {
			android.util.Log.e(GameConfiguration.TAG_ERROR, "An error occured parsing the log: " + e.getMessage());
			AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_processing_log), Toast.LENGTH_LONG);
			
			//Delete local game file
			errorCondition = true;
			StringBuffer gameFileName = new StringBuffer(gameContext.game.id).append(GameConfiguration.GAME_FILE_EXTENSION);
			context.deleteFile(gameFileName.toString());
			SceneManager.getInstance().showScene(new MyGames());
			
		}
	}
}
