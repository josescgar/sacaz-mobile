package com.escobeitor.sacaz.scene;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.activity.GameActivity;
import com.escobeitor.sacaz.enums.EFaction;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.global.LobbySearchResultCache;
import com.escobeitor.sacaz.json.JSONFileUtils;
import com.escobeitor.sacaz.manager.SceneManager;
import com.escobeitor.sacaz.manager.managed.ManagedGameScene;
import com.escobeitor.sacaz.network.NetworkConfiguration;
import com.escobeitor.sacaz.network.StatusCode;
import com.escobeitor.sacaz.network.service.GameSvc;
import com.escobeitor.sacaz.network.service.LobbySvc;
import com.escobeitor.sacaz.network.service.interfaces.ILobbySvc;

/**
 * Single lobby managment scene
 * @author Escobeitor
 *
 */
public class LobbyInfo extends ManagedGameScene {
	
	/**
	 * My games list view
	 */
	private View lobbyInfoView;
	
	/**
	 * Reference to self
	 */
	private LobbyInfo self;
	
	/**
	 * Lobby data
	 */
	private JSONObject lobby;
	
	/**
	 * Selected lobby id
	 */
	private String lobbyId;
	
	/**
	 * Lobby creator faction
	 */
	private String player1Faction;
	
	/**
	 * Context reference
	 */
	private final Context context = AndroidApplicationContextHolder.getInstance().getContext();
	
	/**
	 * Constructor with a lobby already fetched
	 * @param id
	 */
	public LobbyInfo(String id, boolean fromSearch) {
		super();
		self = this;
		lobby = fromSearch ? LobbySearchResultCache.getInstance().searchResult.get(id) : LobbySearchResultCache.getInstance().myLobbies.get(id);
	}
	
	/**
	 * Constructor retrieving lobby info from server
	 * @param id
	 */
	public LobbyInfo(String id) {
		super();
		self = this;
		LobbySvc.getInstance().getLobby(id, self, "refreshCallback");
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedGameScene#onLoadScene()
	 */
	@Override
	public void onLoadScene() {

		lobbyInfoView = ((GameActivity) context).getLayoutInflater().inflate(R.layout.lobby_info, null);
		
		try {

			generateLobbyInfo();
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error initializing lobby info: " + e.getMessage());
		}
		
		super.onLoadScene();
	}

	/**
	 * Generate the lobby info (user-related information)
	 * @throws JSONException
	 */
	private void generateLobbyInfo() throws JSONException {
		
		this.lobbyId = lobby.getJSONObject("_id").getString("$id");
		
		final TextView title = (TextView) lobbyInfoView.findViewById(R.id.lobby_info_title);
		title.setText(lobby.getString("title"));
		title.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/visitor2.ttf"));
		
		final TextView level = (TextView) lobbyInfoView.findViewById(R.id.lobby_info_level);
		level.setText(lobby.getString("map"));
		
		//Activate buttons depending in the current situation of the lobby
		initializeButtons();
		
		final TextView player1 = (TextView) lobbyInfoView.findViewById(R.id.lobby_info_player1);

		player1.setText(new StringBuffer(lobby.getJSONObject("player1").getString("username"))
			.append(' ').append(lobby.getJSONObject("player1").getString("faction")));
		
		this.player1Faction = lobby.getJSONObject("player1").getString("faction");
		
		final TextView player2 = (TextView) lobbyInfoView.findViewById(R.id.lobby_info_player2);
		player2.setText(null);
		if(lobby.has("player2")) {
			player2.setText(new StringBuffer(lobby.getJSONObject("player2").getString("username"))
				.append(' ').append(lobby.getJSONObject("player2").getString("faction")));
		}
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedGameScene#onShowScene()
	 */
	@Override
	public void onShowScene() {
		
		//Show the layout
		((GameActivity) context).runOnUiThread(new Runnable() {

			@Override
			public void run() {
				((GameActivity) context).addContentView(lobbyInfoView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			}
			
		});
		
		super.onShowScene();
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedGameScene#onUnloadScene()
	 */
	@Override
	public void onUnloadScene() {
		ViewGroup vg = (ViewGroup)(lobbyInfoView.getParent());
		vg.removeView(lobbyInfoView);
		self = null;
		System.gc();
		super.onUnloadScene();
	}
	
	/**
	 * Initialize the lobby buttons setting their visibility
	 * and actions
	 * @throws JSONException
	 */
	private void initializeButtons() throws JSONException {		
		final Button start = (Button) lobbyInfoView.findViewById(R.id.lobby_info_button_start);
		final Button leave = (Button) lobbyInfoView.findViewById(R.id.lobby_info_button_leave);
		final Button delete = (Button) lobbyInfoView.findViewById(R.id.lobby_info_button_delete);
		final Button refresh = (Button) lobbyInfoView.findViewById(R.id.lobby_info_button_refresh);
		final Button join = (Button) lobbyInfoView.findViewById(R.id.lobby_info_button_join);
		start.setVisibility(View.GONE);
		leave.setVisibility(View.GONE);
		delete.setVisibility(View.GONE);
		join.setVisibility(View.GONE);
		
		//Set buttons visibility
		final String player1Id = lobby.getJSONObject("player1").getString("userId");
		final String player2Id = lobby.has("player2") ? lobby.getJSONObject("player2").getString("userId") : null;
		final boolean isFull = lobby.has("player1") && lobby.has("player2");
		
		if(isFull && player1Id.equals(GameContextHolder.getInstance().user.id)) {
			start.setVisibility(View.VISIBLE);
		}
		
		if(isFull && player2Id != null && player2Id.equals(GameContextHolder.getInstance().user.id)) {
			leave.setVisibility(View.VISIBLE);
		}
		
		if(player1Id.equals(GameContextHolder.getInstance().user.id)) {
			delete.setVisibility(View.VISIBLE);
		}
		
		if(!isFull && !player1Id.equals(GameContextHolder.getInstance().user.id)) {
			join.setVisibility(View.VISIBLE);
		}
		
		//Set buttons actions
		final ILobbySvc lobbySvc = LobbySvc.getInstance();
		
		//REFRESH BUTTON
		refresh.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				lobbySvc.getLobby(lobbyId, self, "refreshCallback");
			}
		});
		
		//JOIN BUTTON
		join.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				lobbySvc.joinById(lobbyId, GameContextHolder.getInstance().user.username, 
						player1Faction.equals(EFaction.CRIMINALS.name()) ? EFaction.SURVIVORS.name() : EFaction.CRIMINALS.name(),
						self,
						"joinCallback");
			}
		});
		
		//DELETE BUTTON
		delete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				lobbySvc.deleteLobby(lobbyId, self, "deleteCallback");
			}
		});
		
		//LEAVE BUTTON
		leave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				lobbySvc.leaveLobby(lobbyId, self, "leaveCallback");
			}
		});
		
		//START BUTTON
		start.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				GameSvc.getInstance().create(lobbyId, self, "startCallback");
			}
		});
	}
	
	///////////////////////////
	//// CALLBACK METHODS ////
	/////////////////////////
	
	/**
	 * Refresh lobby callback
	 * @param response
	 * @param failed
	 */
	public void refreshCallback(JSONObject response, boolean failed) {
		if(failed) {
			return;
		}
		
		try {
			
			this.lobby = response.getJSONObject(NetworkConfiguration.RESPONSE_PAYLOAD_KEY);
			generateLobbyInfo();
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error refreshing lobby info: " + e.getMessage());
		}
	}
	
	/**
	 * Delete lobby callback
	 * @param response
	 * @param failed
	 */
	public void deleteCallback(JSONObject response, boolean failed) {
		if(failed) {
			return;
		}
		
		String message = null;
		try {
			if(response.getInt(NetworkConfiguration.RESPONSE_STATUS_KEY) != StatusCode.OK) {
				throw new Exception(response.getString(NetworkConfiguration.RESPONSE_MESSAGE_KEY));
			}
				
			SceneManager.getInstance().showMainMenu();
			message = context.getString(R.string.lobby_info_deleted_ok);
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error deleting lobby: " + e.getMessage());
			message = context.getString(R.string.error_deleting_lobby);
			
		} finally {
			AndroidApplicationContextHolder.toastOnUiThread(message, Toast.LENGTH_LONG);
		}
	}
	
	/**
	 * Leave lobby callback
	 * @param response
	 * @param failed
	 */
	public void leaveCallback(JSONObject response, boolean failed) {
		if(failed) {
			return;
		}
		
		String message = null;
		try {
			if(response.getInt(NetworkConfiguration.RESPONSE_STATUS_KEY) != StatusCode.OK) {
				throw new Exception(response.getString(NetworkConfiguration.RESPONSE_MESSAGE_KEY));
			}
			
			SceneManager.getInstance().showMainMenu();
			message = context.getString(R.string.lobby_info_leave_ok);

		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error leaving lobby: " + e.getMessage());
			message = context.getString(R.string.error_leaving_lobby);
			
		} finally {
			AndroidApplicationContextHolder.toastOnUiThread(message, Toast.LENGTH_LONG);
		}
	}
	
	/**
	 * Join lobby callback
	 * @param response
	 * @param failed
	 */
	public void joinCallback(JSONObject response, boolean failed) {
		if(failed) {
			return;
		}

		try {
			if(response.getInt(NetworkConfiguration.RESPONSE_STATUS_KEY) != StatusCode.OK) {
				throw new Exception(response.getString(NetworkConfiguration.RESPONSE_MESSAGE_KEY));
			} 
			
			this.lobby = response.getJSONObject(NetworkConfiguration.RESPONSE_PAYLOAD_KEY);
			
			generateLobbyInfo();
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error joining lobby: " + e.getMessage());
			AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_joining_lobby), Toast.LENGTH_LONG);	
		}
	}
	
	/**
	 * Start game callback
	 * @param response
	 * @param failed
	 */
	public void startCallback(JSONObject response, boolean failed) {
		if(failed) {
			return;
		}
		
		try {
			if(response.getInt(NetworkConfiguration.RESPONSE_STATUS_KEY) != StatusCode.OK) {
				throw new Exception(response.getString(NetworkConfiguration.RESPONSE_MESSAGE_KEY));
			} 
			
			JSONObject data = response.getJSONObject(NetworkConfiguration.RESPONSE_PAYLOAD_KEY);
			
			String gameId = data.getJSONObject("_id").getString("$id");
			StringBuffer filename = new StringBuffer(gameId)
				.append(GameConfiguration.GAME_FILE_EXTENSION);
			
			//Save the game file
			JSONFileUtils.storeJSONInFile(filename.toString(), data);
			
			SceneManager.getInstance().showScene(new MyGames());
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error starting the game: " + e.getMessage());
			AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_starting_game), Toast.LENGTH_LONG);
		}
	}
}
