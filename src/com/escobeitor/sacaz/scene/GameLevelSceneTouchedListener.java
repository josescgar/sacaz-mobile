package com.escobeitor.sacaz.scene;

import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.input.touch.TouchEvent;

import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.model.Game;
import com.escobeitor.sacaz.model.game.Position;

/**
 * Handle general touch events in the game scene
 * @author Escobeitor
 *
 */
public class GameLevelSceneTouchedListener implements IOnSceneTouchListener {

	/**
	 * @see org.andengine.entity.scene.IOnSceneTouchListener#onSceneTouchEvent(org.andengine.entity.scene.Scene, org.andengine.input.touch.TouchEvent)
	 */
	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		if(!pSceneTouchEvent.isActionDown()) {
			return false;
		}
		
		Game game = GameContextHolder.getInstance().game;
		
		//Movement action
		if(game.selectedCharacter != null && game.selectedCharacter.grid != null && !game.getLocalPlayer().isAttacking) {
			game.selectedCharacter.moveToPosition(new Position(pSceneTouchEvent.getX(), pSceneTouchEvent.getY()));
			return true;
		}
		
		return false;
	}

}
