package com.escobeitor.sacaz.scene;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.AutoParallaxBackground;
import org.andengine.entity.scene.background.ParallaxBackground.ParallaxEntity;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.activity.GameActivity;
import com.escobeitor.sacaz.enums.EFaction;
import com.escobeitor.sacaz.enums.ELevel;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.manager.ResourceManager;
import com.escobeitor.sacaz.manager.SceneManager;
import com.escobeitor.sacaz.manager.managed.ManagedMenuScene;
import com.escobeitor.sacaz.network.NetworkConfiguration;
import com.escobeitor.sacaz.network.StatusCode;
import com.escobeitor.sacaz.network.service.LobbySvc;

/**
 * Main menu class. Implemented as a singleton
 * to avoid having to instantiate it every time
 * the user wants to go back to the menu.
 * @author Escobeitor
 *
 */
/**
 * @author Escobeitor
 *
 */
public class MainMenu extends ManagedMenuScene {
	
	/**
	 * Singleton instance
	 */
	private static MainMenu instance = null;
	
	/**
	 * Make constructor private
	 */
	private MainMenu() {
		this.setOnSceneTouchListenerBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionMoveEnabled(true);
		this.setTouchAreaBindingOnActionDownEnabled(true);
	}
	
	/**
	 * Singleton method
	 * @return
	 */
	public static MainMenu getInstance() {
		if(instance == null) {
			instance = new MainMenu();
		}
		
		return instance;
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedScene#onLoadingScreenLoadAndShown()
	 */
	@Override
	public Scene onLoadingScreenLoadAndShown() {
		//No loading screen
		return null;
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedScene#onLoadingScreenUnloadAndHidden()
	 */
	@Override
	public void onLoadingScreenUnloadAndHidden() {
		//No loading screen
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedScene#onLoadScene()
	 */
	@Override
	public void onLoadScene() {
		ResourceManager.loadMenuResources();
		
		final Context context = AndroidApplicationContextHolder.getInstance().getContext();
		
		//Create menu moving background
		final AutoParallaxBackground parallax = new AutoParallaxBackground(0, 0, 0, 5);
		
		parallax.attachParallaxEntity(new ParallaxEntity(1f, 
				new Sprite(ResourceManager.getInstance().cameraWidth / 2,
					ResourceManager.getInstance().cameraHeight - (ResourceManager.menuBackgroundSky.getHeight() / 2) - 10,
					ResourceManager.menuBackgroundSky, 
					ResourceManager.getInstance().engine.getVertexBufferObjectManager())));
		
		Sprite buildings = new Sprite(0,
				0 + (ResourceManager.menuBackgroundBuildings.getHeight() / 2), 
				ResourceManager.menuBackgroundBuildings, 
				ResourceManager.getInstance().engine.getVertexBufferObjectManager());
		buildings.setAnchorCenterX(0);
		parallax.attachParallaxEntity(new ParallaxEntity(-0.5f, buildings));
		
		Sprite lamps = new Sprite(0,
				0 + (ResourceManager.menuBackgroundLamps.getHeight() / 2), 
				ResourceManager.menuBackgroundLamps, 
				ResourceManager.getInstance().engine.getVertexBufferObjectManager());
		lamps.setAnchorCenterX(0);
		parallax.attachParallaxEntity(new ParallaxEntity(-5f, lamps));
		
		parallax.setColor(0.1647f, 0.3803f, 0.6745f);
		
		this.setBackground(parallax);
		
		final AnimatedSprite zombie = new AnimatedSprite(340,
				(ResourceManager.menuBackgroundZombie.getHeight() / 2) + 28,
				ResourceManager.menuBackgroundZombie,
				ResourceManager.getInstance().engine.getVertexBufferObjectManager());
		
		zombie.animate(new long[]{300, 300, 300}, 6, 8, true);
		
		this.attachChild(zombie);
		
		//Create title
		Text title = new Text(15, 190, 
			ResourceManager.getInstance().defaultFont42White, 
			context.getString(R.string.main_menu_title), 
			ResourceManager.getInstance().engine.getVertexBufferObjectManager());
		title.setAnchorCenter(0,0);
		this.attachChild(title);
		
		Text subtitle = new Text(190, 165, 
			ResourceManager.getInstance().defaultFont42White, 
			context.getString(R.string.main_menu_subtitle), 
			ResourceManager.getInstance().engine.getVertexBufferObjectManager());
		subtitle.setAnchorCenter(0, 0);
		subtitle.setColor(0.35f, 0.52f, 0.15f);
		this.attachChild(subtitle);

		//Set the menu options
		int optionsStep = 60;
		int optionsY = 370;
		
		Text myGamesButton = new Text(ResourceManager.getInstance().cameraWidth - 180, optionsY, 
				ResourceManager.getInstance().defaultFont60White, 
				context.getString(R.string.main_menu_my_games), 
				ResourceManager.getInstance().engine.getVertexBufferObjectManager()) {
			
			@Override
	        public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					SceneManager.getInstance().showScene(new MyGames());
				}
				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
			}
		};
		
		optionsY -= optionsStep;
		Text createGameButton = new Text(ResourceManager.getInstance().cameraWidth - 180, optionsY, 
				ResourceManager.getInstance().defaultFont60White, 
				context.getString(R.string.main_menu_create_game), 
				ResourceManager.getInstance().engine.getVertexBufferObjectManager()) {
			
			@Override
	        public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					((GameActivity) context).runOnUiThread(new Runnable() {
						//Show a dialog for creating the new game
						public void run() {
							showCreateGameDialog();
						}
					});
				}
				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
			}
		};
		
		optionsY -= optionsStep;
		Text findGameButton = new Text(ResourceManager.getInstance().cameraWidth - 180, optionsY, 
				ResourceManager.getInstance().defaultFont60White, 
				context.getString(R.string.main_menu_find_game), 
				ResourceManager.getInstance().engine.getVertexBufferObjectManager()) {
			
			@Override
	        public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					SceneManager.getInstance().showScene(new FindGames());
				}
				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
			}
		};
		
		optionsY -= optionsStep;
		Text optionsButton = new Text(ResourceManager.getInstance().cameraWidth - 180, optionsY, 
				ResourceManager.getInstance().defaultFont60White, 
				context.getString(R.string.main_menu_options), 
				ResourceManager.getInstance().engine.getVertexBufferObjectManager()) {
			
			@Override
	        public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
				if(pSceneTouchEvent.isActionDown()) {
					SceneManager.getInstance().showOptionsLayer(false);
				}
				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
			}
		};
		
		this.registerTouchArea(myGamesButton);
		this.registerTouchArea(createGameButton);
		this.registerTouchArea(findGameButton);
		this.registerTouchArea(optionsButton);
		this.attachChild(myGamesButton);
		this.attachChild(createGameButton);
		this.attachChild(findGameButton);
		this.attachChild(optionsButton);

	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedScene#onShowScene()
	 */
	@Override
	public void onShowScene() {
		//Nothing to do
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedScene#onHideScene()
	 */
	@Override
	public void onHideScene() {
		//Nothing to do
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedScene#onUnloadScene()
	 */
	@Override
	public void onUnloadScene() {
		//Nothing to do
	}
	
	/**
	 * Show the create game dialog
	 */
	private void showCreateGameDialog() {
		final Context context = AndroidApplicationContextHolder.getInstance().getContext();
		
		final AlertDialog.Builder alert = new AlertDialog.Builder(context);
		final View createGameDialog = ((GameActivity) context).getLayoutInflater().inflate(R.layout.create_game, null);
		final Spinner factions = (Spinner) createGameDialog.findViewById(R.id.create_game_faction_input);
		final Spinner levels = (Spinner) createGameDialog.findViewById(R.id.create_game_level_input);
		final EditText title = (EditText) createGameDialog.findViewById(R.id.create_game_title_input);
		
		alert.setTitle(context.getString(R.string.create_game_title));
		alert.setView(createGameDialog);
		alert.setNegativeButton(context.getString(R.string.button_ko), null);
		
		//Populate factions select
		factions.setAdapter(new ArrayAdapter<EFaction>(context, android.R.layout.simple_spinner_dropdown_item, EFaction.values()));
		
		//Populate levels select
		levels.setAdapter(new ArrayAdapter<ELevel>(context, android.R.layout.simple_spinner_dropdown_item, ELevel.values()));
		
		//Create the dialog
		alert.setPositiveButton(context.getString(R.string.creage_game_ok_button), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				int minLength = Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.GAME_TITLE_MIN_LENGTH_KEY));
				if(title.getText().length() >= minLength) {
					LobbySvc.getInstance().createLobby(title.getText().toString(), 
							levels.getSelectedItem().toString(), 
							factions.getSelectedItem().toString(), instance, "createGameCallback");
				} else {
					AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_title_length, minLength), Toast.LENGTH_LONG);
					showCreateGameDialog();
				}
			}
		});
		
		alert.create();
		alert.show();
	}
	
	/**
	 * @param response
	 * @param failed
	 */
	public void createGameCallback(JSONObject response, boolean failed) {
		
		if(failed) {
			return;
		}
		
		Context context = AndroidApplicationContextHolder.getInstance().getContext();
		
		try {
			int status = response.getInt(NetworkConfiguration.RESPONSE_STATUS_KEY);
			
			if(status == StatusCode.TITLE_NOT_AVAILABLE) {
				AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_title_not_available), Toast.LENGTH_LONG);
				showCreateGameDialog();
				return;
			}
			
			if(status != StatusCode.OK) {
				throw new Exception();
			}
			
		} catch(Exception e) {
			AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_creating_game), Toast.LENGTH_LONG);
			showCreateGameDialog();
			return;
		}
	}
}
