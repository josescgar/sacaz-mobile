package com.escobeitor.sacaz.scene;

import java.util.ArrayList;
import java.util.List;

import org.andengine.util.adt.array.ArrayUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.activity.GameActivity;
import com.escobeitor.sacaz.enums.ELevel;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.json.JSONFileUtils;
import com.escobeitor.sacaz.manager.SceneManager;
import com.escobeitor.sacaz.manager.managed.ManagedGameScene;
import com.escobeitor.sacaz.network.NetworkConfiguration;
import com.escobeitor.sacaz.network.StatusCode;
import com.escobeitor.sacaz.network.adapters.MyGamesAdapter;
import com.escobeitor.sacaz.network.service.GameSvc;

/**
 * Unfinished games list
 * @author Escobeitor
 *
 */
public class MyGames extends ManagedGameScene {
	
	/**
	 * Reference to self
	 */
	private ManagedGameScene self;
	
	/**
	 * Context reference
	 */
	private final Context context = AndroidApplicationContextHolder.getInstance().getContext();
	
	/**
	 * Reference to the scene's view
	 */
	private View myGamesView;

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedGameScene#onLoadScene()
	 */
	@Override
	public void onLoadScene() {
		self = this;
		myGamesView = ((GameActivity) context).getLayoutInflater().inflate(R.layout.my_games, null);
		
		final TextView title = (TextView) myGamesView.findViewById(R.id.my_games_title);
		title.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/visitor2.ttf"));
		
		final ListView gamesList = (ListView) myGamesView.findViewById(R.id.my_games_list);
		
		OnItemClickListener gameItemClick = new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				final String gameId = ((TextView) view.findViewById(R.id.my_games_item_id)).getText().toString();
				final String map = ((TextView) view.findViewById(R.id.my_games_item_map)).getText().toString();
				StringBuffer fileName = new StringBuffer(gameId).append(GameConfiguration.GAME_FILE_EXTENSION);
				
				if(ArrayUtils.contains(context.fileList(), fileName.toString()) && isGameUpToDate(view, fileName.toString())) {
					SceneManager.getInstance().showScene(new GameLevel(gameId, ELevel.valueOf(map), fileName.toString()));
				} else {
					GameSvc.getInstance().retrieveGame(gameId, self, "retrieveGameCallback");
				}
				
			}
		};
		
		gamesList.setOnItemClickListener(gameItemClick);
		
		((GameActivity) context).runOnUiThread(new Runnable() {

			@Override
			public void run() {
				GameSvc.getInstance().retrieveUnfinishedGames(self, "myGamesCallback");
			}
			
		});
	
		super.onLoadScene();
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedGameScene#onShowScene()
	 */
	@Override
	public void onShowScene() {
		//Show the layout
		((GameActivity) context).runOnUiThread(new Runnable() {

			@Override
			public void run() {
				((GameActivity) context).addContentView(myGamesView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			}
			
		});
		
		super.onShowScene();
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedGameScene#onHideScene()
	 */
	@Override
	public void onHideScene() {
		((GameActivity) context).runOnUiThread(new Runnable() {

			@Override
			public void run() {
				myGamesView.setVisibility(View.GONE);
			}
			
		});
		
		super.onHideScene();
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedGameScene#onUnloadScene()
	 */
	@Override
	public void onUnloadScene() {
		self = null;
		((GameActivity) context).runOnUiThread(new Runnable() {

			@Override
			public void run() {
				((ViewGroup) myGamesView.getParent()).removeView(myGamesView);
			}
			
		});
		System.gc();
		super.onUnloadScene();
	}
	
	/**
	 * Check if the current game file is in sync with the server.
	 * If it is not, it retrieves it from the server
	 * @param fileName Local game filename
	 * @param view Server game data
	 * @return
	 */
	private boolean isGameUpToDate(View view, String fileName) {
		
		try {
			final int serverTurnNumber = Integer.parseInt(((TextView) view.findViewById(R.id.my_games_item_turn)).getText().toString());
			final String serverCurrentTurn = ((TextView) view.findViewById(R.id.my_games_item_currentTurn)).getText().toString();
			
			final JSONObject localGame = JSONFileUtils.getJSONFromFile(context.openFileInput(fileName));
			final int localTurnNumber = localGame.getInt("turns");
			final String localCurrentTurn = localGame.getString("currentTurn");
			
			//Turn and turn owner match
			if(serverTurnNumber == localTurnNumber && serverCurrentTurn.equals(localCurrentTurn)) {
				return true;
			}
			
			//Turn performed by the opponent but not yet fetched
			if((serverTurnNumber == localTurnNumber + 1) && !serverCurrentTurn.equals(localCurrentTurn)) {
				return true;
			}
			
			return false;
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error checking if the game is up to date: " + e.getMessage());
			AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_starting_game), Toast.LENGTH_LONG);
			return false;
		}
	}

	/**
	 * Retrieve my games list callback method
	 * @param response
	 * @param failed
	 */
	public void myGamesCallback(JSONObject response, boolean failed) {
		if(failed) {
			return;
		}
		
		try {
			
			if(response.getInt(NetworkConfiguration.RESPONSE_STATUS_KEY) != StatusCode.OK) {
				throw new Exception(response.getString(NetworkConfiguration.RESPONSE_MESSAGE_KEY));
			}
		
			final ListView list = (ListView) myGamesView.findViewById(R.id.my_games_list);
			List<JSONObject> results = new ArrayList<JSONObject>();
			JSONArray payload = response.getJSONArray(NetworkConfiguration.RESPONSE_PAYLOAD_KEY);
			
			for(int i = 0; i < payload.length(); i++) {
				results.add(payload.getJSONObject(i));
			}
			
			list.setAdapter(new MyGamesAdapter(context, results));
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error retrieving my games list: " + e.getMessage());
			AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_my_games_list), Toast.LENGTH_LONG);
		}
	}
	
	/**
	 * Retireve a game data callback method
	 * @param response
	 * @param failed
	 */
	public void retrieveGameCallback(JSONObject response, boolean failed) {
		if(failed) {
			return;
		}
		
		try {
			
			if(response.getInt(NetworkConfiguration.RESPONSE_STATUS_KEY) != StatusCode.OK) {
				throw new Exception(response.getString(NetworkConfiguration.RESPONSE_MESSAGE_KEY));
			}
			
			JSONObject data = response.getJSONObject(NetworkConfiguration.RESPONSE_PAYLOAD_KEY);
			
			String gameId = data.getJSONObject("_id").getString("$id");
			StringBuffer filename = new StringBuffer(gameId)
				.append(GameConfiguration.GAME_FILE_EXTENSION);
			
			//Save the game file
			JSONFileUtils.storeJSONInFile(filename.toString(), data);
			
			//Start game scene
			String map = data.getString("map");
			SceneManager.getInstance().showScene(new GameLevel(gameId, ELevel.valueOf(map), filename.toString()));
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error retrieving game: " + e.getMessage());
			AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_retrieving_game), Toast.LENGTH_LONG);
		}
	}
}
