package com.escobeitor.sacaz.scene;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.activity.GameActivity;
import com.escobeitor.sacaz.enums.EFaction;
import com.escobeitor.sacaz.enums.ELevel;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.LobbySearchResultCache;
import com.escobeitor.sacaz.manager.SceneManager;
import com.escobeitor.sacaz.manager.managed.ManagedGameScene;
import com.escobeitor.sacaz.network.NetworkConfiguration;
import com.escobeitor.sacaz.network.StatusCode;
import com.escobeitor.sacaz.network.adapters.LobbyAdapter;
import com.escobeitor.sacaz.network.service.LobbySvc;

/**
 * List user's own available games
 * @author Escobeitor
 *
 */
public class FindGames extends ManagedGameScene {
	
	/**
	 * My games list view
	 */
	private View findGamesView;
	
	/**
	 * Reference to self
	 */
	private FindGames self;
	
	/**
	 * Context reference
	 */
	private final Context context = AndroidApplicationContextHolder.getInstance().getContext();

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedGameScene#onLoadScene()
	 */
	@Override
	public void onLoadScene() {
		
		self = this;
		
		((GameActivity) context).runOnUiThread(new Runnable() {

			@Override
			public void run() {
				findGamesView = ((GameActivity) context).getLayoutInflater().inflate(R.layout.find_games, null);
				final TextView title = (TextView) findGamesView.findViewById(R.id.find_games_title);
				final Spinner factionInput = (Spinner) findGamesView.findViewById(R.id.find_games_filter_faction_input);
				final Spinner levelInput = (Spinner) findGamesView.findViewById(R.id.find_games_filter_level_input);
				final Button searchButton = (Button) findGamesView.findViewById(R.id.find_games_search_button);
				
				
				//Register list items actions
				final ListView myLobbies = (ListView) findGamesView.findViewById(R.id.find_games_my_lobbies_list);
				final ListView searchResults = (ListView) findGamesView.findViewById(R.id.find_games_list);
				
				OnItemClickListener myLobbiesListClick = new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						TextView lobbyId = (TextView) view.findViewById(R.id.lobby_list_item_id);
						SceneManager.getInstance().showScene(new LobbyInfo(lobbyId.getText().toString(), false));
					}
				};
				
				OnItemClickListener searchListClick = new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						TextView lobbyId = (TextView) view.findViewById(R.id.lobby_list_item_id);
						SceneManager.getInstance().showScene(new LobbyInfo(lobbyId.getText().toString(), true));
					}
				};
				
				myLobbies.setOnItemClickListener(myLobbiesListClick);
				searchResults.setOnItemClickListener(searchListClick);
				
				//Set scene elements
				title.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/visitor2.ttf"));
				factionInput.setAdapter(new ArrayAdapter<EFaction>(context, android.R.layout.simple_spinner_dropdown_item, EFaction.values()));
				levelInput.setAdapter(new ArrayAdapter<ELevel>(context, android.R.layout.simple_spinner_dropdown_item, ELevel.values()));
				
				//Set search button action
				searchButton.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {

						LobbySvc.getInstance().getGameList(-1, 
								levelInput.getSelectedItem().toString(), 
								factionInput.getSelectedItem().toString(), 
								self, 
								"searchCallback");
							
					}
				});
				
				//Get list of own games (joined or created)
				LobbySvc.getInstance().getGameList(-1, self, "myLobbiesCallback");

			}
			
		});
		
		
		
		super.onLoadScene();
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedGameScene#onShowScene()
	 */
	@Override
	public void onShowScene() {
		
		//Show the layout
		((GameActivity) context).runOnUiThread(new Runnable() {

			@Override
			public void run() {
				((GameActivity) context).addContentView(findGamesView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			}
			
		});
		
		super.onShowScene();
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedGameScene#onUnloadScene()
	 */
	@Override
	public void onUnloadScene() {
		ViewGroup vg = (ViewGroup)(findGamesView.getParent());
		vg.removeView(findGamesView);
		LobbySearchResultCache.getInstance().myLobbies.clear();
		LobbySearchResultCache.getInstance().searchResult.clear();
		self = null;
		System.gc();
		super.onUnloadScene();
	}
	
	/**
	 * Callback method for the user's lobbies search
	 */
	public void myLobbiesCallback(JSONObject response, boolean failed) {
		LobbySearchResultCache.getInstance().myLobbies.clear();
		final ListView myLobbies = (ListView) findGamesView.findViewById(R.id.find_games_my_lobbies_list);
		fillResults(myLobbies, response, failed);
	}
	
	/**
	 * Callback function for filling the results view
	 * @param response
	 * @param failed
	 */
	public void searchCallback(JSONObject response, boolean failed) {
		LobbySearchResultCache.getInstance().searchResult.clear();
		final ListView searchResults = (ListView) findGamesView.findViewById(R.id.find_games_list);
		fillResults(searchResults, response, failed);
	}
	
	/**
	 * Fill the given list with the results from the search
	 * @param list
	 * @param response
	 * @param failed
	 */
	private void fillResults(final ListView list, final JSONObject response, final boolean failed) {
		
		if(failed) {
			return;
		}
		
		try {
			
			int status = response.getInt(NetworkConfiguration.RESPONSE_STATUS_KEY);
			if(status != StatusCode.OK) {
				throw new Exception(response.getString(NetworkConfiguration.RESPONSE_MESSAGE_KEY));
			}
			
			Map<String, JSONObject> resultCache = list.getId() == R.id.find_games_list ? 
					LobbySearchResultCache.getInstance().searchResult : LobbySearchResultCache.getInstance().myLobbies;

			JSONArray result = response.getJSONArray(NetworkConfiguration.RESPONSE_PAYLOAD_KEY);
			List<JSONObject> items = new ArrayList<JSONObject>();
			
			for(int i = 0; i < result.length(); i++) {
				items.add(result.getJSONObject(i));
				resultCache.put(result.getJSONObject(i).getJSONObject("_id").getString("$id"), result.getJSONObject(i));
			}
			
			list.setAdapter(new LobbyAdapter(context, items));

		} catch(Exception e) {
			AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_getting_lobbies_list), Toast.LENGTH_LONG);
			Log.e(GameConfiguration.TAG_ERROR, "Error getting user's lobbies list: " + e.getMessage());
		}
	}
}
