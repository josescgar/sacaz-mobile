package com.escobeitor.sacaz.logging;

import com.escobeitor.sacaz.enums.EActionTypes;
import com.google.gson.annotations.Expose;

/**
 * An action performed in the game. Logging purposes.
 * @author Escobeitor
 *
 */
public class Action {

	/**
	 * Cost of performing the action
	 */
	@Expose
	public Object cost;
	
	/**
	 * Entity that performed the action
	 */
	@Expose
	public Object source;
	
	/**
	 * Result of applying the action
	 */
	@Expose
	public Object value;
	
	/**
	 * Nature of the action
	 */
	@Expose
	public EActionTypes type;
}
