package com.escobeitor.sacaz.logging;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.escobeitor.sacaz.abilities.interfaces.IAbility;
import com.escobeitor.sacaz.enums.EActionTypes;
import com.escobeitor.sacaz.enums.EGameResult;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.model.Game;
import com.escobeitor.sacaz.model.game.AbstractTarget;
import com.escobeitor.sacaz.model.game.Character;
import com.escobeitor.sacaz.model.game.Item;
import com.escobeitor.sacaz.model.game.NPC;
import com.escobeitor.sacaz.model.game.Player;
import com.escobeitor.sacaz.model.game.Position;
import com.escobeitor.sacaz.model.game.Weapon;
import com.escobeitor.sacaz.model.interfaces.ITargetable;
import com.escobeitor.sacaz.network.service.LogSvc;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Game log
 * @author Escobeitor
 *
 */
public class Log {
	
	/**
	 * Log unique id
	 */
	@Expose
	@SerializedName("_id")
	public String id;
	
	/**
	 * Log date
	 */
	@Expose
	public Date date;
	
	/**
	 * Performed actions
	 */
	@Expose
	public List<Action> actions;
	
	/**
	 * Default constructor
	 */
	public Log() {
		this.actions = new ArrayList<Action>();
		this.date = new Date();
	}
	
	/**
	 * Process the log, reconstructing the game from the actions.
	 */
	public void process() {
		//TODO
	}
	
	/**
	 * Finish the log
	 */
	public void finish() {
		LogSvc.getInstance().sendLog(this, GameContextHolder.getInstance().game.id);
		clear();
	}
	
	/**
	 * Clear the log
	 */
	public void clear() {
		actions.clear();
		date = new Date();
		storeLocal();
	}
	
	/**
	 * Insert the new character creation in the log
	 * and updates the game raw data
	 * @param character
	 * @throws JSONException
	 */
	public void addNewCharacter(Character character) throws JSONException {

		JSONObject object = new JSONObject(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(character, Character.class));
		JSONObject gameJSON = GameContextHolder.getInstance().gameJSON;
		
		String player = gameJSON.getJSONObject("player1").getString("userId").equals(GameContextHolder.getInstance().user.id) ? "player1" : "player2";

		gameJSON.getJSONObject(player).getJSONArray("characters").put(object);
			
		updateGameJSONResources(character.player, gameJSON.getJSONObject(player));

		addAction(EActionTypes.NEW_CHARACTER, null, character.player.userId, serializeObject(character, Character.class));
	}
	
	public void addEnergyDistribution(float[] distribution) {
		//TODO
	}
	
	public void addRangerAllocation(int[] allocation) {
		//TODO
	}
	
	/**
	 * Registers the character movement in the log
	 * @param character
	 * @param cost
	 * @param index
	 * @throws JSONException
	 */
	public void addMoveCharacter(Character character, int cost, int index) throws JSONException {
		
		JSONObject gameJSON = GameContextHolder.getInstance().gameJSON;
		
		String player = gameJSON.getJSONObject("player1").getString("userId").equals(GameContextHolder.getInstance().user.id) ? "player1" : "player2";
		
		JSONObject c = gameJSON.getJSONObject(player).getJSONArray("characters").getJSONObject(index);
		
		c.getJSONObject("position").put("x", character.position.x);
		c.getJSONObject("position").put("y", character.position.y);
		
		updateGameJSONResources(character.player, gameJSON.getJSONObject(player));
		
		addAction(EActionTypes.CHARACTER_MOVEMENT, cost, character.id, serializeObject(character.position, Position.class));
	}
	
	public void addPickUp(Character character, Item item) {
		//TODO
	}
	
	/**
	 * Registers a character attack in the log
	 * @param character
	 * @param target
	 * @param weapon
	 * @param damage
	 * @throws JSONException 
	 */
	public void addStrike(Character character, AbstractTarget target, Weapon weapon, int damage) throws JSONException {
		
		final StringBuffer targetType = new StringBuffer(target.getClass().getSimpleName().toLowerCase()).append('s');
		
		//Update local game file
		final JSONObject gameJSON = GameContextHolder.getInstance().gameJSON;
		final String playerKey = character.player.userId.equals(gameJSON.getJSONObject("player1").get("userId")) ? "player1" : "player2";
		final int weaponIndex = character.weapons.indexOf(weapon);
		final int characterIndex = character.player.characters.indexOf(character);
		final JSONObject characterJSON = gameJSON.getJSONObject(playerKey).getJSONArray("characters").getJSONObject(characterIndex);
		characterJSON.getJSONArray("weapons").getJSONObject(weaponIndex).put("ammo", weapon.ammo);
		
		String opponentKey = playerKey.equals("player1") ? "player2." : "player1.";
		opponentKey = targetType.toString().equals("npcs") ? "" : opponentKey;
		
		final StringBuffer targetKey = new StringBuffer(opponentKey).append(targetType);
		
		//Update log
		Map<String, Object> value = new HashMap<String, Object>();
		value.put("targetId", target.id);
		value.put("weaponId", weapon.id);
		value.put("damage", damage);
		value.put("finalHealth", target.health - damage);
		value.put("targetType", targetType.toString());
		value.put("weaponIndex", weaponIndex);
		value.put("targetKey", targetKey);
		
		addAction(EActionTypes.STRIKE, null, character.id, new Gson().toJson(value));
	}
	
	public void addRangerSwith(boolean status) {
		//TODO
	}
	
	public void addFinishGame(EGameResult result) {
		//TODO
	}
	
	/**
	 * Registers the new NPC in the log
	 * @param npc
	 * @throws JSONException 
	 */
	public void addNewNPC(NPC npc) throws JSONException {
		
		JSONObject object = new JSONObject(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(npc, NPC.class));
		
		GameContextHolder.getInstance().gameJSON.getJSONArray("npcs").put(object);

		addAction(EActionTypes.NEW_NPC, null, null, serializeObject(npc, NPC.class));
	}
	
	/**
	 * Registers a NPC movement
	 * @param npc
	 * @param position
	 * @throws JSONException 
	 */
	public void addMoveNPC(NPC npc, int index) throws JSONException {	
		
		JSONObject npcJSON = GameContextHolder.getInstance().gameJSON.getJSONArray("npcs").getJSONObject(index);
		npcJSON.put("target", npc.target.id);
		
		npcJSON.getJSONObject("position").put("x", npc.position.x);
		npcJSON.getJSONObject("position").put("y", npc.position.y);
		
		addAction(EActionTypes.NPC_MOVEMENT, npc.target.id, npc.id, serializeObject(npc.position, Position.class));
	}
	
	/**
	 * Registers a new NPC attack
	 * @param npc
	 * @param target
	 * @param damage
	 * @throws JSONException
	 */
	public void addNPCStrike(NPC npc, ITargetable target, int damage) throws JSONException {
		final String playerKey = ((Character) target).player.userId.equals(
				GameContextHolder.getInstance().gameJSON.getJSONObject("player1").get("userId")) ? "player1" : "player2";
		addAction(EActionTypes.NPC_STRIKE, playerKey, ((Character) target).id, ((Character) target).health - damage);
	}
	
	public void addRangerRaffle(int[] allocation, List<Item> itemsFound) {
		//TODO
	}
	
	public void addAbilityUsed(IAbility ability, ITargetable target) {
		//TODO
	}
	
	/**
	 * Registers the end of a turn sending the next turn owner ID
	 * @param currentTurn
	 * @throws JSONException 
	 */
	public void addTurnFinished() throws JSONException {

		Game game = GameContextHolder.getInstance().game;
		
		JSONObject gameJSON = GameContextHolder.getInstance().gameJSON;
		gameJSON.put("turns", game.turns);
		gameJSON.put("currentTurn", game.currentTurn);
		gameJSON.put("lastTurnDate", game.lastTurnDate.getTime());
		gameJSON.put("numNPC", game.numNPC);
		
		Player player = GameContextHolder.getInstance().game.getLocalPlayer();
		
		final HashMap<String, Integer> totalCosts = new HashMap<String, Integer>();
		totalCosts.put("ap", player.actionPoints);
		totalCosts.put("food", player.food);
		totalCosts.put("materials", player.materials);
		totalCosts.put("water", player.water);
		totalCosts.put("electricity", player.electricity);
		
		addAction(EActionTypes.TURN_FINISHED, serializeObject(totalCosts, Map.class), null, serializeObject(game, Game.class));
	}
	
	/**
	 * Creates a new Action and add it to the log
	 * @param type
	 * @param cost
	 * @param source
	 * @param value
	 */
	private void addAction(EActionTypes type, Object cost, Object source, Object value) {
		Action action = new Action();
		action.cost = cost;
		action.source = source;
		action.value = value;
		action.type = type;
		
		actions.add(action);
	}
	
	/**
	 * @param player
	 * @param playerJSON
	 * @throws JSONException 
	 */
	public void updateGameJSONResources(Player player, JSONObject playerJSON) throws JSONException {
		playerJSON.put("actionPoints", player.actionPoints);
		playerJSON.getJSONObject("resources").put("electricity", player.electricity);
		playerJSON.getJSONObject("resources").put("food", player.food);
		playerJSON.getJSONObject("resources").put("materials", player.materials);
		playerJSON.getJSONObject("resources").put("water", player.water);
	}
	
	/**
	 * @param player
	 * @param playerJSON
	 * @throws JSONException 
	 */
	public void updateGameJSONResources(Player player) throws JSONException {
		
		JSONObject playerJSON = GameContextHolder.getInstance().gameJSON.getJSONObject("player1");
		if(!playerJSON.getString("userId").equals(player.userId)) {
			playerJSON = GameContextHolder.getInstance().gameJSON.getJSONObject("player2");
		}
		
		updateGameJSONResources(player, playerJSON);
	}
	
	/**
	 * Serialize and store the current log instance
	 */
	public void storeLocal() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(AndroidApplicationContextHolder.getInstance().getContext());
		StringBuffer logName = new StringBuffer(GameContextHolder.getInstance().game.id).append(GameConfiguration.LOCAL_LOG_FILE_SUFFIX);
		prefs.edit().putString(logName.toString(), new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(this)).commit();
	}
	
	/**
	 * Serialize an object before storing it
	 * @param o
	 * @param c
	 * @return
	 */
	private String serializeObject(Object o, Class<?> c) {
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(o, c);
	}
}
