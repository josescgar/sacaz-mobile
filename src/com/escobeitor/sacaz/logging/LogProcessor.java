package com.escobeitor.sacaz.logging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.escobeitor.sacaz.enums.EActionTypes;
import com.escobeitor.sacaz.enums.EElement;
import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.manager.ResourceManager;
import com.escobeitor.sacaz.model.Game;
import com.escobeitor.sacaz.model.factory.WalkerFactory;
import com.escobeitor.sacaz.model.game.AbstractTarget;
import com.escobeitor.sacaz.model.game.Character;
import com.escobeitor.sacaz.model.game.NPC;
import com.escobeitor.sacaz.model.game.Position;
import com.escobeitor.sacaz.model.game.Weapon;

/**
 * Process a log and reconstructs a game from the actions.
 * Game reference obtained by through GameContextHolder.
 * @author Escobeitor
 *
 */
public class LogProcessor {
	
	/**
	 * Class instance (singleton)
	 */
	private static LogProcessor instance = null;
	
	/**
	 * Singleton method
	 * @return
	 */
	public static LogProcessor getInstance() {
		if(instance == null) {
			instance = new LogProcessor();
		}
		
		return instance;
	}
	
	/**
	 * Process a new LOG
	 * @param log
	 * @param game
	 * @throws JSONException
	 */
	public void process(JSONObject log) throws JSONException {
		JSONArray actions = log.getJSONArray("actions");
		for(int i = 0; i < actions.length(); i++) {
			JSONObject action = actions.getJSONObject(i);
			EActionTypes type = EActionTypes.valueOf(action.getString("type"));
			
			switch(type) {
				case ABILITY_USED:
					break;
				case CHARACTER_MOVEMENT:
					processCharacterMovement(action);
					break;
				case ENERGY_DISTRIBUTION:
					break;
				case GAME_FINISHED:
					break;
				case NEW_CHARACTER:
					processNewCharacter(action);
					break;
				case NEW_NPC:
					processNewNPC(action);
					break;
				case NPC_MOVEMENT:
					processNPCMovement(action);
					break;
				case PICK_UP_OBJECT:
					break;
				case STRIKE:
					processStrike(action);
					break;
				case TURN_FINISHED:
					processTurnFinished(action);
					break;
				default:
					break;
			}
		}
		
	}
	
	private void processEnergyDistribution(JSONObject action) {
		//TODO
	}
	
	/**
	 * Process a character movement from current position to another
	 * @param action
	 * @throws JSONException 
	 */
	private void processCharacterMovement(JSONObject action) throws JSONException {
		
		JSONObject value = new JSONObject(action.getString("value"));

		//Move character
		Character c = GameContextHolder.getInstance().game.findCharacterById(action.getString("source"));
		final Position pos = new Position((float) value.getDouble("x"), (float) value.getDouble("y"));
		c.moveToPosition(pos, false);
		
		//Update game file
		final JSONObject gameJSON = GameContextHolder.getInstance().gameJSON;
		final String playerKey = c.player.userId.equals(gameJSON.getJSONObject("player1").getString("userId")) ? "player1" : "player2";
		final JSONObject characterJSON = gameJSON.getJSONObject(playerKey).getJSONArray("characters").getJSONObject(c.player.characters.indexOf(c));
		characterJSON.getJSONObject("position").put("x", pos.x);
		characterJSON.getJSONObject("position").put("y", pos.y);

	}
	
	/**
	 * Process a NPC movement from current position to another
	 * @param action
	 * @throws JSONException 
	 */
	private void processNPCMovement(JSONObject action) throws JSONException {

		//Move npc
		NPC npc = GameContextHolder.getInstance().game.findNPCById(action.getString("source"));
		final int npcIndex = GameContextHolder.getInstance().game.npcs.indexOf(npc);
		npc.target = GameContextHolder.getInstance().game.findCharacterById(action.getString("cost"));
		npc.moveTowardsTarget(npcIndex, false);
		
		//Update game
		final JSONObject gameNpcJSON = GameContextHolder.getInstance().gameJSON.getJSONArray("npcs").getJSONObject(npcIndex);
		gameNpcJSON.getJSONObject("position").put("x", npc.position.x);
		gameNpcJSON.getJSONObject("position").put("y", npc.position.y);

	}
	
	private void processPickUpObject(JSONObject action) {
		//TODO
	}
	
	private void processGameFinished(JSONObject action) {
		//TODO
	}
	
	private void processAbilityUsed(JSONObject action) {
		//TODO
	}
	
	private void processRangerRaffle(JSONObject action) {
		//TODO
	}
	
	private void processRangerSwitch(JSONObject action) {
		//TODO
	}
	
	/**
	 * Process a character strike
	 * @param action
	 * @throws JSONException 
	 */
	private void processStrike(JSONObject action) throws JSONException {
		final Character character = GameContextHolder.getInstance().game.findCharacterById(action.getString("source"));
		final JSONObject value = new JSONObject(action.getString("value"));
		
		final Weapon weapon = character.weapons.get(value.getInt("weaponIndex"));
		
		AbstractTarget target = null;
		final String targetType = value.getString("targetType");
		final String targetId = value.getString("targetId");
		
		if("npcs".equals(targetType)) {
			target = GameContextHolder.getInstance().game.findNPCById(targetId);
		}
		
		if("characters".equals(targetType)) {
			target = GameContextHolder.getInstance().game.findCharacterById(targetId);
		}
		
		if(("generators").equals(targetType)) {
			target = GameContextHolder.getInstance().game.findGeneratorById(targetId);
		}
		
		character.strike(target, weapon, false, value.getInt("damage"));
	}
	
	private void processRangerResources(JSONObject action) {
		//TODO
	}
	
	/**
	 * Processes a new opponent's character
	 * @param action
	 * @throws JSONException 
	 */
	private void processNewCharacter(JSONObject action) throws JSONException {
		
		//Update game file
		JSONObject character = new JSONObject(action.getString("value"));
		JSONObject gameJSON = GameContextHolder.getInstance().gameJSON;
		
		final String playerKey = action.getString("source").equals(gameJSON.getJSONObject("player1").getString("userId")) ? "player1" : "player2";
		gameJSON.getJSONObject(playerKey).getJSONArray("characters").put(character);
		
		//Create and show character
		Character c = (Character) WalkerFactory.getInstance().createElement(EElement.valueOf(character.getString("type")));
		c.player = GameContextHolder.getInstance().game.getPlayerById(action.getString("source"));
		c.health = character.getInt("health");
		c.id = character.getString("id");
		c.player.characters.add(c);
		
		c.position.x = (float) character.getJSONObject("position").getDouble("x");
		c.position.y = (float) character.getJSONObject("position").getDouble("y");
		c.setPosition(c.position.x, c.position.y);
		c.setCurrentTileIndex(1);
		c.setAnchorCenter(0, 0);
		
		ResourceManager.gameMap.attachChild(c);
	}
	
	/**
	 * Process a new NPC from log
	 * @param action
	 * @throws JSONException 
	 */
	private void processNewNPC(JSONObject action) throws JSONException {
			
		//Update game
		JSONObject npcJSON = new JSONObject(action.getString("value"));
		GameContextHolder.getInstance().gameJSON.getJSONArray("npcs").put(npcJSON);
		
		//Create a show NPC
		NPC npc = (NPC) WalkerFactory.getInstance().createElement(EElement.valueOf(npcJSON.getString("type")));
		npc.health = npcJSON.getInt("health");
		npc.boobyTrapped = npcJSON.getBoolean("boobyTrapped");
		npc.id = npcJSON.getString("id");
		
		if(npcJSON.has("target")) {
			npc.target = GameContextHolder.getInstance().game.findCharacterById(npcJSON.getString("target"));
		}
		
		npc.position.x = (float) npcJSON.getJSONObject("position").getDouble("x");
		npc.position.y = (float) npcJSON.getJSONObject("position").getDouble("y");
		npc.setPosition(npc.position.x, npc.position.y);
		npc.setCurrentTileIndex(1);
		npc.setAnchorCenter(0, 0);
		
		GameContextHolder.getInstance().game.npcs.add(npc);
		
		ResourceManager.gameMap.attachChild(npc);
	}
	
	/**
	 * Process a new NPC from log
	 * @param action
	 * @throws JSONException 
	 */
	private void processTurnFinished(JSONObject action) throws JSONException {
			
		final JSONObject value = new JSONObject(action.getString("value"));
		
		final Game game = GameContextHolder.getInstance().game;
		game.currentTurn = value.getString("currentTurn");
		game.turns = value.getInt("turns");
		game.numNPC = value.getInt("numNPC");
		
		final JSONObject gameJSON = GameContextHolder.getInstance().gameJSON;
		gameJSON.put("currentTurn", game.currentTurn);
		gameJSON.put("turns", game.turns);
		gameJSON.put("numNPC", game.numNPC);

	}
}
