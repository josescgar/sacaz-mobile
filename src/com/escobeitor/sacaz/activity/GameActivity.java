package com.escobeitor.sacaz.activity;

import java.io.IOException;

import org.andengine.engine.Engine;
import org.andengine.engine.FixedStepEngine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.LayoutGameActivity;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Process;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.manager.ResourceManager;
import com.escobeitor.sacaz.manager.SceneManager;
import com.escobeitor.sacaz.manager.managed.ManagedGameScene;
import com.escobeitor.sacaz.model.User;
import com.escobeitor.sacaz.network.NetworkConfiguration;
import com.escobeitor.sacaz.network.StatusCode;
import com.escobeitor.sacaz.network.service.UserSvc;
import com.escobeitor.sacaz.scene.GameLevel;
import com.escobeitor.sacaz.scene.LobbyInfo;
import com.escobeitor.sacaz.scene.MainMenu;
import com.google.android.gms.common.AccountPicker;

/**
 * Android's activity used for creating the game. This activity
 * is where the game attaches the different scenes and load
 * all resources.
 * @author Escobeitor
 * @author Based on code by Brian Broyles - IFL Game Studio
 */
public class GameActivity extends LayoutGameActivity {
	
	/**
	 * Engine fixed frame rate
	 */
	public static final int STEPS_PER_SECOND = 60;

	/**
	 * Id for user account selection request
	 */
	private static final int PICK_ACCOUNT_REQUEST = 1987;
	
	/**
	 * Resolution for which the game was designed (in pixels)
	 */
	private static float DESIGN_SCREEN_WIDTH_PX = 800f, DESIGN_SCREEN_HEIGHT_PX = 480f;
	
	/**
	 * Resolution for which the game was designed (in inches)
	 */
	private static float DESIGN_SCREEN_WIDTH_IN = 4.472441f, DESIGN_SCREEN_HEIGHT_IN = 2.805118f;
	
	/**
	 * Minimum supported resolution (in pixels)
	 */
	private static float MIN_WIDTH = 800f, MIN_HEIGHT = 480f;
	
	/**
	 * Maximum supported resolution (in pixels)
	 */
	private static float MAX_WIDTH = 800f, MAX_HEIGHT = 480f;
	
	/**
	 * Game camera final size (in pixels)
	 */
	private float cameraWidth, cameraHeight;
	
	/**
	 * Current device resolution (in inches)
	 */
	private float realWidthInches, realHeightInches;
	
	/**
	 * Game camera
	 */
	private Camera camera;
	
	/**
	 * Game shared preferences storage
	 */
	private SharedPreferences prefs;
	
	/**
	 * Reference to self
	 */
	private GameActivity activity;

	/**
	 * @see org.andengine.ui.IGameInterface#onCreateEngineOptions()
	 */
	@Override
	public EngineOptions onCreateEngineOptions() {
		
		realWidthInches = getResources().getDisplayMetrics().widthPixels / getResources().getDisplayMetrics().xdpi;
		
		realHeightInches = getResources().getDisplayMetrics().heightPixels / getResources().getDisplayMetrics().ydpi;
		
		cameraWidth = Math.round(Math.max(Math.min(DESIGN_SCREEN_WIDTH_PX * (realWidthInches / DESIGN_SCREEN_WIDTH_IN), MAX_WIDTH), MIN_WIDTH));
		
		cameraHeight = Math.round(Math.max(Math.min(DESIGN_SCREEN_HEIGHT_PX * (realHeightInches / DESIGN_SCREEN_HEIGHT_IN), MAX_HEIGHT), MIN_HEIGHT));

		camera = new Camera(0, 0, cameraWidth, cameraHeight);
		
		EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, 
				new FillResolutionPolicy(), camera);
		
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
		
		engineOptions.getRenderOptions().setDithering(true);
		
		engineOptions.getRenderOptions().getConfigChooserOptions().setRequestedMultiSampling(true);
		
		return engineOptions;
	}

	/**
	 * Creates the game engine. Changing the engine of the game
	 * may alter the user experience drastically.
	 */
	@Override
	public Engine onCreateEngine(EngineOptions pEngineOptions) {
		Engine engine = new FixedStepEngine(pEngineOptions, STEPS_PER_SECOND);
		initializeGameConfiguration();
		ResourceManager.getInstance().initialize(engine, camera, 
				cameraHeight / DESIGN_SCREEN_HEIGHT_PX, 
				cameraWidth / DESIGN_SCREEN_WIDTH_PX, 
				cameraWidth, 
				cameraHeight);
		return engine;
	}
	
	/**
	 * Initialize game global resources and log in into the main server
	 */
	private void initializeGameConfiguration() {

		//Set the app context in the app context holder
		AndroidApplicationContextHolder.getInstance().setContext(this);
		this.activity = this;
		
		//Load shared preferences
		this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		//Load the global app configuration
		try {
			GameConfiguration.initializeConstants();
			NetworkConfiguration.initializeNetworkConfiguration();
		} catch(Exception e) {
			AndroidApplicationContextHolder.toastOnUiThread(this.getString(R.string.error_game_initialization), Toast.LENGTH_LONG);
			Process.killProcess(Process.myPid());
		}
		
		//Initialize options configuration
		if(!prefs.contains(GameConfiguration.SHARED_PREFS_ALLOW_NOTIFICATIONS_KEY)) {
			prefs.edit().putBoolean(GameConfiguration.SHARED_PREFS_ALLOW_NOTIFICATIONS_KEY, true).commit();
		}
		
		if(!prefs.contains(GameConfiguration.SHARED_PREFS_MUTE_SOUND_KEY)) {
			prefs.edit().putBoolean(GameConfiguration.SHARED_PREFS_MUTE_SOUND_KEY, false).commit();
		}
		
		//Select user account to identify him in the game if not already identified
		if(prefs.getString(GameConfiguration.SHARED_PREFS_AUTH_TOKEN_KEY, null) == null) {
			Intent intent = AccountPicker.newChooseAccountIntent(null, null, new String[]{"com.google"}, false, null, null, null, null);
			startActivityForResult(intent, PICK_ACCOUNT_REQUEST);
		} else {
			UserSvc.getInstance().login(this, "loginCallback");
		}
		
	}

	/**
	 * @see org.andengine.ui.IGameInterface#onCreateResources(org.andengine.ui.IGameInterface.OnCreateResourcesCallback)
	 */
	@Override
	public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException {
		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	/**
	 * @see org.andengine.ui.IGameInterface#onCreateScene(org.andengine.ui.IGameInterface.OnCreateSceneCallback)
	 */
	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws IOException {
		SceneManager.getInstance().showMainMenu();
		pOnCreateSceneCallback.onCreateSceneFinished(MainMenu.getInstance());
	}

	/**
	 * @see org.andengine.ui.IGameInterface#onPopulateScene(org.andengine.entity.scene.Scene, org.andengine.ui.IGameInterface.OnPopulateSceneCallback)
	 */
	@Override
	public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws IOException {
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}

	/**
	 * Returns a reference to the game activity layout
	 */
	@Override
	protected int getLayoutID() {
		return R.layout.game_layout;
	}

	/**
	 * Returns a reference to the game activity render surface view
	 */
	@Override
	protected int getRenderSurfaceViewID() {
		return R.id.gameSurfaceView;
	}
	//TODO
//	/**
//	 * Get SACAZ notification intents
//	 * @see android.app.Activity#onNewIntent(android.content.Intent)
//	 */
//	@Override
//	public void onNewIntent(Intent intent) {
//		Log.d(GameConfiguration.TAG_DEBUG, "Starting from new intent: " + intent.getExtras().toString());
//		
//		final Bundle extras = intent.getExtras();
//		final String type = extras.getString(GameConfiguration.GCM_MESSAGE_TYPE);
//		final String action = extras.getString(GameConfiguration.GCM_MESSAGE_ACTION_KEY);
//		
//		if(GameConfiguration.GCM_MESSAGE_TYPE_LOBBY.equals(type) 
//				&& !GameConfiguration.GCM_MESSAGE_ACTION_DELETE.equals(action)) {
//			
//			String lobbyId = extras.getString(GameConfiguration.GCM_MESSAGE_MESSAGE_LOBBY_ID);
//			
//			SceneManager.getInstance().showScene(new LobbyInfo(lobbyId));
//		}
//	}
	
	/**
	 * Since there is only one activity, overwrite default "back" button
	 * behavior to navigate through the game scenes
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 */
	public boolean onKeyDown(final int keyCode, final KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
			//If the engine is running, override default behavior
			if(ResourceManager.getInstance().engine != null) {
				
				//If a layer is shown, just close it
				if(SceneManager.getInstance().isLayerShown) {
					SceneManager.getInstance().currentLayer.onHideLayer();
				
				//If in-game, show options menu
				} else if(SceneManager.getInstance().currentScene.getClass().equals(GameLevel.class)) {
					SceneManager.getInstance().showOptionsLayer(false);
					
				//If an scene different from the main menu is currently shown, close it and open the main menu
				} else if(SceneManager.getInstance().currentScene.getClass().getGenericSuperclass().equals(ManagedGameScene.class)
						&& !SceneManager.getInstance().currentScene.getClass().getGenericSuperclass().equals(MainMenu.class)) {
					SceneManager.getInstance().showMainMenu();
				
				//If not scene to fall back, finish the application
				} else {
					System.exit(0);
				}
				
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * Callback function after performing a login. Identifies the user
	 * in the game. If an error occurred, invalidate the current auth token.
	 * @param response
	 * @param failed
	 */
	public void loginCallback(JSONObject response, boolean failed) {
		if(failed) {
			return;
		}
		
		try {
			int status = response.getInt(NetworkConfiguration.RESPONSE_STATUS_KEY);
			switch(status) {
			case StatusCode.OK:
				GameContextHolder.getInstance().user = getUserFromResponse(response);
				break;
			case StatusCode.INVALID_AUTH_KEY:
				//prefs.edit().remove(GameConfiguration.SHARED_PREFS_AUTH_TOKEN_KEY).commit();
				throw new Exception(this.getString(R.string.error_auth_token));
			case StatusCode.USERNAME_REQUIRED:
				GameContextHolder.getInstance().user = getUserFromResponse(response);
				showUsernameDialog();
				break;
			default:
				String message = response.getString(NetworkConfiguration.RESPONSE_MESSAGE_KEY);
				throw new Exception(message);
			}
			
		} catch (Exception e) {
			AndroidApplicationContextHolder.toastOnUiThread(this.getString(R.string.error_login_problem) + ' ' + e.getMessage(), Toast.LENGTH_LONG);
		}
	}

	/**
	 * Get the user parameters from a server response
	 * @param response
	 * @return
	 * @throws JSONException
	 */
	private User getUserFromResponse(JSONObject response) throws JSONException {
		JSONObject data = response.getJSONObject(NetworkConfiguration.RESPONSE_PAYLOAD_KEY);
		User user = new User();
		user.email = data.getString("email");
		user.id = data.getString("_id");
		user.username = data.has("username") ? data.getString("username") : null;
		user.won = data.getInt("won");
		user.lost = data.getInt("lost");
		user.resigned = data.getInt("resigned");
		return user;
	}
	
	/**
	 * If the user doesn't have an username,
	 * prompt a dialog to ask for it and send it
	 * to the server
	 */
	private void showUsernameDialog() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle(this.getString(R.string.username_dialog_title));
		alert.setMessage(this.getString(R.string.username_dialog_field));

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		alert.setCancelable(false);
		alert.setView(input);

		alert.setPositiveButton(this.getString(R.string.button_ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				int maxLength = Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.USERNAME_MAX_LENGTH_KEY));
				int minLength = Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.USERNAME_MIN_LENGTH_KEY));
				
				if(value.length() < minLength || value.length() > maxLength) {
					AndroidApplicationContextHolder.toastOnUiThread(getApplicationContext().getString(R.string.username_limits, minLength, maxLength), Toast.LENGTH_LONG);
					showUsernameDialog();
					return;
				}
				
				UserSvc.getInstance().saveUsername(value, activity, "usernameCallback");
			}
		});

		alert.show();
	}
	
	/**
	 * Save username to server callback method. Show the username pick
	 * dialog again if any problem occurred.
	 * @param response
	 * @param failed
	 */
	public void usernameCallback(JSONObject response, boolean failed) {
		try {	
			if(failed) {
				throw new Exception();
			}
			
			int status = response.getInt(NetworkConfiguration.RESPONSE_STATUS_KEY);
			if(status != StatusCode.OK) {
				throw new Exception();
			}
			
			GameContextHolder.getInstance().user.username = 
					response.getString(NetworkConfiguration.RESPONSE_PAYLOAD_KEY);
			
		} catch(Exception e) {
			if(!failed) {
				AndroidApplicationContextHolder.toastOnUiThread(this.getString(R.string.error_saving_username), Toast.LENGTH_LONG);
			}
			
			showUsernameDialog();
		}
	}
	
	/**
	 * Get the result from the user account selection and register it
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == PICK_ACCOUNT_REQUEST && resultCode == RESULT_OK) {
            String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
	    	this.prefs.edit().putString(GameConfiguration.SHARED_PREFS_USER_ACCOUNT_KEY, accountName).commit();
	    	UserSvc.getInstance().login(this, "loginCallback");
        } else {
        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
        	builder.setMessage(this.getString(R.string.error_no_google_account));
        	builder.setPositiveButton(R.string.button_ok, null);
        	builder.show();
        }
    }
}
