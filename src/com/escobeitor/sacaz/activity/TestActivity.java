package com.escobeitor.sacaz.activity;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;

import android.app.Activity;
import android.os.Bundle;

public class TestActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationContextHolder.getInstance().setContext(getApplicationContext());
		setContentView(R.layout.activity_test);
	}

}
