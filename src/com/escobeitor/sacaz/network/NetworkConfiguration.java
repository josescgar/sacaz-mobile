package com.escobeitor.sacaz.network;

import java.util.Properties;

import android.util.Log;

import com.escobeitor.sacaz.exceptions.GameConfigurationInitializationException;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;

/**
 * Network related configuration values
 * @author Escobeitor
 *
 */
public class NetworkConfiguration {
	
	/**
	 * Selected network protocol key
	 */
	public static final String PROTOCOL_KEY = "protocol";
	
	/**
	 * Base url key
	 */
	public static final String BASE_URL_KEY = "base_url";
	
	/**
	 * Login server url key
	 */
	public static final String URL_LOGIN_KEY = "url_login";
	
	/**
	 * Server API Key properties key
	 */
	public static final String API_KEY = "api_key";
	
	/**
	 * Google Cloud Messages sender ID
	 */
	public static final String GCM_SENDER = "gcm_sender_id";
	
	/**
	 * Google Cloud Messages API key
	 */
	public static final String GCM_API_KEY = "gcm_api_key";
	
	/**
	 * Request time out
	 */
	public static final String TIMEOUT_KEY = "timeout";
	
	/**
	 * Scope string for Google Auth
	 */
	public static final String AUTH_SCOPE = "auth_scope";
	
	/**
	 * Scope string for Google Auth
	 */
	public static final String RESPONSE_STATUS_KEY = "status";
	
	/**
	 * Scope string for Google Auth
	 */
	public static final String RESPONSE_MESSAGE_KEY = "msg";
	
	/**
	 * Scope string for Google Auth
	 */
	public static final String RESPONSE_PAYLOAD_KEY = "data";
	
	/**
	 * API url for saving a username
	 */
	public static final String URL_SAVE_USERNAME_KEY = "url_save_username";
	
	/**
	 * API url for creating a new game
	 */
	public static final String URL_CREATE_GAME_KEY = "url_create_game";
	
	/**
	 * API url for listing the user's lobbies
	 */
	public static final String URL_MY_LOBBIES_KEY = "url_my_lobbies";
	
	/**
	 * API url for searching lobbies
	 */
	public static final String URL_SEARCH_LOBBIES_KEY = "url_find_lobbies";
	
	/**
	 * API url for getting a lobby
	 */
	public static final String URL_GET_LOBBY_KEY = "url_get_lobby";
	
	/**
	 * API url for removing a lobby
	 */
	public static final String URL_REMOVE_LOBBY_KEY = "url_remove_lobby";
	
	/**
	 * API url for leaving a lobby
	 */
	public static final String URL_LEAVE_LOBBY_KEY = "url_leave_lobby";
	
	/**
	 * API url for joining a lobby
	 */
	public static final String URL_JOIN_LOBBY_KEY = "url_join_lobby";

	/**
	 * API url for starting a new game
	 */
	public static final String URL_START_GAME_KEY = "url_start_game";
	
	/**
	 * API url for starting a new game
	 */
	public static final String URL_MY_GAMES_LIST = "url_games_list";
	
	/**
	 * API url for retrieving an existing game
	 */
	public static final String URL_RETRIEVE_GAME = "url_retrieve_game";
	
	/**
	 * API url for retrieving a game log
	 */
	public static final String URL_RETRIEVE_LOG = "url_retrieve_log";
	
	/**
	 * API url for sending a game log
	 */
	public static final String URL_SEND_LOG = "url_send_log";
	
	/**
	 * API url for confirming a received log
	 */
	public static final String URL_CONFIRM_LOG = "url_confirm_log";
	
	/**
	 * Network properties
	 */
	private static Properties properties;
	
	/**
	 * Read the network configuration file
	 * @throws GameConfigurationInitializationException
	 */
	public static void initializeNetworkConfiguration() throws GameConfigurationInitializationException {
		try {
			properties = new Properties();
			properties.load(AndroidApplicationContextHolder.getInstance().getContext().getAssets().open(GameConfiguration.ASSETS_CONFIG_NETWORK_FILE_PATH));
		} catch (Exception e) {
			Log.e(GameConfiguration.TAG_ERROR_CONFIG_INIT, "Error initializing network configuration");
			throw new GameConfigurationInitializationException();
		}
	}

	public static Properties getProperties() {
		return properties;
	}
}
