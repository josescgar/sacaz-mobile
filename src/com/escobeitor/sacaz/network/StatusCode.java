package com.escobeitor.sacaz.network;

/**
 * SACAZ Server response status codes
 * @author Escobeitor
 *
 */
public class StatusCode {
	
	/**
	 * Response OK
	 */
	public static final int OK = 1;
	
	/**
	 * Response not OK
	 */
	public static final int KO = 2;
	
	/**
	 * Username required
	 */
	public static final int USERNAME_REQUIRED = 3;
	
	/**
	 * Google Authentication Key invalid
	 */
	public static final int INVALID_AUTH_KEY = 4;
	
	/**
	 * Game API Key invalid
	 */
	public static final int INVALID_API_KEY = 5;
	
	/**
	 * No registration id supplied
	 */
	public static final int REGISTRATION_ID_REQUIRED = 6;
	
	/**
	 * Supplied username not valid
	 */
	public static final int INVALID_USERNAME = 7;
	
	/**
	 * The game title is not available
	 */
	public static final int TITLE_NOT_AVAILABLE = 8;

}
