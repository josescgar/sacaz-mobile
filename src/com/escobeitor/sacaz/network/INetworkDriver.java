package com.escobeitor.sacaz.network;

import java.util.Map;

/**
 * SACAZ Network driver for communicating with the central server
 * @author Escobeitor
 *
 */
public interface INetworkDriver {
	
	/**
	 * Attach the current device google authentication key in order to uniquely
	 * identify the user of the app in the game server.
	 */
	public String attachAuthKey();
	
	/**
	 * Attach the server API key to certify that requests are being made from
	 * known devices and avoid third-party applications to modify the server database
	 * @return
	 */
	public String attachAPIKey();
	
	/**
	 * Attach the device registration ID to the request
	 * @return
	 */
	public String attachRegId();
	
	/**
	 * Makes a new request to an URL
	 * @param urlParameters
	 * 			Parameters attached to the URL (e.g.: http//www.example.com/index.php/param1/param2/paramN)
	 * @param payload
	 * 			Data included inside the Request
	 * @param url
	 * 			Target URL for the request
	 * @param callbackHolder
	 * 			Object expecting the response
	 * @param callbackMethod
	 * 			Method to be invoked when the request is completed. All callback methods must expect two parameters:
	 * 				1. Response (JSONObject)
	 * 				2. Request failed (Boolean)
	 * @param attatchRegistration
	 * 			Attach the device's registration ID
	 */
	public void doPost(String[] urlParameters, Map<String, String> payload, String url, Object callbackHolder, String callbackMethod, boolean attatchRegistration);
	
}
