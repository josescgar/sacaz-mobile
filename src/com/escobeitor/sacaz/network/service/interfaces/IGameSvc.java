package com.escobeitor.sacaz.network.service.interfaces;


/**
 * Game network service (communication with central server)
 * @author Escobeitor
 *
 */
public interface IGameSvc {
	
	/**
	 * Retrieves a list of unfinished games for the given user
	 * @param callbackHolder
	 * @param callbackMethod
	 */
	void retrieveUnfinishedGames(Object callbackHolder, String callbackMethod);
	
	/**
	 * Retrieves a single game
	 * @param gameId
	 * @param callbackHolder
	 * @param callbackMethod
	 */
	void retrieveGame(String gameId, Object callbackHolder, String callbackMethod);
	
	/**
	 * Creates a new game (started from the given lobby)
	 * @param lobbyId
	 * @param callbackHolder TODO
	 * @param callbackMethod TODO
	 * @return
	 */
	void create(String lobbyId, Object callbackHolder, String callbackMethod);
}
