package com.escobeitor.sacaz.network.service.interfaces;


/**
 * User network service
 * @author Escobeitor
 *
 */
public interface IUserSvc {
	
	/**
	 * Logs in the user into the game server as well as 
	 * in the GCM server for receiving messages. 
	 * This method will attach the current device registration id.
	 * @param callbackHolder Object with the callback function
	 * @param callbackMethod Callback function name
	 */
	void login(Object callbackHolder, String callbackMethod);
	
	/**
	 * Saves the given username into the server
	 * @param username the username
	 * @param callbackHolder Object with the callback function
	 * @param callbackMethod Callback function name
	 */
	void saveUsername(String username, Object callbackHolder, String callbackMethod);
	
}
