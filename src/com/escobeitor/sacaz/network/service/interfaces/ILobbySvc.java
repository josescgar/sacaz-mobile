package com.escobeitor.sacaz.network.service.interfaces;


/**
 * Lobby network service (communication with central server)
 * @author Escobeitor
 *
 */
public interface ILobbySvc {
	
	/**
	 * Creates a new lobby
	 * @param title
	 * @param level
	 * @param creatorFaction
	 * @param callbackHolder TODO
	 * @param callbackMethod TODO
	 */
	void createLobby(String title, String level, String creatorFaction, Object callbackHolder, String callbackMethod);
	
	/**
	 * Deletes a lobby
	 * @param lobbyId
	 * @param callbackHolder 
	 * @param callbackMethod 
	 */
	void deleteLobby(String lobbyId, Object callbackHolder, String callbackMethod);
	
	/**
	 * Joins the current User to the given lobby
	 * @param lobbyId
	 * @param username
	 * @param faction
	 * @param callbackHolder 
	 * @param callbackMethod 
	 * @return if the operation was successful
	 */
	void joinById(String lobbyId, String username, String faction, Object callbackHolder, String callbackMethod);
	
	/**
	 * Joins the current User to the given lobby
	 * @param lobbyTitle
	 * @return if the operation was successful
	 */
	boolean joinByTitle(String lobbyTitle);
	
	/**
	 * Get an available games list (paginated)
	 * @param page
	 * @param callbackHolder 
	 * @param callbackMethod 
	 */
	void getGameList(int page, Object callbackHolder, String callbackMethod);
	
	/**
	 * Get an available games list filtered by level and faction (paginated)
	 * @param page
	 * @param level
	 * @param faction
	 * @param callbackHolder
	 * @param callbackMethod
	 */
	void getGameList(int page, String level, String faction, Object callbackHolder, String callbackMethod);
	
	/**
	 * Get information about the specified lobby
	 * @param lobbyId
	 * @param callbackHolder
	 * @param callbackMethod
	 */
	void getLobby(String lobbyId, Object callbackHolder, String callbackMethod);
	
	/**
	 * Leave the given lobby
	 * @param lobbyId
	 * @param callbackHolder
	 * @param callbackMethod
	 */
	void leaveLobby(String lobbyId, Object callbackHolder, String callbackMethod);
}
