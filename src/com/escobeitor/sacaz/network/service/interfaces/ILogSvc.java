package com.escobeitor.sacaz.network.service.interfaces;

import com.escobeitor.sacaz.logging.Log;

/**
 * Log network service
 * @author Escobeitor
 *
 */
public interface ILogSvc {
	
	/**
	 * Retrieves and generates a log by its log id
	 * @param gameId
	 * @param callbackHolder TODO
	 * @param callbackMethod TODO
	 * @return a constructed log
	 */
	void retrieveLog(String gameId, Object callbackHolder, String callbackMethod);
	
	/**
	 * Sends a log to the server
	 * @param log
	 * @param gameId TODO
	 */
	void sendLog(Log log, String gameId);
	
	/**
	 * Confirm that the log has been received
	 * and processed correctly
	 * @param logId
	 */
	void confirmLog(String logId);
}
