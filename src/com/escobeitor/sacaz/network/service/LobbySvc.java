package com.escobeitor.sacaz.network.service;

import java.util.HashMap;
import java.util.Map;

import com.escobeitor.sacaz.network.NetworkConfiguration;
import com.escobeitor.sacaz.network.NetworkDriverFactory;
import com.escobeitor.sacaz.network.service.interfaces.ILobbySvc;

/**
 * Lobby network service implementation
 * @author Escobeitor
 *
 */
public class LobbySvc implements ILobbySvc {
	
	/**
	 * Singleton instance
	 */
	private static LobbySvc instance = null;
	
	/**
	 * Map to set the payload. Instatiate only once
	 */
	private final Map<String, String> payload = new HashMap<String, String>();
	
	/**
	 * Make constructor private
	 */
	private LobbySvc() {}
	
	/**
	 * Singleton getInstance method
	 * @return
	 */
	public static LobbySvc getInstance() {
		if(instance == null) {
			instance = new LobbySvc();
		}
		
		return instance;
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.ILobbySvc#createLobby(java.lang.String, java.lang.String, String, Object, String)
	 */
	@Override
	public void createLobby(String title, String level, String creatorFaction, Object callbackHolder, String callbackMethod) {
		
		payload.clear();
		payload.put("title", title);
		payload.put("level", level);
		payload.put("faction", creatorFaction);
		
		NetworkDriverFactory.getDriver().doPost(null, 
				payload, 
				NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_CREATE_GAME_KEY), 
				callbackHolder, 
				callbackMethod,
				false);
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.ILobbySvc#deleteLobby(java.lang.String, java.lang.Object, java.lang.String)
	 */
	@Override
	public void deleteLobby(String lobbyId, Object callbackHolder, String callbackMethod) {
		NetworkDriverFactory.getDriver().doPost(new String[]{lobbyId},
				null, 
				NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_REMOVE_LOBBY_KEY), 
				callbackHolder, 
				callbackMethod, 
				false);
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.ILobbySvc#joinById(java.lang.String, java.lang.Object, java.lang.String)
	 */
	@Override
	public void joinById(String lobbyId, String username, String faction, Object callbackHolder, String callbackMethod) {
		payload.clear();
		payload.put("username", username);
		payload.put("faction", faction);
		
		NetworkDriverFactory.getDriver().doPost(new String[]{lobbyId},
				payload,
				NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_JOIN_LOBBY_KEY),
				callbackHolder,
				callbackMethod,
				false);
	}

	@Override
	public boolean joinByTitle(String lobbyTitle) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.ILobbySvc#getGameList(int, java.lang.Object, java.lang.String)
	 */
	@Override
	public void getGameList(int page, Object callbackHolder, String callbackMethod) {
		
		NetworkDriverFactory.getDriver().doPost(new String[]{Integer.toString(page)}, 
				null, 
				NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_MY_LOBBIES_KEY), 
				callbackHolder, 
				callbackMethod, 
				false);
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.ILobbySvc#getGameList(int, java.lang.String, java.lang.String, java.lang.Object, java.lang.String)
	 */
	@Override
	public void getGameList(int page, String level, String faction, Object callbackHolder, String callbackMethod) {

		payload.clear();
		payload.put("level", level);
		payload.put("faction", faction);
		
		NetworkDriverFactory.getDriver().doPost(new String[]{Integer.toString(page)},
				payload,
				NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_SEARCH_LOBBIES_KEY), 
				callbackHolder, 
				callbackMethod, 
				false);
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.ILobbySvc#getLobby(java.lang.String, java.lang.Object, java.lang.String)
	 */
	@Override
	public void getLobby(String lobbyId, Object callbackHolder, String callbackMethod) {
		NetworkDriverFactory.getDriver().doPost(new String[]{lobbyId}, 
				null, 
				NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_GET_LOBBY_KEY), 
				callbackHolder,
				callbackMethod,
				false);
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.ILobbySvc#leaveLobby(java.lang.String, java.lang.Object, java.lang.String)
	 */
	@Override
	public void leaveLobby(String lobbyId, Object callbackHolder, String callbackMethod) {
		NetworkDriverFactory.getDriver().doPost(new String[]{lobbyId},
				null, 
				NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_LEAVE_LOBBY_KEY), 
				callbackHolder, 
				callbackMethod, 
				false);
	}

}
