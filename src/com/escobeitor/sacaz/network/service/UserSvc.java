package com.escobeitor.sacaz.network.service;

import com.escobeitor.sacaz.network.NetworkConfiguration;
import com.escobeitor.sacaz.network.NetworkDriverFactory;
import com.escobeitor.sacaz.network.service.interfaces.IUserSvc;

/**
 * User network service implementation.
 * Singleton.
 * @author Escobeitor
 *
 */
public class UserSvc implements IUserSvc {
	
	/**
	 * Singleton instance
	 */
	private static UserSvc instance = null;

	/**
	 * Make constructor private
	 */
	private UserSvc() {}
	
	/**
	 * Singleton getInstance method
	 * @return
	 */
	public static UserSvc getInstance() {
		if(instance == null) {
			instance = new UserSvc();
		}
		
		return instance;
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.IUserSvc#login(Object, String)
	 */
	@Override
	public void login(Object callbackHolder, String callbackMethod) {

		NetworkDriverFactory.getDriver().doPost(null, 
				null, 
				NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_LOGIN_KEY), 
				callbackHolder, 
				callbackMethod, 
				true);
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.IUserSvc#saveUsername(java.lang.String, java.lang.Object, java.lang.String)
	 */
	@Override
	public void saveUsername(String username, Object callbackHolder, String callbackMethod) {
		NetworkDriverFactory.getDriver().doPost(new String[]{username}, 
				null, 
				NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_SAVE_USERNAME_KEY), 
				callbackHolder, 
				callbackMethod, 
				false);
	}

}
