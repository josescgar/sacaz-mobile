package com.escobeitor.sacaz.network.service;

import java.util.HashMap;
import java.util.Map;

import com.escobeitor.sacaz.logging.Log;
import com.escobeitor.sacaz.network.NetworkConfiguration;
import com.escobeitor.sacaz.network.NetworkDriverFactory;
import com.escobeitor.sacaz.network.service.interfaces.ILogSvc;
import com.google.gson.GsonBuilder;

/**
 * Log network service implementation
 * @author Escobeitor
 *
 */
public class LogSvc implements ILogSvc {
	
	/**
	 * Singleton instance
	 */
	private static LogSvc instance = null;
	
	/**
	 * Make constructor private
	 */
	private LogSvc() {}
	
	/**
	 * Singleton getInstance method
	 * @return
	 */
	public static LogSvc getInstance() {
		if(instance == null) {
			instance = new LogSvc();
		}
		
		return instance;
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.ILogSvc#retrieveLog(java.lang.String, Object, String)
	 */
	@Override
	public void retrieveLog(String gameId, Object callbackHolder, String callbackMethod) {
		NetworkDriverFactory.getDriver().doPost(new String[]{gameId}, 
				null,
				NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_RETRIEVE_LOG),
				callbackHolder, 
				callbackMethod,
				false);
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.ILogSvc#sendLog(com.escobeitor.sacaz.logging.Log, String)
	 */
	@Override
	public void sendLog(Log log, String gameId) {
		Map<String, String> payload = new HashMap<String, String>();
		
		payload.put("logData", new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(log, Log.class));
		
		NetworkDriverFactory.getDriver().doPost(new String[]{gameId}, 
				payload, 
				NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_SEND_LOG), 
				null, 
				null, 
				false);
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.ILogSvc#confirmLog(java.lang.String)
	 */
	@Override
	public void confirmLog(String logId) {
		NetworkDriverFactory.getDriver().doPost(new String[]{logId},
				null,
				NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_CONFIRM_LOG),
				null,
				null,
				false);
	}

}
