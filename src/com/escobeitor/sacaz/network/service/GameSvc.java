package com.escobeitor.sacaz.network.service;

import com.escobeitor.sacaz.network.NetworkConfiguration;
import com.escobeitor.sacaz.network.NetworkDriverFactory;
import com.escobeitor.sacaz.network.service.interfaces.IGameSvc;

/**
 * Game network service
 * @author Escobeitor
 *
 */
public class GameSvc implements IGameSvc {
	
	/**
	 * Singleton instance
	 */
	private static GameSvc instance = null;
	
	/**
	 * Make constructor private
	 */
	private GameSvc() {}
	
	/**
	 * Singleton getInstance method
	 * @return
	 */
	public static GameSvc getInstance() {
		if(instance == null) {
			instance = new GameSvc();
		}
		
		return instance;
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.IGameSvc#retrieveUnfinishedGames(Object, String)
	 */
	@Override
	public void retrieveUnfinishedGames(Object callbackHolder, String callbackMethod) {
		NetworkDriverFactory.getDriver().doPost(null,
			null,
			NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_MY_GAMES_LIST), 
			callbackHolder, 
			callbackMethod, 
			false);
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.IGameSvc#retrieveGame(java.lang.String, java.lang.Object, java.lang.String)
	 */
	@Override
	public void retrieveGame(String gameId, Object callbackHolder, String callbackMethod) {
		NetworkDriverFactory.getDriver().doPost(new String[]{gameId},
				null,
				NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_RETRIEVE_GAME),
				callbackHolder,
				callbackMethod,
				false);
	}

	/**
	 * @see com.escobeitor.sacaz.network.service.interfaces.IGameSvc#create(java.lang.String, java.lang.Object, java.lang.String)
	 */
	@Override
	public void create(String lobbyId, Object callbackHolder, String callbackMethod) {
		NetworkDriverFactory.getDriver().doPost(
				new String[]{lobbyId}, 
				null, 
				NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.URL_START_GAME_KEY), 
				callbackHolder, 
				callbackMethod, 
				false);
	}

}
