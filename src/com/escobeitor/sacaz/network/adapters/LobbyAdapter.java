package com.escobeitor.sacaz.network.adapters;

import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.enums.EFaction;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.GameContextHolder;

/**
 * List view adapter for game search list
 * @author Escobeitor
 *
 */
public class LobbyAdapter extends ArrayAdapter<JSONObject> {

	/**
	 * Android context
	 */
	private Context context;
	
	/**
	 * List values
	 */
	private List<JSONObject> values;
	
	/**
	 * Constructor
	 * @param context
	 * @param resource
	 * @param objects
	 */
	public LobbyAdapter(Context context, List<JSONObject> objects) {
		super(context, R.layout.find_games, objects);
		this.context = context;
		this.values = objects;
	}

	/**
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View rowView;
		if(convertView == null) {
			final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.lobby_list_item, parent, false);
		} else {
			rowView = convertView;
		}
		
		final TextView title = (TextView) rowView.findViewById(R.id.lobby_list_item_title);
		final TextView status = (TextView) rowView.findViewById(R.id.lobby_list_item_status);
		final TextView faction = (TextView) rowView.findViewById(R.id.lobby_list_item_faction);
		final TextView map = (TextView) rowView.findViewById(R.id.lobby_list_item_map);
		final TextView rowId = (TextView) rowView.findViewById(R.id.lobby_list_item_id);
		
		try {
			JSONObject object = values.get(position);
			
			String id = object.getJSONObject("_id").getString("$id");
			rowId.setText(id);
			
			title.setText(object.getString("title"));
			map.setText(object.getString("map"));
			JSONObject player1 = object.getJSONObject("player1");
			
			JSONObject player2 = null;
			if(object.has("player2")) {
				player2 = object.getJSONObject("player1");
			}
			
			if(player1.getString("userId").equals(GameContextHolder.getInstance().user.id)) {
				faction.setText(player1.getString("faction"));
				if(player2 == null) {
					status.setText(context.getString(R.string.find_games_status_waiting_another));
					status.setTextColor(Color.RED);
				} else {
					status.setText(context.getString(R.string.find_games_status_ready));
					status.setTextColor(Color.GREEN);
				}
			} else {

				final String opponentFaction = player2 == null ? player1.getString("faction") : player2.getString("faction");
				if(opponentFaction.equals(EFaction.CRIMINALS.name())) {
					faction.setText(EFaction.SURVIVORS.name());
				} else {
					faction.setText(EFaction.CRIMINALS.name());
				}
				
				if(player2 != null) {
					status.setText(context.getString(R.string.find_games_status_waiting_start));
					status.setTextColor(Color.YELLOW);
				}
			}
			
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error creating game list");
			AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_getting_lobbies_list), Toast.LENGTH_LONG);
		}
		
		return rowView;
	}
	
	
}
