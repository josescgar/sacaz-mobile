package com.escobeitor.sacaz.network.adapters;

import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.GameContextHolder;

public class MyGamesAdapter extends ArrayAdapter<JSONObject> {
	
	/**
	 * Space separator for string construction
	 */
	private static final char SPACE_SEPARATOR = ' ';

	/**
	 * Android context
	 */
	private Context context;
	
	/**
	 * List values
	 */
	private List<JSONObject> values;
	
	/**
	 * Logged in user id
	 */
	final private String userId = GameContextHolder.getInstance().user.id;
	
	/**
	 * Constructor
	 * @param context
	 * @param resource
	 * @param objects
	 */
	public MyGamesAdapter(Context context, List<JSONObject> objects) {
		super(context, R.layout.my_games, objects);
		this.context = context;
		this.values = objects;
	}
	
	/**
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View rowView;
		if(convertView == null) {
			final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.my_games_list_item, parent, false);
		} else {
			rowView = convertView;
		}
		
		final TextView id = (TextView) rowView.findViewById(R.id.my_games_item_id);
		final TextView map = (TextView) rowView.findViewById(R.id.my_games_item_map);
		final TextView turn = (TextView) rowView.findViewById(R.id.my_games_item_turn);
		final TextView currentTurn = (TextView) rowView.findViewById(R.id.my_games_item_currentTurn);
		final TextView title = (TextView) rowView.findViewById(R.id.my_games_item_title);
		final TextView opponent = (TextView) rowView.findViewById(R.id.my_games_item_opponent);
		final TextView info = (TextView) rowView.findViewById(R.id.my_games_item_info);
		final TextView status = (TextView) rowView.findViewById(R.id.my_games_item_status);

		
		//Parse JSONObjects
		try {
			
			JSONObject element = values.get(position);
			
			//Set title and hidden fields
			id.setText(element.getJSONObject("_id").getString("$id"));
			title.setText(element.getString("title"));
			map.setText(element.getString("map"));
			turn.setText(element.getString("turns"));
			
			String currentServerTurn = element.getString("currentTurn");
			currentTurn.setText(currentServerTurn);
			
			//Set opponent text
			String player1UserId = element.getJSONObject("player1").getString("userId");
			StringBuffer opponentText = new StringBuffer(context.getString(R.string.my_games_opponent_label)).append(SPACE_SEPARATOR)
					.append(userId.equals(player1UserId) ? element.getJSONObject("player2").getString("username") :element.getJSONObject("player1").getString("username"));
			
			opponent.setText(opponentText);
			
			//Set info string
			StringBuffer infoText = new StringBuffer(element.getString("map")).append(SPACE_SEPARATOR)
					.append(element.getString("turns")).append(SPACE_SEPARATOR).append(context.getString(R.string.my_games_turns_label)).append(SPACE_SEPARATOR);
			
			info.setText(infoText);
			
			//Set game status
			if(userId.equals(currentServerTurn)) {
				status.setTextColor(Color.GREEN);
				status.setText(R.string.my_games_current_turn_mine);
			} else {
				status.setTextColor(Color.RED);
				status.setText(R.string.my_games_current_turn_his);
			}
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error creating my games list: " + e.getMessage());
			AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_my_games_list), Toast.LENGTH_LONG);
		}
		
		return rowView;
	}
}
