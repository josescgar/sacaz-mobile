package com.escobeitor.sacaz.network;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.activity.GameActivity;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;

/**
 * SACAZ Network communication driver for HTTP Protocol
 * @author Escobeitor
 *
 */
public class HttpDriver extends AbstractNetworkDriver {

	/**
	 * @see com.escobeitor.sacaz.network.INetworkDriver#doPost(java.lang.String[], java.util.Map, java.lang.String, Object, String, boolean)
	 */
	@Override
	public void doPost(String[] urlParameters, Map<String, String> payload, String url, Object callbackHolder, String callbackMethod, boolean attatchRegistration) {
		
		class RequestTask extends AsyncTask<Object, Void, String> {
			
			/**
			 * Callback method holder
			 */
			private Object callbackHolder;
			
			/**
			 * Callback method
			 */
			private String callbackMethod;
			
			/**
			 * Progress indicator dialog
			 */
			private ProgressDialog dialog;
			
			/**
			 * Flag to indicate if the request failed
			 */
			private boolean requestFailed = false;

			/**
			 * @see android.os.AsyncTask#onPreExecute()
			 */
			@Override
			protected void onPreExecute() {
				((GameActivity) AndroidApplicationContextHolder.getInstance().getContext()).runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						dialog = new ProgressDialog(AndroidApplicationContextHolder.getInstance().getContext());
						dialog.setMessage(AndroidApplicationContextHolder.getInstance().getContext().getString(R.string.net_connecting_to_server));
						dialog.setCancelable(false);
						dialog.show();
					}
				});
				
			}

			/**
			 * @see android.os.AsyncTask#doInBackground(java.lang.Object[])
			 */
			@SuppressWarnings("unchecked")
			@Override
			protected String doInBackground(Object... params) {
				try {
					//Set timeout
					HttpParams httpParameters = new BasicHttpParams();
					int timeout = Integer.parseInt(NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.TIMEOUT_KEY));
					HttpConnectionParams.setConnectionTimeout(httpParameters, timeout);
					HttpConnectionParams.setSoTimeout(httpParameters, timeout);
					
					HttpClient httpclient = new DefaultHttpClient(httpParameters);
					String[] urlParameters = (String[]) params[0];
					Map<String, String> payload = (Map<String, String>) params[1];
					this.callbackHolder = params[3];
					this.callbackMethod = (String) params[4];
					boolean attachRegId = (Boolean) params[5];
					
					//Attach url parameters
					StringBuffer url = new StringBuffer(NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.BASE_URL_KEY));
					url.append((String) params[2]);
					
					if(urlParameters != null) {
						for(int i = 0; i < urlParameters.length; i++) {
							url.append('/').append(urlParameters[i]);
						}
					}
					
					HttpPost request = new HttpPost(url.toString());
					
					//Add request payload
					List<NameValuePair> parameters = new ArrayList<NameValuePair>();
					parameters.add(new BasicNameValuePair("authKey", attachAuthKey()));
					parameters.add(new BasicNameValuePair("apiKey", attachAPIKey()));
					
					if(attachRegId) {
						parameters.add(new BasicNameValuePair("regId", attachRegId()));
					}
					
					if(payload != null) {
						for(Entry<String, String> entry : payload.entrySet()) {
							parameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
						}
					}
					
					request.setEntity(new UrlEncodedFormEntity(parameters));
					
					Log.d(GameConfiguration.TAG_DEBUG, "Making HTTP request to: " + request.getURI());
					
				    HttpResponse response = httpclient.execute(request);

				    StatusLine statusLine = response.getStatusLine();
				    if(statusLine.getStatusCode() == HttpStatus.SC_OK){
				        ByteArrayOutputStream out = new ByteArrayOutputStream();
				        response.getEntity().writeTo(out);
				        out.close();
				        Log.d(GameConfiguration.TAG_DEBUG, "HTTP Response: " + out.toString());
				        return out.toString();
				    } else{
				        response.getEntity().getContent().close();
				        throw new IOException(statusLine.getReasonPhrase());
				    }
				    
				} catch (Exception e) {
					Log.e(GameConfiguration.TAG_NETWORK_REQUEST_ERROR, "Error making HTTP request: " + e.getMessage());
					this.requestFailed = true;
					return null;
				}
			}

			/**
			 * Call the callback method with the response result
			 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
			 */
			@Override
			protected void onPostExecute(String result) {
				
				Context context = AndroidApplicationContextHolder.getInstance().getContext();
				
				if(this.requestFailed) {
					AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_http_request), Toast.LENGTH_LONG);
				}
				
				if(callbackHolder != null) {
				
					try {
						Method method = callbackHolder.getClass().getMethod(callbackMethod, JSONObject.class, boolean.class);
						method.invoke(callbackHolder, new JSONObject(result), this.requestFailed);
					} catch (Exception e) {
						Log.e(GameConfiguration.TAG_NETWORK_REQUEST_ERROR, "Error invoking callback method from HTTP request: " + e.getMessage());
						AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_http_request), Toast.LENGTH_LONG);
					}
				
				}
				
				dialog.dismiss();		
			}
		}
		
		new RequestTask().execute(urlParameters, payload, url, callbackHolder, callbackMethod, attatchRegistration);
	}

}
