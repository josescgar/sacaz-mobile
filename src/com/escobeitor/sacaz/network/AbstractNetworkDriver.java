package com.escobeitor.sacaz.network;

import java.io.IOException;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Abstract class with common functionallity for all
 * network drivers protocols
 * @author Escobeitor
 *
 */
public abstract class AbstractNetworkDriver implements INetworkDriver {

	/**
	 * @see com.escobeitor.sacaz.network.INetworkDriver#attachAuthKey()
	 */
	@Override
	public String attachAuthKey() {
		
		Context context = AndroidApplicationContextHolder.getInstance().getContext();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		
		String token = null;
//		String token = prefs.getString(GameConfiguration.SHARED_PREFS_AUTH_TOKEN_KEY, null);
//		
//		if(token == null) {

		    String accountName = prefs.getString(GameConfiguration.SHARED_PREFS_USER_ACCOUNT_KEY, null);

		    try {
		    	token = GoogleAuthUtil.getToken(context, accountName, NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.AUTH_SCOPE));
		    	prefs.edit().putString(GameConfiguration.SHARED_PREFS_AUTH_TOKEN_KEY, token).commit();
		    } catch(Exception e) {
		    	Log.e(GameConfiguration.TAG_NETWORK_REQUEST_ERROR, "Error obtaining Google Auth token");
		    	AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_auth_token), Toast.LENGTH_LONG);
		    }
		//}
	    
	    return token;
	}

	/**
	 * @see com.escobeitor.sacaz.network.INetworkDriver#attachAPIKey()
	 */
	@Override
	public String attachAPIKey() {
		return NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.API_KEY);
	}

	/**
	 * @see com.escobeitor.sacaz.network.INetworkDriver#attachRegId()
	 */
	@Override
	public String attachRegId() {
		String regId = null;
		Context context = AndroidApplicationContextHolder.getInstance().getContext();
		try {
			GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
			regId = gcm.register(NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.GCM_SENDER));
		} catch (IOException e) {
			Log.e(GameConfiguration.TAG_NETWORK_REQUEST_ERROR, "Error registering device in GCM");
			AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_gcm_registration), Toast.LENGTH_LONG);
		}
		
		return regId;
	}

}
