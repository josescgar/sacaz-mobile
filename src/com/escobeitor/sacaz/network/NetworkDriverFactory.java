package com.escobeitor.sacaz.network;

import android.util.Log;

import com.escobeitor.sacaz.global.GameConfiguration;

/**
 * Returns an instance for the network driver.
 * Protocol driver selection is performed here.
 * @author Escobeitor
 *
 */
public class NetworkDriverFactory {
	
	/**
	 * Package containing network classes
	 */
	private static final String NETWORK_PACKAGE = "com.escobeitor.sacaz.network";
	/**
	 * The network driver for the current protocol
	 */
	private static INetworkDriver driver = null;
	
	/**
	 * Get an instance of the network driver
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static INetworkDriver getDriver() {
		try {
			
			if(driver == null) {
				StringBuffer protocol = new StringBuffer(NETWORK_PACKAGE)
						.append('.')
						.append(NetworkConfiguration.getProperties().getProperty(NetworkConfiguration.PROTOCOL_KEY))
						.append("Driver");
				Class<INetworkDriver> driverClass = (Class<INetworkDriver>) Class.forName(protocol.toString());
				driver = driverClass.newInstance();
			}
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR_CONFIG_INIT, "Error creating network driver");
			return null;
		}
		
		return driver;
	}
}
