package com.escobeitor.sacaz.abilities;

/**
 * Ability that doesn't require to interact with the user via a sensor
 * @author Escobeitor
 *
 */
public abstract class StandardAbility extends Ability {

	/**
	 * @see com.escobeitor.sacaz.abilities.interfaces.IAbility#getMeasurement()
	 */
	@Override
	public void getMeasurement() {
		// Do nothing
		return;
	}
	
}
