package com.escobeitor.sacaz.abilities;

import com.escobeitor.sacaz.abilities.interfaces.IAbility;
import com.escobeitor.sacaz.exceptions.NotEnoughResourcesException;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.model.game.Player;

/**
 * @author Escobeitor
 *
 */
public abstract class Ability implements IAbility {
	
	/**
	 * Resources cost of executing the ability.
	 */
	public int[] cost;
	
	/**
	 * Max number of uses of the ability.
	 */
	public int numUses;
	
	/**
	 * Player using the ability.
	 */
	public Player player;
	
	/**
	 * Character using the ability.
	 */
	public Character character;


	/**
	 * @see com.escobeitor.sacaz.abilities.interfaces.IAbility#execute(com.escobeitor.sacaz.model.game.Player, java.lang.Character)
	 */
	@Override
	public void execute(Player player, Character character) {
		
		this.player = player;
		
		this.character = character;
		
		try {
			substractResources();
		} catch (NotEnoughResourcesException e) {
			// TODO Inform the user that he doesn't have enough resources
		}
		
		selectTarget();
		
		validateTarget();
		
		apply();
	}

	/**
	 * @throws NotEnoughResourcesException 
	 * @see com.escobeitor.sacaz.abilities.interfaces.IAbility#substractResources()
	 */
	@Override
	public void substractResources() throws NotEnoughResourcesException {
		this.player.substractResources(cost[GameConfiguration.COST_ELECTRICITY], cost[GameConfiguration.COST_FOOD], cost[GameConfiguration.COST_MATERIALS], cost[GameConfiguration.COST_WATER]);
		
		this.numUses--;
	}
}
