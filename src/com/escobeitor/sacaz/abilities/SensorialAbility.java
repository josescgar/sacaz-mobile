package com.escobeitor.sacaz.abilities;

import com.escobeitor.sacaz.sensor.ISensor;

/**
 * Ability that requires to interact with the user using one or more sensors
 * @author Escobeitor
 *
 */
public abstract class SensorialAbility extends Ability {
	
	/**
	 * Sensors required to execute the ability
	 */
	public ISensor[] sensorsRequired;

}
