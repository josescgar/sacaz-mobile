package com.escobeitor.sacaz.abilities;

import com.escobeitor.sacaz.enums.EAbilityCost;
import com.escobeitor.sacaz.global.GameConfiguration;

/**
 * Surgery ability
 * @author Escobeitor
 *
 */
public class Surgery extends SensorialAbility {

	/**
	 * Public constructor
	 */
	public Surgery() {
		int[] cost = new int[4];
		cost[GameConfiguration.COST_ELECTRICITY] = EAbilityCost.SURGERY.electricityCost;
		cost[GameConfiguration.COST_FOOD] = EAbilityCost.SURGERY.foodCost;
		cost[GameConfiguration.COST_MATERIALS] = EAbilityCost.SURGERY.materialsCost;
		cost[GameConfiguration.COST_WATER] = EAbilityCost.SURGERY.waterCost;
		
		this.cost = cost;
		this.numUses = 1;
	}
	
	/**
	 * @see com.escobeitor.sacaz.abilities.interfaces.IAbility#reset()
	 */
	@Override
	public void reset() {
		this.numUses = 1;
	}

	/**
	 * @see com.escobeitor.sacaz.abilities.interfaces.IAbility#selectTarget()
	 */
	@Override
	public void selectTarget() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see com.escobeitor.sacaz.abilities.interfaces.IAbility#validateTarget()
	 */
	@Override
	public void validateTarget() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see com.escobeitor.sacaz.abilities.interfaces.IAbility#getMeasurement()
	 */
	@Override
	public void getMeasurement() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see com.escobeitor.sacaz.abilities.interfaces.IAbility#apply()
	 */
	@Override
	public void apply() {
		// TODO Auto-generated method stub
		
	}

}
