package com.escobeitor.sacaz.abilities.interfaces;

import com.escobeitor.sacaz.exceptions.NotEnoughResourcesException;
import com.escobeitor.sacaz.model.game.Player;

/**
 * Template method. All abilities must implement
 * this interface
 * @author Escobeitor
 *
 */
public interface IAbility {
	
	/**
	 * Reset the ability counters
	 */
	void reset();
	
	/**
	 * Execute the ability. Template method.
	 * @param player
	 * @param character
	 */
	void execute(Player player, Character character);
	
	/**
	 * Substract all resources needed for execution
	 * @throws NotEnoughResourcesException 
	 */
	void substractResources() throws NotEnoughResourcesException;
	
	/**
	 * Select the ability target (if any)
	 */
	void selectTarget();
	
	/**
	 * Validates the selected target
	 */
	void validateTarget();
	
	/**
	 * Get a sensor(s) measurement(s) (if any)
	 */
	void getMeasurement();
	
	/**
	 * Apply the effects of the ability
	 */
	void apply();
}
