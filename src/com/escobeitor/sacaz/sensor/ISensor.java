package com.escobeitor.sacaz.sensor;

/**
 * Sacaz sensors common interface
 * @author Escobeitor
 *
 */
public interface ISensor {
	
	/**
	 * Checks if the sensor is available at the moment
	 * @return
	 */
	boolean isAvailable();
	
	/**
	 * Probes the sensor and returns a measurement
	 * @return
	 */
	public Object getMeasurement();
	
	/**
	 * Calitrabates the sensor
	 */
	public void calibrate();
}
