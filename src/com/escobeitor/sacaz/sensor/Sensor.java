package com.escobeitor.sacaz.sensor;

public abstract class Sensor implements ISensor {

	/**
	 * Sensor availability
	 */
	private boolean present;
	
	/**
	 * Sensor calibration values
	 */
	private double[] calibration;

	public boolean isPresent() {
		return present;
	}

	public void setPresent(boolean present) {
		this.present = present;
	}

	public double[] getCalibration() {
		return calibration;
	}

	public void setCalibration(double[] calibration) {
		this.calibration = calibration;
	}

}
