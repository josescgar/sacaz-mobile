package com.escobeitor.sacaz.exceptions;

/**
 * General SACAZ game exception
 * @author Escobeitor
 *
 */
public class SacazControlledException extends Exception {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

}
