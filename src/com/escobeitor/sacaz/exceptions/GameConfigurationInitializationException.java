package com.escobeitor.sacaz.exceptions;

/**
 * Exception when problems occur during game configuration initialization
 * @author Escobeitor
 *
 */
public class GameConfigurationInitializationException extends
		SacazControlledException {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

}
