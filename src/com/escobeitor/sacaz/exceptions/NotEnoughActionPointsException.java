package com.escobeitor.sacaz.exceptions;

/**
 * Exception thrown when the player doesn't have enough actions points for
 * an specific action
 * @author Escobeitor
 *
 */
public class NotEnoughActionPointsException extends SacazControlledException {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

}
