package com.escobeitor.sacaz.exceptions;

/**
 * Not enough resources left
 * @author Escobeitor
 *
 */
public class NotEnoughResourcesException extends SacazControlledException {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

}
