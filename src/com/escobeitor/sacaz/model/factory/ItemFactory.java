package com.escobeitor.sacaz.model.factory;

import java.util.Stack;

import com.escobeitor.sacaz.enums.EElement;
import com.escobeitor.sacaz.model.game.Item;
import com.escobeitor.sacaz.model.interfaces.IBuildable;

/**
 * Items factory
 * @author Escobeitor
 *
 */
public class ItemFactory implements IElementFactory {
	
	private final Stack<IBuildable> itemPool;
	
	/**
	 * Singleton instance
	 */
	private static ItemFactory instance = null;
	
	/**
	 * Private constructor to avoid new instances
	 */
	private ItemFactory() {
		this.itemPool = new Stack<IBuildable>();
	}
	
	/**
	 * Singleton method
	 * @return
	 */
	public static ItemFactory getInstance() {
		if(instance == null) {
			instance = new ItemFactory();
		}
		
		return instance;
	}
	
	/**
	 * @see com.escobeitor.sacaz.model.factory.IElementFactory#createElement(com.escobeitor.sacaz.enums.EElement)
	 */
	@Override
	public IBuildable createElement(EElement element) {
		IBuildable object = this.itemPool.isEmpty() ? new Item(element) : this.itemPool.pop();
		return object;
	}

	/**
	 * @see com.escobeitor.sacaz.model.factory.IElementFactory#recycleElement(com.escobeitor.sacaz.model.interfaces.IBuildable)
	 */
	@Override
	public void recycleElement(IBuildable element) {
		this.itemPool.push(element);
	}

}
