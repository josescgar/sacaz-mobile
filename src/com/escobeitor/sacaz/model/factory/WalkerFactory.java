package com.escobeitor.sacaz.model.factory;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import com.escobeitor.sacaz.enums.EElement;
import com.escobeitor.sacaz.model.game.Character;
import com.escobeitor.sacaz.model.game.NPC;
import com.escobeitor.sacaz.model.interfaces.IBuildable;

/**
 * Characters factory
 * @author Escobeitor
 *
 */
public class WalkerFactory implements IElementFactory {
	
	/**
	 * Character object pool
	 */
	private final Map<String, Stack<IBuildable>> characterPool;
	
	/**
	 * NPC object pool
	 */
	private final Map<String, Stack<IBuildable>> npcPool;
	
	/**
	 * Singleton instance
	 */
	private static WalkerFactory instance = null;
	
	/**
	 * Private constructor to avoid new instances
	 */
	private WalkerFactory() {
		this.characterPool = new HashMap<String, Stack<IBuildable>>();
		this.npcPool = new HashMap<String, Stack<IBuildable>>();
	}
	
	/**
	 * Singleton method
	 * @return
	 */
	public static WalkerFactory getInstance() {
		if(instance == null) {
			instance = new WalkerFactory();
		}
		
		return instance;
	}
	
	/**
	 * @see com.escobeitor.sacaz.model.factory.IElementFactory#createElement(com.escobeitor.sacaz.enums.EElement)
	 */
	@Override
	public IBuildable createElement(EElement element) {
		IBuildable object = null;
		
		switch(element.type) {
			case CHARACTER:
				if(characterPool.get(element.name()) == null || characterPool.get(element.name()).isEmpty()) {
					return new Character(element);
				}
				
				object = characterPool.get(element.name()).pop();	
				break;
			case NPC:
				if(npcPool.get(element.name()) == null || npcPool.get(element.name()).isEmpty()) {
					return new NPC(element);
				}
				
				object = npcPool.get(element.name()).pop();
				break;
			default:
				break;
		}
		
		object.resetElement(element);
		return object;
	}

	/**
	 * @see com.escobeitor.sacaz.model.factory.IElementFactory#recycleElement(com.escobeitor.sacaz.model.interfaces.IBuildable)
	 */
	@Override
	public void recycleElement(IBuildable element) {
		
		Stack<IBuildable> stack = null;
		if(element instanceof Character) {
			stack = characterPool.get(((Character) element).type.name());
			
			if(stack == null) {
				stack = new Stack<IBuildable>();
				characterPool.put(((Character) element).type.name(), stack);
			}

		}
		
		if(element instanceof NPC) {
			stack = npcPool.get(((NPC) element).type.name());
			
			if(stack == null) {
				stack = new Stack<IBuildable>();
				npcPool.put(((NPC) element).type.name(), stack);
			}
		}
		
		stack.push(element);
	}

}
