package com.escobeitor.sacaz.model.factory;

import java.util.Stack;

import com.escobeitor.sacaz.enums.EElement;
import com.escobeitor.sacaz.model.game.Weapon;
import com.escobeitor.sacaz.model.interfaces.IBuildable;

/**
 * Weapons factory
 * @author Escobeitor
 *
 */
public class WeaponFactory implements IElementFactory {
	
	/**
	 * Weapon object pool
	 */
	private final Stack<IBuildable> weaponPool;
	
	/**
	 * Singleton instance
	 */
	private static WeaponFactory instance = null;
	
	/**
	 * Private constructor to avoid new instances
	 */
	private WeaponFactory() {
		this.weaponPool = new Stack<IBuildable>();
	}
	
	/**
	 * Singleton method
	 * @return
	 */
	public static WeaponFactory getInstance() {
		if(instance == null) {
			instance = new WeaponFactory();
		}
		
		return instance;
	}
	
	/**
	 * @see com.escobeitor.sacaz.model.factory.IElementFactory#createElement(com.escobeitor.sacaz.enums.EElement)
	 */
	@Override
	public IBuildable createElement(EElement element) {
		if(this.weaponPool.isEmpty()) {
			return new Weapon(element);
		}
		
		IBuildable object = this.weaponPool.pop();
		object.resetElement(element);
		return object;
	}

	/**
	 * @see com.escobeitor.sacaz.model.factory.IElementFactory#recycleElement(com.escobeitor.sacaz.model.interfaces.IBuildable)
	 */
	@Override
	public void recycleElement(IBuildable element) {
		this.weaponPool.push(element);
	}

}
