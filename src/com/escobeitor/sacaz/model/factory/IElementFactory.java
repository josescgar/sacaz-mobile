package com.escobeitor.sacaz.model.factory;

import com.escobeitor.sacaz.enums.EElement;
import com.escobeitor.sacaz.model.interfaces.IBuildable;

/**
 * Abstract Factory Interface. Uses singleton.
 * @author Escobeitor
 *
 */
public interface IElementFactory {
	
	/**
	 * Creates a new element
	 * @param element
	 * @return
	 */
	IBuildable createElement(EElement element);
	
	/**
	 * Recycle a no longer used element (object pooling)
	 * @param element
	 */
	void recycleElement(IBuildable element);
}
