package com.escobeitor.sacaz.model.game;

/**
 * Player's ranger
 * @author Escobeitor
 *
 */
public class Ranger {
	
	/**
	 * Ranger is active
	 */
	public boolean active;
	
	/**
	 * Ranger resources (food, materials and water) allocation
	 */
	public int[] allocation;
	
	/**
	 * Managa ranger resources game dialog
	 */
	public void manageRangerResources() {
		//TODO
	}
	
	/**
	 * Change the resources allocation of the ranger
	 * @param food
	 * @param materials
	 * @param water
	 */
	public void updateRangerResources(int food, int materials, int water) {
		//TODO
	}
	
	/**
	 * Turn ranger on or off
	 * @param status
	 */
	public void switchRanger(boolean status) {
		//TODO
	}
	
	/**
	 * Perform ranger item raffle
	 */
	public void doRaffle() {
		//TODO
	}
}
