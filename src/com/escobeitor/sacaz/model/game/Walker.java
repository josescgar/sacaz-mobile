package com.escobeitor.sacaz.model.game;

import org.andengine.entity.modifier.FadeOutModifier;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.text.Text;
import org.andengine.extension.tmx.TMXLayer;
import org.andengine.extension.tmx.TMXTile;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.adt.color.Color;
import org.andengine.util.algorithm.path.ICostFunction;
import org.andengine.util.algorithm.path.IPathFinderMap;
import org.andengine.util.algorithm.path.Path;
import org.andengine.util.algorithm.path.astar.AStarPathFinder;
import org.andengine.util.algorithm.path.astar.EuclideanHeuristic;

import com.escobeitor.sacaz.enums.EElement;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.manager.ResourceManager;
import com.escobeitor.sacaz.model.interfaces.IBuildable;
import com.escobeitor.sacaz.model.interfaces.ITargetable;
import com.google.gson.annotations.Expose;

/**
 * Class representing an entity being able to move. Only common attributes.
 * @author Escobeitor
 *
 */
public abstract class Walker extends AbstractTarget implements IBuildable {
	
	/**
	 * Defense points
	 */
	public int defense;
	
	/**
	 * Melee points
	 */
	public int melee;
	
	/**
	 * Movement points
	 */
	public int movement;
	
	/**
	 * Walker type
	 */
	@Expose
	public EElement type;
	
	/**
	 * Create a new animated sprite representing the walker
	 * @param x
	 * @param y
	 * @param texture
	 */
	public Walker(float x, float y, TiledTextureRegion texture) {
		super(x, y, texture);
	}
	
	/**
	 * Finds a new path for the given initial and end tiles
	 * @param layer
	 * @param from
	 * @param to
	 * @return
	 */
	protected Path getPath(TMXLayer layer, TMXTile from, TMXTile to) {
		final IPathFinderMap<TMXLayer> pathFinder = new IPathFinderMap<TMXLayer>() {

			@Override
			public boolean isBlocked(int pX, int pY, TMXLayer pEntity) {
				return false;
			}
			
		};
		
		final AStarPathFinder<TMXLayer> finder = new AStarPathFinder<TMXLayer>();
		final org.andengine.util.algorithm.path.Path aStarPath = finder.findPath(pathFinder, 
				0, 
				0, 
				24, 
				14, 
				layer, 
				from.getTileColumn(), 
				from.getTileRow(),
				to.getTileColumn(),
				to.getTileRow(), 
				false,
				new EuclideanHeuristic<TMXLayer>(),
				new ICostFunction<TMXLayer>() {

					@Override
					public float getCost(IPathFinderMap<TMXLayer> pPathFinderMap, int pFromX, int pFromY, int pToX, int pToY, TMXLayer pEntity) {
						return 1f;
					}
				});
		
		return aStarPath;
	}
}
