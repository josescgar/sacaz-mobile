package com.escobeitor.sacaz.model.game;

import org.andengine.input.touch.TouchEvent;
import org.json.JSONArray;

import android.util.Log;

import com.escobeitor.sacaz.enums.EGameResult;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.manager.ResourceManager;

/**
 * Player generator
 * @author Escobeitor
 *
 */
public class Generator extends AbstractTarget {

	/**
	 * Who is the owner of the generator
	 */
	public Player player;
	
	/**
	 * Create a new generator
	 * @param player
	 * @param id
	 * @param position
	 */
	public Generator(Player player, String id) {
		super(0, 0, ResourceManager.generatorTexture);
		this.setCurrentTileIndex(0);
		this.id = id;
		this.player = player;
	}
	
	/**
	 * Generator deactivate game dialog
	 * @param character
	 */
	public void deactivate(Character character) {
		//TODO
	}
	
	/**
	 * Check if the selected character is in range with the generator
	 * @param character
	 * @return
	 */
	public boolean isCharacterInRange(Character character) {
		//TODO
		return true;
	}

	/**
	 * @see com.escobeitor.sacaz.model.interfaces.ITargetable#removeFromGame()
	 */
	@Override
	public void removeFromGame() {
		
		try {
			
			//Last player generator. Game lost.
			if(player.generators.size() == 1) {
				final EGameResult result = GameContextHolder.getInstance().game.getLocalPlayer().userId.equals(player.userId) ? EGameResult.LOST : EGameResult.WON;
				GameContextHolder.getInstance().game.finishGame(result);
				return;
			}
			
			player.generators.remove(this);
			this.setCurrentTileIndex(1);
			GameContextHolder.getInstance().gameLevelScene.unregisterTouchArea(this);
			
			final String playerKey = player.userId.equals(GameContextHolder.getInstance().gameJSON.getJSONObject("player1").get("userId")) ? "player1" : "player2";
			GameContextHolder.getInstance().gameJSON.getJSONObject(playerKey).put("generators", new JSONArray(player.generators));
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error removing generator from game: " + e.getMessage());
		}
		
	}
	
	/**
	 * @see org.andengine.entity.Entity#onAreaTouched(org.andengine.input.touch.TouchEvent, float, float)
	 */
	@Override
    public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
		if(!GameContextHolder.getInstance().game.getLocalPlayer().isAttacking || 
				GameContextHolder.getInstance().game.getLocalPlayer().userId.equals(player.userId)) {
			return false;
		}
		
		GameContextHolder.getInstance().game.selectedCharacter.strike(this);
		return true;
	}
}
