package com.escobeitor.sacaz.model.game;

import com.google.gson.annotations.Expose;

/**
 * Class representing an entitie's position
 * @author Escobeitor
 *
 */
public class Position {
	
	/**
	 * Horizontal coordinate
	 */
	@Expose
	public float x;
	
	/**
	 * Vertical coordinate
	 */
	@Expose
	public float y;
	
	/**
	 * Create an initialize a position
	 * @param x
	 * @param y
	 */
	public Position(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Calculate the distance between two given positions
	 * @param pos
	 * @return distance between the current position and the parameter
	 */
	public float getDistance(Position pos) {
		return (float) Math.sqrt((Math.pow(this.x - pos.x, 2) + Math.pow(this.y - pos.y, 2)));
	}
}
