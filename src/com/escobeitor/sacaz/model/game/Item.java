package com.escobeitor.sacaz.model.game;

import java.lang.reflect.Constructor;
import java.util.UUID;

import android.util.Log;

import com.escobeitor.sacaz.abilities.Ability;
import com.escobeitor.sacaz.abilities.interfaces.IAbility;
import com.escobeitor.sacaz.enums.EElement;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.model.interfaces.IBuildable;

/**
 * Game item (supplies, etc.)
 * @author Escobeitor
 *
 */
public class Item implements IBuildable {
	
	/**
	 * Item id
	 */
	public String id;
	
	/**
	 * Descriptive name
	 */
	public String name;
	
	/**
	 * Description
	 */
	public String description;
	
	/**
	 * Number of uses left
	 */
	public int numUses;
	
	/**
	 * Item type
	 */
	public EElement type;
	
	/**
	 * Item fired ability
	 */
	public Ability ability;
	
	/**
	 * Item position within the map
	 */
	public Position position;
	
	/**
	 * Create new item
	 * @param element
	 */
	public Item(EElement element) {
		resetElement(element);
	}

	/**
	 * @see com.escobeitor.sacaz.model.interfaces.IBuildable#resetElement(com.escobeitor.sacaz.enums.EElement)
	 */
	@Override
	public void resetElement(EElement element) {
		this.id = UUID.randomUUID().toString();
		this.name = element.properties.get("name");
		this.numUses = Integer.parseInt(element.properties.get("maxuses"));
		this.type = element;
		
		if(element.properties.containsKey("ability")) {
			try {
				Constructor<?> constructor = Class.forName(element.properties.get("ability")).getConstructor();
				this.ability = (Ability) constructor.newInstance();
			} catch (Exception e) {
				Log.e(GameConfiguration.TAG_ERROR, "Error creating item ability: " + e.getMessage());
				this.ability = null;
			}
		}
	}
}
