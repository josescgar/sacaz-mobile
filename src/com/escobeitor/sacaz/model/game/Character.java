package com.escobeitor.sacaz.model.game;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.UUID;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.FadeOutModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.PathModifier;
import org.andengine.entity.modifier.PathModifier.IPathModifierListener;
import org.andengine.entity.modifier.PathModifier.Path;
import org.andengine.entity.primitive.Mesh;
import org.andengine.entity.text.Text;
import org.andengine.extension.tmx.TMXLayer;
import org.andengine.extension.tmx.TMXTile;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.adt.color.Color;
import org.andengine.util.modifier.IModifier;
import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;
import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.abilities.Ability;
import com.escobeitor.sacaz.enums.EElement;
import com.escobeitor.sacaz.exceptions.NotEnoughActionPointsException;
import com.escobeitor.sacaz.exceptions.NotEnoughAmmoException;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.global.GeometricalUtils;
import com.escobeitor.sacaz.manager.ResourceManager;
import com.escobeitor.sacaz.model.factory.WalkerFactory;
import com.google.gson.annotations.Expose;

/**
 * User controlled character entity class
 * @author Escobeitor
 *
 */
public class Character extends Walker {
	
	/**
	 * Name of the character
	 */
	@Expose
	public String name;
	
	/**
	 * Description of the character
	 */
	public String description;
	
	/**
	 * Firearm skill points
	 */
	public int firearm;
	
	/**
	 * Character's ability
	 */
	public Ability ability;
	
	/**
	 * Owner of the character
	 */
	public Player player;
	
	/**
	 * Player's equipped weapons
	 */
	@Expose
	public List<Weapon> weapons;
	
	/**
	 * Player's action grid figure
	 */
	public Mesh grid = null;
	
	/**
	 * Reference to self
	 */
	private Character self;
	
	/**
	 * Create new character
	 * @param element
	 */
	public Character(EElement element) {
		
		super(0, 0, (TiledTextureRegion) ResourceManager.getTextureForElement(element.name()));
		this.setCurrentTileIndex(1);
		this.self = this;
		
		resetElement(element);
	}

	/**
	 * @see com.escobeitor.sacaz.model.interfaces.IBuildable#resetElement(com.escobeitor.sacaz.enums.EElement)
	 */
	public void resetElement(EElement element) {
		this.defense = Integer.parseInt(element.properties.get("defense"));
		this.firearm = Integer.parseInt(element.properties.get("firearm"));
		this.health = Integer.parseInt(element.properties.get("health"));
		this.id = UUID.randomUUID().toString();
		this.melee = Integer.parseInt(element.properties.get("melee"));
		this.movement = Integer.parseInt(element.properties.get("movement"));
		this.name = element.properties.get("name");
		this.type = element;
		this.position = new Position(0, 0);
		this.grid = null;
		this.weapons = null;
		this.player = null;
		
		try {
			Constructor<?> constructor = Class.forName(element.properties.get("ability")).getConstructor();
			this.ability = (Ability) constructor.newInstance();
		} catch (Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error creating character's ability: " + e.getMessage());
			this.ability = null;
		}
	}
	
	/**
	 * @see com.escobeitor.sacaz.model.interfaces.ITargetable#removeFromGame()
	 */
	@Override
	public void removeFromGame() {
		
		try {
			
			player.characters.remove(this);
			final String playerKey = player.userId.equals(GameContextHolder.getInstance().gameJSON.getJSONObject("player1").get("userId")) ? "player1" : "player2";
			GameContextHolder.getInstance().gameJSON.getJSONObject(playerKey).put("characters", new JSONArray(player.characters));
			ResourceManager.gameMap.detachChild(this);
			GameContextHolder.getInstance().gameLevelScene.unregisterTouchArea(this);
			WalkerFactory.getInstance().recycleElement(this);
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error removing character: " + e.getMessage());
		}
		
	}
	
	/**
	 * Move the character to a given position
	 * @param pos
	 */
	public void moveToPosition(Position pos) {
		moveToPosition(pos, true);
	}
	
	/**
	 * Move the character to a given position
	 * @param pos
	 */
	public void moveToPosition(Position pos, final boolean byLocalPlayer) {
		TMXLayer groundLayer = ResourceManager.gameMap.getTMXLayers().get(0);
		TMXTile tileTo = groundLayer.getTMXTileAt(pos.x, pos.y);
		TMXTile tileFrom = groundLayer.getTMXTileAt(position.x, position.y);
		
		final org.andengine.util.algorithm.path.Path aStarPath = getPath(groundLayer, tileFrom, tileTo);
		
		if(aStarPath == null) {
			return;
		}
		
		final int totalCost = aStarPath.getLength() * movement;
		final int steps = (int) ((totalCost > player.actionPoints) && byLocalPlayer ? Math.floor(player.actionPoints / movement) : aStarPath.getLength());
		if(steps == 0) {
			return;
		}
		
		final int pathLength = steps == 1 ? 2 : steps;
		
		final Path path = new Path(pathLength);
		for(int i = 0; i < pathLength; i++) {
			path.to(groundLayer.getTileX(aStarPath.getX(i)), groundLayer.getTileY(aStarPath.getY(i)));
		}
		
		detachGrid();
		
		position.x = path.getCoordinatesX()[path.getSize() - 1];
		position.y = path.getCoordinatesY()[path.getSize() - 1];
		
		if(byLocalPlayer) {
			try {
				
				player.substractActionPoints(steps * movement);
				GameContextHolder.getInstance().game.gameLog.addMoveCharacter(self, steps * movement, player.characters.indexOf(self));
				
			} catch (Exception e) {
				Log.d(GameConfiguration.TAG_ERROR, "Error moving character: " + e.getMessage());
			}
		}
		
		//Create entity modifier
		final PathModifier modifier = new PathModifier((float) steps, path, 
			new IEntityModifierListener() {
			
					@Override
					public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}
					
					@Override
					public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
						if(byLocalPlayer) {
							attachGrid();
						}
					}
			},
				
			new IPathModifierListener() {
			
			@Override
			public void onPathWaypointStarted(PathModifier pPathModifier, IEntity pEntity, int pWaypointIndex) {
				switch(aStarPath.getDirectionToNextStep(pWaypointIndex)) {
                case UP:                                                                       
                		self.animate(new long[]{200, 200, 200}, 0, 2, 2);
                        break;

                case DOWN:
                		self.animate(new long[]{200, 200, 200}, 9, 11, 2);
                        break;

                case LEFT:                                                             
                		self.animate(new long[]{200, 200, 200}, 3, 5, 2);
                        break;

                case RIGHT:
                		self.animate(new long[]{200, 200, 200}, 6, 8, 2);

                        break;
                default:
                        break;
                }   
			}
			
			@Override
			public void onPathStarted(PathModifier pPathModifier, IEntity pEntity) {}
			
			@Override
			public void onPathWaypointFinished(PathModifier pPathModifier, IEntity pEntity, int pWaypointIndex) {}

			@Override
			public void onPathFinished(PathModifier pPathModifier, IEntity pEntity) {}
			
		});
		
		this.registerEntityModifier(modifier);
	}
	
	/**
	 * Picks up an item
	 * @param item
	 */
	public void pickUpObject(Item item) {
		//TODO
	}
	
	/**
	 * Strikes a target
	 * @param target
	 */
	public void strike(AbstractTarget target) {
		strike(target, getWeapon(player.isAttackingWithFirearm), true, -1);
	}
	
	/**
	 * Strikes a target
	 * @param target
	 */
	public void strike(AbstractTarget target, Weapon weapon, boolean byLocalUser, int totalDamage) {
		
		if(weapon == null) {
			AndroidApplicationContextHolder.toastOnUiThread(AndroidApplicationContextHolder.getInstance().getContext().getString(R.string.error_no_selected_weapon), Toast.LENGTH_LONG);
			return;
		}
		
		//Check if the target is in range
		if(!weapon.isTargetInRange(this, target)) {
			AndroidApplicationContextHolder.toastOnUiThread(AndroidApplicationContextHolder.getInstance().getContext().getString(R.string.error_not_in_range), Toast.LENGTH_LONG);
			return;
		}
		
		try {
			
			//Check if there is enough ammo
			if(weapon.firearm && weapon.ammo < 1) {
				throw new NotEnoughAmmoException();
			}
			
			//Substract action points
			if(byLocalUser) {
				player.substractActionPoints(Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.COST_AP_STRIKE_KEY)));
			}
			
			//Attack animation
			final Text characterAttackTarget = new Text(0, 0, 
					ResourceManager.getInstance().defaultFont28White, 
					AndroidApplicationContextHolder.getInstance().getContext().getString(R.string.attack_animation_text),
					ResourceManager.getInstance().engine.getVertexBufferObjectManager());

			characterAttackTarget.setColor(Color.GREEN);
			characterAttackTarget.setAnchorCenter(0, 0);
			characterAttackTarget.setPosition(this.position.x, this.position.y + GameConfiguration.TILE_DIMENSION);		
			ResourceManager.gameMap.attachChild(characterAttackTarget);
			characterAttackTarget.registerEntityModifier(new FadeOutModifier(2f));
			
			//Perform the attack
			final int damage = byLocalUser ? weapon.hitTarget(this, target) : totalDamage;
			
			if(byLocalUser) {
				GameContextHolder.getInstance().game.gameLog.addStrike(this, target, weapon, damage);
			}
			
			target.decreaseHealth(damage);

		} catch(NotEnoughActionPointsException e) {
			AndroidApplicationContextHolder.toastOnUiThread(AndroidApplicationContextHolder.getInstance().getContext().getString(R.string.error_not_enough_ap), Toast.LENGTH_LONG);
			return;
		} catch (NotEnoughAmmoException e) {
			AndroidApplicationContextHolder.toastOnUiThread(AndroidApplicationContextHolder.getInstance().getContext().getString(R.string.error_no_ammo), Toast.LENGTH_LONG);
			return;
		} catch (JSONException e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error registering character attack in log: " + e.getMessage());
		} finally {
			player.isAttacking = false;
			detachGrid();
		}
	}
	
	/**
	 * @see org.andengine.entity.Entity#onAreaTouched(org.andengine.input.touch.TouchEvent, float, float)
	 */
	@Override
    public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
		
		if(!player.userId.equals(GameContextHolder.getInstance().user.id)) {
			if(GameContextHolder.getInstance().game.getLocalPlayer().isAttacking) {
				GameContextHolder.getInstance().game.selectedCharacter.strike(this);
				return true;
			}
			
			return false;
		}
		
		if(!GameContextHolder.getInstance().game.isLocalPlayerTurn()) {
			return false;
		}
		
		if(pSceneTouchEvent.isActionDown()) {
			if(grid == null) {
				attachGrid();
			} else {
				detachGrid();
			}
			
			return true;
		}
		
		return false;
	}

	/**
	 * Attach a new grid
	 */
	public void attachGrid() {
		GameContextHolder.getInstance().game.selectedCharacter = this;
		grid = GeometricalUtils.createMovementGrid(position, (int) Math.floor(player.actionPoints / movement));
		grid.setColor(Color.RED);
		ResourceManager.gameMap.attachChild(grid);
	}

	/**
	 * Detach current grid
	 */
	public void detachGrid() {
		GameContextHolder.getInstance().game.selectedCharacter = null;
		ResourceManager.gameMap.detachChild(grid);
		grid = null;
	}
	
	/**
	 * Get the character's weapon
	 * @param firearm
	 * @return
	 */
	private Weapon getWeapon(final boolean isFirearm) {
		for(int i = 0; i < weapons.size(); i++) {
			if(weapons.get(i).firearm == isFirearm) {
				return weapons.get(i);
			}
		}
		
		return null;
	}
	
	/**
	 * Find a weapon by its id
	 * @param weaponId
	 * @return
	 */
	public Weapon getWeapon(String weaponId) {
		for(int i = 0; i < weapons.size(); i++) {
			if(weapons.get(i).id.equals(weaponId)) {
				return weapons.get(i);
			}
		}
		
		return null;
	}
}
