package com.escobeitor.sacaz.model.game;

import java.util.Random;
import java.util.UUID;

import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.enums.EElement;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.model.interfaces.IBuildable;
import com.google.gson.annotations.Expose;

/**
 * Character weapon
 * @author Escobeitor
 *
 */
public class Weapon implements IBuildable {

	/**
	 * Weapon id
	 */
	public String id;
	
	/**
	 * Accuracy points
	 */
	public int accuracy;
	
	/**
	 * Ammo left
	 */
	@Expose
	public int ammo;
	
	/**
	 * Damage points
	 */
	public int damage;
	
	/**
	 * The weapon is a firearm
	 */
	public boolean firearm;
	
	/**
	 * Descriptive name
	 */
	@Expose
	public String name;
	
	/**
	 * Weapon type
	 */
	@Expose
	public EElement type;
	
	/**
	 * Create new weapon
	 * @param element
	 */
	public Weapon(EElement element) {
		resetElement(element);
	}

	/**
	 * @see com.escobeitor.sacaz.model.interfaces.IBuildable#resetElement(com.escobeitor.sacaz.enums.EElement)
	 */
	@Override
	public void resetElement(EElement element) {
		this.id = UUID.randomUUID().toString();
		this.accuracy = Integer.parseInt(element.properties.get("accuracy"));
		this.ammo = Integer.parseInt(element.properties.get("ammo"));
		this.damage = Integer.parseInt(element.properties.get("damage"));
		this.firearm = Boolean.parseBoolean(element.properties.get("firearm"));
		this.name = element.properties.get("name");
		this.type = element;
	}
	
	/**
	 * Checks if the target is in range of the weapon or not
	 * @param character
	 * @param target
	 * @return is target in range
	 */
	public boolean isTargetInRange(Character character, AbstractTarget target) {
		if(firearm) {
			return true;
		}
		
		return character.position.getDistance(target.position) <= GameConfiguration.TILE_DIMENSION;
	}
	
	/**
	 * Hits the target with the weapon
	 * @param character
	 * @param target
	 * @return damage caused 
	 */
	public int hitTarget(Character character, AbstractTarget target) {

		//Calculate probability of hitting the target
		float chanceOfHitting = 1.0f;
		if(firearm) {
			final int distanceInTiles = (int) Math.ceil(character.position.getDistance(target.position) / GameConfiguration.TILE_DIMENSION);
			chanceOfHitting = ((character.firearm + accuracy) - distanceInTiles) / 10f;
		}

		final boolean hit = new Random().nextFloat() <= chanceOfHitting;
		
		//Hit the target
		int damage = 0;
		if(hit) {
			damage = firearm ? this.damage : this.damage + character.melee;
			if(target instanceof Character) {
				damage -= ((Character) target).defense;
			}
		}
		
		//Substract ammo
		if(firearm) {
			ammo--;
			AndroidApplicationContextHolder.toastOnUiThread(AndroidApplicationContextHolder.getInstance().getContext().getString(R.string.attack_ammo_left, ammo), Toast.LENGTH_LONG);
		}
		
		return damage;
	}
}
