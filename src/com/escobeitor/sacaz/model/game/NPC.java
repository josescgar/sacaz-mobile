package com.escobeitor.sacaz.model.game;

import java.util.UUID;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.PathModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.PathModifier.IPathModifierListener;
import org.andengine.entity.modifier.PathModifier.Path;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.tmx.TMXLayer;
import org.andengine.extension.tmx.TMXTile;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.modifier.IModifier;
import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.escobeitor.sacaz.abilities.CharismaticBomb;
import com.escobeitor.sacaz.enums.EElement;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.manager.ResourceManager;
import com.escobeitor.sacaz.model.factory.WalkerFactory;
import com.google.gson.annotations.Expose;

public class NPC extends Walker {
	
	/**
	 * The NPC is booby trapped
	 */
	@Expose
	public boolean boobyTrapped;
	
	/**
	 * Acquired target
	 */
	public Character target;
	
	/**
	 * Create a new NPC
	 */
	public NPC(EElement element) {
		super(0, 0, (TiledTextureRegion) ResourceManager.getTextureForElement(element.name()));
		resetElement(element);
	}

	/**
	 * @see com.escobeitor.sacaz.model.interfaces.IBuildable#resetElement(com.escobeitor.sacaz.enums.EElement)
	 */
	@Override
	public void resetElement(EElement element) {
		this.boobyTrapped = false;
		this.target = null;
		this.defense = Integer.parseInt(element.properties.get("defense"));
		this.health = Integer.parseInt(element.properties.get("health"));
		this.melee = Integer.parseInt(element.properties.get("melee"));
		this.movement = Integer.parseInt(element.properties.get("movement"));
		this.id = UUID.randomUUID().toString();
		this.type = element;
		this.position = new Position(0, 0);
	}
	
	/**
	 * Checks if it is under the effects of {@link CharismaticBomb}
	 * @return
	 */
	public Character isUnderEffectOfCharismaticBomb() {
		//TODO
		return null;
	}
	
	/**
	 * Attack the current target
	 * @return
	 */
	public void attackTarget(final boolean byLocalPlayer) {
		if(target == null) {
			return;
		}
		
		if(position.getDistance(target.position) > GameConfiguration.TILE_DIMENSION) {
			return;
		}
		
		int damage = melee;
		if(boobyTrapped) {
			damage += Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.BOOBY_TRAP_BASE_POWER_KEY));
		}
		
		
			try {
				if(byLocalPlayer) {
					GameContextHolder.getInstance().game.gameLog.addNPCStrike(this, target, damage);
				}
				
				final int characterIndex = ((Character) target).player.characters.indexOf(target);
				final JSONObject gameJSON = GameContextHolder.getInstance().gameJSON;
				final String playerKey = ((Character) target).player.userId.equals(gameJSON.getJSONObject("player1").get("userId")) ? "player1" : "player2";
				final int finalHealth = gameJSON.getJSONObject(playerKey).getJSONArray("characters").getJSONObject(characterIndex).getInt("health") - damage;
				gameJSON.getJSONObject(playerKey).getJSONArray("characters").getJSONObject(characterIndex).put("health", finalHealth);
				
			} catch(Exception e) {
				Log.e(GameConfiguration.TAG_ERROR, "Error recording NPC strike: " + e.getMessage());
			}
		
		
		target.decreaseHealth(damage);
	}

	/**
	 * @see com.escobeitor.sacaz.model.interfaces.ITargetable#removeFromGame()
	 */
	@Override
	public void removeFromGame() {
		ResourceManager.gameMap.detachChild(this);
		GameContextHolder.getInstance().gameLevelScene.unregisterTouchArea(this);
		final int npcIndex = GameContextHolder.getInstance().game.npcs.indexOf(this);
		try {
			GameContextHolder.getInstance().game.npcs.remove(npcIndex);
			GameContextHolder.getInstance().game.numNPC--;
			GameContextHolder.getInstance().gameJSON.put("npcs", new JSONArray(GameContextHolder.getInstance().game.npcs));
			WalkerFactory.getInstance().recycleElement(this);
		} catch(Exception e) {
			Log.d(GameConfiguration.TAG_DEBUG, "Error deleting NPC from game: " + e.getMessage());
		}
	}
	
	/**
	 * Move the zombie towards the given position
	 * @param npcListIndex
	 */
	public void moveTowardsTarget(int npcListIndex) {
		moveTowardsTarget(npcListIndex, true);
	}
	
	/**
	 * Move the zombie towards the given position
	 * @param pos
	 */
	public void moveTowardsTarget(final int npcListIndex, final boolean anotate) {
		Character charismaticTarget = isUnderEffectOfCharismaticBomb();
		
		target = charismaticTarget == null ? GameContextHolder.getInstance().game.getNearestCharacter(this) : charismaticTarget;
		
		if(target == null) {
			return;
		}
		
		TMXLayer layer = ResourceManager.gameMap.getTMXLayers().get(0);
		TMXTile from = layer.getTMXTileAt(position.x, position.y);
		TMXTile to = layer.getTMXTileAt(target.position.x, target.position.y);
				
		final org.andengine.util.algorithm.path.Path aStarPath = getPath(layer, from, to);
		
		if(aStarPath == null) {
			attackTarget(anotate);
			checkGenerateLogAction(npcListIndex, anotate);
			return;
		}
		
		final int totalMovement = (int) Math.floor(Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.NPC_MOVEMENT_AP_KEY)) / movement);
		final int steps = totalMovement < aStarPath.getLength() ? totalMovement : aStarPath.getLength() - 1;
		
		if(steps == 1) {
			attackTarget(anotate);
			checkGenerateLogAction(npcListIndex, anotate);
			return;
		}
		
		final Path path = new Path(steps);
		for(int i = 0; i < steps; i++) {
			path.to(layer.getTileX(aStarPath.getX(i)), layer.getTileY(aStarPath.getY(i)));
		}
		
		position.x = path.getCoordinatesX()[path.getSize() - 1];
		position.y = path.getCoordinatesY()[path.getSize() - 1];

		final PathModifier modifier = new PathModifier((float) steps, path, 
				new IEntityModifierListener() {
			
						@Override
						public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}
						
						@Override
						public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
							attackTarget(anotate);
						}
				},
				
				new IPathModifierListener() {
				
				@Override
				public void onPathWaypointStarted(PathModifier pPathModifier, IEntity pEntity, int pWaypointIndex) {
					switch(aStarPath.getDirectionToNextStep(pWaypointIndex)) {
	                case UP:                                                                       
	                		((AnimatedSprite) pEntity).animate(new long[]{200, 200, 200}, 0, 2, 2);
	                        break;

	                case DOWN:
	                	((AnimatedSprite) pEntity).animate(new long[]{200, 200, 200}, 9, 11, 2);
	                        break;

	                case LEFT:                                                             
	                		((AnimatedSprite) pEntity).animate(new long[]{200, 200, 200}, 3, 5, 2);
	                        break;

	                case RIGHT:
	                		((AnimatedSprite) pEntity).animate(new long[]{200, 200, 200}, 6, 8, 2);
	                        break;
	                default:
	                        break;
	                }   
				}
				
				@Override
				public void onPathStarted(PathModifier pPathModifier, IEntity pEntity) {}
				
				@Override
				public void onPathWaypointFinished(PathModifier pPathModifier, IEntity pEntity, int pWaypointIndex) {}

				@Override
				public void onPathFinished(PathModifier pPathModifier, IEntity pEntity) {}
				
		});
		
		checkGenerateLogAction(npcListIndex, anotate);
			
		this.registerEntityModifier(modifier);
	}

	/**
	 * Check if the movement must be registered in the log action
	 * @param npcListIndex
	 * @param anotate
	 */
	private void checkGenerateLogAction(final int npcListIndex, final boolean anotate) {
		if(anotate) {
			try {
				GameContextHolder.getInstance().game.gameLog.addMoveNPC(this, npcListIndex);
			} catch (Exception e) {
				Log.d(GameConfiguration.TAG_ERROR, "Error moving NPC: " + e.getMessage());
			}
		}
	}
	
	/**
	 * @see org.andengine.entity.Entity#onAreaTouched(org.andengine.input.touch.TouchEvent, float, float)
	 */
	@Override
    public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
		if(!GameContextHolder.getInstance().game.getLocalPlayer().isAttacking) {
			return false;
		}
		
		GameContextHolder.getInstance().game.selectedCharacter.strike(this);
		return true;
	}
}
