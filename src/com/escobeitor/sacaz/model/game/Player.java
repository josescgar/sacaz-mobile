package com.escobeitor.sacaz.model.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.abilities.Surgery;
import com.escobeitor.sacaz.activity.GameActivity;
import com.escobeitor.sacaz.enums.EElement;
import com.escobeitor.sacaz.enums.EElementType;
import com.escobeitor.sacaz.enums.EFaction;
import com.escobeitor.sacaz.exceptions.NotEnoughActionPointsException;
import com.escobeitor.sacaz.exceptions.NotEnoughResourcesException;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.manager.ResourceManager;
import com.escobeitor.sacaz.model.factory.WalkerFactory;
import com.escobeitor.sacaz.model.factory.WeaponFactory;

/**
 * A Player within a game. 
 * Game accessed via {@link GameContextHolder}
 * @author Escobeitor
 *
 */
public class Player {
	
	/**
	 * User unique identification number. 
	 * Identifies the Player within a game.
	 */
	public String userId;
	
	/**
	 * Player's username
	 */
	public String username;
	
	/**
	 * Remaining action points
	 */
	public int actionPoints;
	
	/**
	 * Remaining electricity
	 */
	public int electricity;
	
	/**
	 * Remaining food
	 */
	public int food;
	
	/**
	 * Remaining materials
	 */
	public int materials;
	
	/**
	 * Remaining water
	 */
	public int water;
	
	/**
	 * Player's {@link Surgery} global ability
	 */
	public Surgery surgery;
	
	/**
	 * Player's faction
	 */
	public EFaction faction;
	
	/**
	 * Player's warehouse
	 */
	public Warehouse warehouse;
	
	/**
	 * Player's generators
	 */
	public List<Generator> generators;
	
	/**
	 * Player's ranger
	 */
	public Ranger ranger;
	
	/**
	 * Player's characters
	 */
	public List<Character> characters;
	
	/**
	 * Flag to indicate if the player is 
	 * Performing an attack operation
	 */
	public boolean isAttacking = false;
	
	/**
	 * Flag to indicate which weapon the player is using 
	 * in case of performing an attack
	 */
	public boolean isAttackingWithFirearm = false;
	
	/**
	 * App context reference
	 */
	final private Context context = AndroidApplicationContextHolder.getInstance().getContext();
	
	/*
	 * DIALOG ATTRIBUTES
	 */
	private Spinner charactersList;
	private Spinner firearmsList;
	private Spinner meleesList;
	
	/**
	 * Substract the resources from the player. If there is not enough of any of them
	 * the method throws an exception.
	 * @param electricity
	 * @param food
	 * @param materials
	 * @param water
	 * @throws NotEnoughResourcesException 
	 */
	public void substractResources(int electricity, int food, int materials, int water) throws NotEnoughResourcesException {
		if(this.electricity < electricity || this.food < food || this.materials < materials || this.water < water) {
			throw new NotEnoughResourcesException();
		}
		
		this.electricity -= electricity;
		this.food -= food;
		this.materials -= materials;
		this.water -= water;
		
		updateHUDResources();
	}

	/**
	 * Updates the HUD display with the current amount of resources
	 */
	private void updateHUDResources() {
		ResourceManager.hudElectricity.setText(Integer.toString(this.electricity));
		ResourceManager.hudFood.setText(Integer.toString(this.food));
		ResourceManager.hudMaterials.setText(Integer.toString(this.materials));
		ResourceManager.hudWater.setText(Integer.toString(this.water));
	}
	
	/**
	 * Substract the resources from the player. If there is not enough of any of them
	 * the method throws an exception.
	 * @param electricity
	 * @param food
	 * @param materials
	 * @param water
	 * @throws NotEnoughResourcesException 
	 */
	public void increaseResources(int actionPoints, int electricity, int food, int materials, int water) throws NotEnoughResourcesException {
		
		this.actionPoints += actionPoints;
		this.electricity += electricity;
		this.food += food;
		this.materials += materials;
		this.water += water;
		
		updateHUDResources();
		updateHUDAP();
	}
	
	/**
	 * Substract the user resources for the given element
	 * @param element
	 * @throws NotEnoughResourcesException 
	 */
	public void substractResources(EElement... elements) throws NotEnoughResourcesException {
		int totalFood = 0;
		int totalElectricity = 0;
		int totalWater = 0;
		int totalMaterials = 0;
		
		for(int i = 0; i < elements.length; i++) {
			EElement element = elements[i];
			totalFood += element.foodCost;
			totalElectricity += element.electricityCost;
			totalWater += element.waterCost;
			totalMaterials += element.materialsCost;
		}
		
		substractResources(totalElectricity, totalFood, totalMaterials, totalWater);
	}
	
	/**
	 * Creates a new character
	 * @param firearm
	 * @param melee
	 * @param character
	 */
	public void generateCharacter(EElement firearm, EElement melee, EElement character) {
		if(EElement.NONE.equals(firearm) && EElement.NONE.equals(melee)) {
			AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_no_weapons_selected), Toast.LENGTH_LONG);
			return;
		}
		
		if(this.actionPoints < Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.COST_AP_CREATE_CHARACTER_KEY))) {
			AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_not_enough_ap), Toast.LENGTH_LONG);
			return;
		}
		
		try {
			substractResources(firearm, melee, character);
			substractActionPoints(Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.COST_AP_CREATE_CHARACTER_KEY)));
		
			Character c = (Character) WalkerFactory.getInstance().createElement(character);
			c.weapons = new ArrayList<Weapon>();
			if(!EElement.NONE.equals(firearm)) {
				c.weapons.add((Weapon) WeaponFactory.getInstance().createElement(firearm));
			}
			
			if(!EElement.NONE.equals(melee)) {
				c.weapons.add((Weapon) WeaponFactory.getInstance().createElement(melee));
			}

			c.player = this;
			c.position = GameContextHolder.getInstance().game.getInitialCharacterPosition(this);
			c.setAnchorCenter(0, 0);
			c.setPosition(c.position.x, c.position.y);
			
			GameContextHolder.getInstance().game.gameLog.addNewCharacter(c);
			characters.add(c);
			GameContextHolder.getInstance().gameLevelScene.registerTouchArea(c);
			ResourceManager.gameMap.attachChild(c);
		
		} catch (NotEnoughResourcesException e) {
			AndroidApplicationContextHolder.toastOnUiThread(context.getText(R.string.error_not_enough_resources), Toast.LENGTH_LONG);
			return;
		} catch (Exception e) {
			return;
		}
	}
	
	/**
	 * Substacts the given amount to the player's action points
	 * @param points
	 * @throws NotEnoughActionPointsException 
	 */
	public void substractActionPoints(int points) throws NotEnoughActionPointsException {
		if(this.actionPoints < points) {
			throw new NotEnoughActionPointsException();
		}
		
		this.actionPoints -= points;
		
		updateHUDAP();
	}

	/**
	 * Updates the HUD display with the current amount of action points
	 */
	private void updateHUDAP() {
		ResourceManager.hudAP.setText(Integer.toString(actionPoints));
	}
	
	/**
	 * Checks if there are uses left for the surgery ability
	 * @return
	 */
	public boolean isSurgeryAvailable() {
		return this.surgery.numUses > 0;
	}
	
	/**
	 * Shows a dialog for character creation
	 */
	public void createCharacter() {

		((GameActivity) context).runOnUiThread(new Runnable() {

			@Override
			public void run() {
				
				final AlertDialog.Builder alert = new AlertDialog.Builder(context);
				final View createCharacterDialog = LayoutInflater.from(context).inflate(R.layout.create_character, null);
				charactersList = (Spinner) createCharacterDialog.findViewById(R.id.create_character_option_character);
				firearmsList = (Spinner) createCharacterDialog.findViewById(R.id.create_character_option_firearm);
				meleesList = (Spinner) createCharacterDialog.findViewById(R.id.create_character_option_melee);
				alert.setTitle(context.getString(R.string.create_character_title, GameConfiguration.properties.getProperty(GameConfiguration.COST_AP_CREATE_CHARACTER_KEY)));
				alert.setView(createCharacterDialog);
				alert.setNegativeButton(context.getString(R.string.button_ko), null);
				
				List<EElement> elements;
				SimpleAdapter adapter;
				List<Map<String, String>> data = new ArrayList<Map<String, String>>();
				
				//Populate characters
				elements = EElement.getCharactersByFaction(faction);
				for(int i = 0; i < elements.size(); i++) {
					Map<String, String> row = new HashMap<String, String>(2);
					row.put("name", elements.get(i).name());
					row.put("cost", elements.get(i).getCostString());
					data.add(row);
				}
				
				adapter = new SimpleAdapter(context, data, android.R.layout.simple_list_item_2, new String[]{"name","cost"}, new int[]{android.R.id.text1, android.R.id.text2});			
				charactersList.setAdapter(adapter);
				
				//Populate firearms
				data = new ArrayList<Map<String, String>>();
				elements = EElement.getElemetsForType(EElementType.WEAPON_FIREARM);
				for(int i = 0; i < elements.size(); i++) {
					Map<String, String> row = new HashMap<String, String>(2);
					row.put("name", elements.get(i).name());
					row.put("cost", elements.get(i).getCostString());
					data.add(row);
				}
				adapter = new SimpleAdapter(context, data, android.R.layout.simple_list_item_2, new String[]{"name","cost"}, new int[]{android.R.id.text1, android.R.id.text2});
				firearmsList.setAdapter(adapter);
				
				//Populate melee weapons
				data = new ArrayList<Map<String, String>>();
				elements = EElement.getElemetsForType(EElementType.WEAPON_MELEE);
				for(int i = 0; i < elements.size(); i++) {
					Map<String, String> row = new HashMap<String, String>(2);
					row.put("name", elements.get(i).name());
					row.put("cost", elements.get(i).getCostString());
					data.add(row);
				}
				
				adapter = new SimpleAdapter(context, data, android.R.layout.simple_list_item_2, new String[]{"name","cost"}, new int[]{android.R.id.text1, android.R.id.text2});
				meleesList.setAdapter(adapter);
				
				//Create the dialog
				alert.setPositiveButton(context.getString(R.string.creage_game_ok_button), new DialogInterface.OnClickListener() {
					
					@SuppressWarnings("unchecked")
					@Override
					public void onClick(DialogInterface dialog, int which) {
						EElement character = EElement.valueOf(((HashMap<String, String>) charactersList.getSelectedItem()).get("name"));
						EElement firearm = EElement.valueOf(((HashMap<String, String>) firearmsList.getSelectedItem()).get("name"));
						EElement melee = EElement.valueOf(((HashMap<String, String>) meleesList.getSelectedItem()).get("name"));
						generateCharacter(firearm,  melee, character);
					}
				});
				
				alert.create();
				alert.show();
			}
			
		});
	}
}
