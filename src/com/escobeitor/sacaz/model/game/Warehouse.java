package com.escobeitor.sacaz.model.game;

import java.util.List;

import com.escobeitor.sacaz.global.GameConfiguration;

/**
 * Player's warehouse
 * @author Escobeitor
 *
 */
public class Warehouse {
	
	/**
	 * Player energy distribution. The energy can be used for increasing the resistance
	 * to damge of the generators or to speed up the character creation process.
	 */
	public double[] energyDistribution;
	
	/**
	 * Player who owns the warehouse
	 */
	public Player player;
	
	/**
	 * Items contained in the warehouse
	 */
	public List<Item> items;
	
	/**
	 * @param player
	 */
	public Warehouse(Player player) {
		this.player = player;
	}
	
	/**
	 * Check warehouse game dialog
	 */
	public void checkWarehouse() {
		//TODO
	}
	
	/**
	 * Change energy distribution game dialog
	 */
	public void changeEnergyDistribution() {
		//TODO
	}
	
	/**
	 * Change the energy distribution
	 * @param generators
	 * @param characters
	 */
	public void updateEnergyDistribution(float generators, float characters) {
		//TODO
	}
}
