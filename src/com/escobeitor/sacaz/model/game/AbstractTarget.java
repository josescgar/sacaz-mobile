package com.escobeitor.sacaz.model.game;

import org.andengine.entity.modifier.FadeOutModifier;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.adt.color.Color;
import org.json.JSONObject;

import android.util.Log;

import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.manager.ResourceManager;
import com.escobeitor.sacaz.model.interfaces.ITargetable;
import com.google.gson.annotations.Expose;

/**
 * Implements common functionality to all 
 * targetable entities in the game
 * @author Escobeitor
 *
 */
public abstract class AbstractTarget extends AnimatedSprite implements ITargetable {
	
	/**
	 * Unique id
	 */
	@Expose
	public String id;
	
	/**
	 * Entity health
	 */
	@Expose
	public int health;
	
	/**
	 * Element's position in the map
	 */
	@Expose
	public Position position;
	
	/**
	 * Entity texture constructor
	 * @param x
	 * @param y
	 * @param texture
	 */
	public AbstractTarget(float x, float y, TiledTextureRegion texture) {
		super(x, y, texture, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
	}
	
	/**
	 * @see com.escobeitor.sacaz.model.interfaces.ITargetable#decreaseHealth(int)
	 */
	public void decreaseHealth(int amount) {
		final Text targetHitText = new Text(0, 0, 
				ResourceManager.getInstance().defaultFont28White, 
				"9999",
				ResourceManager.getInstance().engine.getVertexBufferObjectManager());
		
		targetHitText.setText(Integer.toString(amount));
		targetHitText.setColor(Color.RED);
		targetHitText.setAnchorCenter(0, 0);
		targetHitText.setPosition(this.position.x, this.position.y + GameConfiguration.TILE_DIMENSION);		
		ResourceManager.gameMap.attachChild(targetHitText);
		
		targetHitText.registerEntityModifier(new FadeOutModifier(2f));
		
		health -= amount;

		if(amount >= health) {
			removeFromGame();
		} else {
			updateGameFile();
		}
	}
	
	/**
	 * Update the local game file with the new health
	 */
	private void updateGameFile() {
		try {
			final JSONObject gameJSON = GameContextHolder.getInstance().gameJSON;

			int elementIndex = -1;
			
			if(this instanceof NPC) {
				elementIndex = GameContextHolder.getInstance().game.npcs.indexOf(this);
				gameJSON.getJSONArray("npcs").getJSONObject(elementIndex).put("healt", health);
			}
			
			if(this instanceof Character) {
				final String playerKey = ((Character) this).player.userId.equals(gameJSON.getJSONObject("player1").get("userId")) ? "player1" : "player2";
				elementIndex = ((Character) this).player.characters.indexOf(this);
				gameJSON.getJSONObject(playerKey).getJSONArray("characters").getJSONObject(elementIndex).put("health", health);
			}
			
			if(this instanceof Generator) {
				final String playerKey = ((Generator) this).player.userId.equals(gameJSON.getJSONObject("player1").get("userId")) ? "player1" : "player2";
				elementIndex = ((Generator) this).player.generators.indexOf(this);
				gameJSON.getJSONObject(playerKey).getJSONArray("generators").getJSONObject(elementIndex).put("health", health);
			}
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error updating element health: " + e.getMessage());
		}
	}
}
