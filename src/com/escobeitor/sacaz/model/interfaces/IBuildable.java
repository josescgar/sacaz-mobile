package com.escobeitor.sacaz.model.interfaces;

import com.escobeitor.sacaz.enums.EElement;

/**
 * Entities that can be created by a user in a game must
 * implement this interface
 * @author Escobeitor
 *
 */
public interface IBuildable {
	
	/**
	 * Resets the object's properties
	 * @param element
	 */
	void resetElement(EElement element);
}
