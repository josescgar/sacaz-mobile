package com.escobeitor.sacaz.model.interfaces;

/**
 * Entities that can be targeted in a game must implement
 * this interface
 * @author Escobeitor
 *
 */
public interface ITargetable {
	
	/**
	 * Remove a targetable element from the game
	 */
	void removeFromGame();
	
	/**
	 * Decreases the health of the element
	 * @param amount
	 */
	void decreaseHealth(int amount);
}
