package com.escobeitor.sacaz.model;

import java.util.Date;
import java.util.List;

import com.escobeitor.sacaz.enums.EElement;
import com.escobeitor.sacaz.enums.EGameResult;
import com.escobeitor.sacaz.enums.ELevel;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.hud.SacazHUD;
import com.escobeitor.sacaz.logging.Log;
import com.escobeitor.sacaz.manager.ResourceManager;
import com.escobeitor.sacaz.model.factory.WalkerFactory;
import com.escobeitor.sacaz.model.game.Character;
import com.escobeitor.sacaz.model.game.Generator;
import com.escobeitor.sacaz.model.game.NPC;
import com.escobeitor.sacaz.model.game.Player;
import com.escobeitor.sacaz.model.game.Position;
import com.google.gson.annotations.Expose;

/**
 * An undergoing game
 * @author Escobeitor
 *
 */
public class Game {
	
	/**
	 * Game unique id
	 */
	public String id;
	
	/**
	 * Game title
	 */
	public String title;
	
	/**
	 * Current turn owner's
	 */
	@Expose
	public String currentTurn;
	
	/**
	 * Number of turns passed
	 */
	@Expose
	public int turns;
	
	/**
	 * Name of the game map
	 */
	public ELevel map;
	
	/**
	 * Number of concurrent NPCs
	 */
	@Expose
	public int numNPC;
	
	/**
	 * Players holding the game
	 */
	public Player[] players;
	
	/**
	 * Game log
	 */
	public Log gameLog;
	
	/**
	 * Game creation date
	 */
	public Date creationDate;
	
	/**
	 * Last turn finished date
	 */
	@Expose
	public Date lastTurnDate;
	
	/**
	 * Game NPCs
	 */
	public List<NPC> npcs;
	
	/**
	 * Current selected character in the game
	 */
	public Character selectedCharacter;
	
	/**
	 * Finish the current game and register the
	 * result for the local player
	 * @param result
	 */
	public void finishGame(EGameResult result) {
		//TODO
		
	}
	
	/**
	 * Finish the current turn
	 */
	public void finishTurn() {
		try {
			if(selectedCharacter != null) {
				selectedCharacter.detachGrid();
			}
			
			currentTurn = players[0].userId.equals(currentTurn) ? players[1].userId : players[0].userId;
			
			((SacazHUD) GameContextHolder.getInstance().gameLevelScene.gameHud).updateTurnControls();
			
			addZombie();
			moveZombies();
			
			turns++;
			lastTurnDate = new Date();
	
			GameContextHolder.getInstance().game.gameLog.addTurnFinished();
			GameContextHolder.getInstance().game.gameLog.finish();
			
		} catch(Exception e) {
			android.util.Log.e(GameConfiguration.TAG_ERROR, "Error finishing turn: " + e.getMessage());
		}
	}
	
	/**
	 * Move available zombies
	 */
	public void moveZombies() {
		for(int i = 0; i < npcs.size(); i++) {
			npcs.get(i).moveTowardsTarget(i);
		}
	}
	
	/**
	 * Start a new turn
	 */
	public void startTurn() {
		try {
			
			//Unlock HUD controls
			((SacazHUD) GameContextHolder.getInstance().gameLevelScene.gameHud).updateTurnControls();

			//Increase AP and resources
			final Player currentPlayer = getCurrentTurnPlayer();
			currentPlayer.increaseResources(Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.AP_PER_TURN_KEY)), 
					Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.ENERGY_PER_TURN_KEY)),
					Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.FOOD_PER_TURN_KEY)), 
					Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.MATERIALS_PER_TURN_KEY)),
					Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.WATER_PER_TURN_KEY)));
			
			gameLog.updateGameJSONResources(currentPlayer);
			
		} catch(Exception e) {
			android.util.Log.e(GameConfiguration.TAG_ERROR, "Error starting turn: " + e.getMessage());
		}
	}
	
	/**
	 * Spawn a new NPC
	 */
	private void addZombie() {
		if(numNPC == Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.MAX_NPC_KEY))) {
			return;
		}
		
		try {
			
			NPC npc = (NPC) WalkerFactory.getInstance().createElement(EElement.getRandomNPC());
			npc.setAnchorCenter(0, 0);
			final Position pos = getRandomPosition();
			npc.setPosition(pos.x, pos.y);
			npc.position = pos;
			
			GameContextHolder.getInstance().game.gameLog.addNewNPC(npc);
			npcs.add(npc);
			numNPC++;
			ResourceManager.gameMap.attachChild(npc);
			
		} catch(Exception e) {
			return;
		}
	}
	
	/**
	 * Get the nearest character to the NPC
	 */
	public Character getNearestCharacter(NPC zombie) {
		Character target = null;
		float minDistance = ResourceManager.getInstance().cameraWidth;
		float currentDistance;
		Character aux = null;
		
		for(int j = 0; j < players.length; j++) {
			for(int i = 0; i < players[j].characters.size(); i++) {
				aux = players[j].characters.get(i);
				currentDistance = zombie.position.getDistance(aux.position);
				if(currentDistance < minDistance) {
					minDistance = currentDistance;
					target = aux;
				}
			}
		}

		return target;
	}
	
	/**
	 * Find a character object by its ID
	 * @param characterId
	 * @return
	 */
	public Character findCharacterById(String characterId) {
		for(int j = 0; j < this.players.length; j++) {
			for(int i = 0; i < this.players[j].characters.size(); i++) {
				if(this.players[j].characters.get(i).id.equals(characterId)) {
					return this.players[j].characters.get(i);
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Find a generator object by its ID
	 * @param generatorId
	 * @return
	 */
	public Generator findGeneratorById(String generatorId) {
		for(int j = 0; j < this.players.length; j++) {
			for(int i = 0; i < this.players[j].generators.size(); i++) {
				if(this.players[j].generators.get(i).id.equals(generatorId)) {
					return this.players[j].generators.get(i);
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Find a NPC by its ID
	 * @param npcId
	 * @return
	 */
	public NPC findNPCById(String npcId) {
		for(int i = 0; i < npcs.size(); i++) {
			if(npcs.get(i).id.equals(npcId)) {
				return npcs.get(i);
			}
		}
		
		return null;
	}
	
	/**
	 * Returns the username of the current turn owner
	 * @return
	 */
	public Player getCurrentTurnPlayer() {
		for(int i = 0; i < players.length; i++) {
			if(players[i].userId.equals(currentTurn)) {
				return players[i];
			}
		}
		
		return null;
	}
	
	/**
	 * Get local Player instance
	 * @return
	 */
	public Player getLocalPlayer() {
		final String localId = GameContextHolder.getInstance().user.id;
		
		return getPlayerById(localId);
	}
	
	/**
	 * Finds a player by its ID
	 * @param userId
	 * @return
	 */
	public Player getPlayerById(String userId) {
		
		for(int i = 0; i < players.length; i++) {
			if(players[i].userId.equals(userId)) {
				return players[i];
			}
		}
		
		return null;
	}
	
	/**
	 * Checks if the current turn belongs to the local user
	 * @return
	 */
	public boolean isLocalPlayerTurn() {
		return currentTurn.equals(GameContextHolder.getInstance().user.id);
	}
	
	/**
	 * Returns an initial position for a new character
	 * @param p
	 * @return
	 */
	public Position getInitialCharacterPosition(Player p) {
		int playerNumber = 0; 
				
		for(int i = 0; i < players.length; i++) {
			if(players[i].userId.equals(p.userId)) {
				playerNumber = i;
			}
		}
		
		final int posX = playerNumber == 0 ? 160 : 608;
		final int posY = 0 + (int)(Math.random() * ((14 - 0) + 1));
		
		return new Position(posX, posY * GameConfiguration.TILE_DIMENSION);
	}
	
	/**
	 * Get a random position within the map
	 * @return
	 */
	public Position getRandomPosition() {
		final int posX = 5 + (int)(Math.random() * ((19 - 5) + 1));
		final int posY = 0 + (int)(Math.random() * ((14 - 0) + 1));
		
		return new Position(posX * GameConfiguration.TILE_DIMENSION, posY * GameConfiguration.TILE_DIMENSION);
	}
}
