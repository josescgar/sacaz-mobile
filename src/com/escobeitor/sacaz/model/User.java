package com.escobeitor.sacaz.model;

import java.util.ArrayList;

/**
 * Global game system user
 * @author Escobeitor
 *
 */
public class User {
	
	/**
	 * User unique id
	 */
	public String id;
	
	/**
	 * Email address
	 */
	public String email;
	
	/**
	 * Number of lost games
	 */
	public int lost;
	
	/**
	 * Number of resigned games
	 */
	public int resigned;
	
	/**
	 * Username within the game
	 */
	public String username;
	
	/**
	 * Number of won games
	 */
	public int won;
	
	/**
	 * Lobbies joined (created or not)
	 */
	public ArrayList<Lobby> lobbies;
}
