package com.escobeitor.sacaz.model;

import java.util.List;

/**
 * Game lobby. Used to create a new game and find opponents.
 * @author Escobeitor
 *
 */
public class Lobby {
	
	/**
	 * Lobby unique id
	 */
	public String id;
	
	/**
	 * Descriptive title. Used to identify a game.
	 */
	public String title;
	
	/**
	 * Is there available room
	 */
	public boolean available;
	
	/**
	 * Game started by the lobby
	 */
	public Game game;
	
	/**
	 * Lobby creator's user id
	 */
	public String creator;
	
	/**
	 * Users who joined the lobby
	 */
	public User[] users;
	
	/**
	 * Resumes a started game
	 */
	public void resumeGame() {
		//TODO
	}
	
	/**
	 * Starts the game
	 */
	public void startGame() {
		//TODO
	}
}
