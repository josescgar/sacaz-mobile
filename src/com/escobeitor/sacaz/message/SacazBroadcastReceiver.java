package com.escobeitor.sacaz.message;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

/**
 * SACAZ GCM messages receiver
 * @author Escobeitor
 *
 */
public class SacazBroadcastReceiver extends BroadcastReceiver {

	/**
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		//Specify the handler class to handle the intent
		ComponentName comp = new ComponentName(context.getPackageName(), SacazIntentService.class.getName());
		context.startService(intent.setComponent(comp));
		setResultCode(Activity.RESULT_OK);
	}

}
