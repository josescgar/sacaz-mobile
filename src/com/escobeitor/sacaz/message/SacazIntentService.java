package com.escobeitor.sacaz.message;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.activity.GameActivity;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.manager.SceneManager;
import com.escobeitor.sacaz.network.service.LogSvc;
import com.escobeitor.sacaz.scene.GameLevel;
import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Handle SACAZ GCM Server messages
 * @author Escobeitor
 *
 */
public class SacazIntentService extends IntentService {
	
	/**
	 * Notification ID
	 */
	public static final int NOTIFICATION_ID = 1;

	/**
	 * Constructor
	 * @param name
	 */
	public SacazIntentService() {
		super("SacazIntentService");
	}

	/**
	 * @see android.app.IntentService#onHandleIntent(android.content.Intent)
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		String msgType = gcm.getMessageType(intent);
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		boolean allowNotifications = prefs.getBoolean(GameConfiguration.SHARED_PREFS_ALLOW_NOTIFICATIONS_KEY, true);
		
		//SACAZ Message received
		if(!extras.isEmpty() && GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(msgType) && allowNotifications) {
			
			Log.d(GameConfiguration.TAG_DEBUG, "GCM Message received: " + extras.toString());
			String notificationType = extras.getString(GameConfiguration.GCM_MESSAGE_TYPE);
			
			//Lobby related message
			if(GameConfiguration.GCM_MESSAGE_TYPE_LOBBY.equals(notificationType)) {
				Intent throwIntent = new Intent(this, GameActivity.class);
				throwIntent.putExtras(extras);
				throwIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				
				showNotification(extras.getString(GameConfiguration.GCM_MESSAGE_TEXT_KEY), 
						extras.getString(GameConfiguration.GCM_MESSAGE_MESSAGE_LOBBY_ID), 
						!prefs.getBoolean(GameConfiguration.SHARED_PREFS_MUTE_SOUND_KEY, false), 
						throwIntent);
			}
			
			//Game related message
			if(GameConfiguration.GCM_MESSAGE_TYPE_GAME.equals(notificationType)) {
				
				//If it is a turn start notification, check if the game is running and process it right now
				if(extras.getString(GameConfiguration.GCM_MESSAGE_ACTION_KEY).equals(GameConfiguration.GCM_MESSAGE_ACTION_TURN_START)) {
					
					final String gameId = extras.getString(GameConfiguration.GCM_MESSAGE_MESSAGE_GAME_ID);
					
					if(SceneManager.getInstance().currentScene instanceof GameLevel 
							&& gameId.equals(GameContextHolder.getInstance().game.id)) {
						
						LogSvc.getInstance().retrieveLog(gameId, GameContextHolder.getInstance().gameLevelScene, "retrieveLogCallback");
						return;
					}
				}
				
				showNotification(extras.getString(GameConfiguration.GCM_MESSAGE_TEXT_KEY), 
						extras.getString(GameConfiguration.GCM_MESSAGE_MESSAGE_GAME_ID), 
						!prefs.getBoolean(GameConfiguration.SHARED_PREFS_MUTE_SOUND_KEY, false), 
						null);
			}
		}
	}

	/**
	 * Show notification in task bar
	 * @param extras
	 * @param prefs
	 */
	private void showNotification(String message, String tag, boolean playSound, Intent intent) {
		NotificationManager manager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
			.setSmallIcon(R.drawable.ic_launcher)
			.setContentTitle(this.getString(R.string.gcm_notification_title))
			.setStyle(new NotificationCompat.BigTextStyle().bigText(message))
			.setContentText(message);
		
		if(playSound) {
			builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
		}
		
		if(intent != null) {
			builder.setContentIntent(PendingIntent.getActivity(this, 0, intent, Intent.FLAG_ACTIVITY_SINGLE_TOP));
		}
		
		manager.notify(tag, NOTIFICATION_ID, builder.build());
	}

}
