package com.escobeitor.sacaz.layer;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.util.adt.color.Color;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.manager.ResourceManager;
import com.escobeitor.sacaz.manager.SceneManager;
import com.escobeitor.sacaz.manager.managed.ManagedLayer;
import com.escobeitor.sacaz.scene.GameLevel;
import com.escobeitor.sacaz.scene.MyGames;

/**
 * Represents the configurable options
 * available throughout the whole game.
 * Singleton.
 * @author Modified by Escobeitor
 * @author Originally created by Brian Broyles - IFL Game Studio
 */
public class OptionsLayer extends ManagedLayer {
	
	/**
	 * Singleton instance
	 */
	private static OptionsLayer instance = null;
	
	/**
	 * Make constructor private
	 */
	private OptionsLayer() {}
	
	/**
	 * Exit game option
	 */
	private Text optionExitGame;
	
	/**
	 * Singleton method
	 * @return
	 */
	public static OptionsLayer getInstance() {
		if(instance == null) {
			instance = new OptionsLayer();
		}
		
		return instance;
	}

	/**
	 * Add an "slide from the top" effect when the
	 * layer is brought to the front
	 */
	IUpdateHandler slideIn = new IUpdateHandler() {
		@Override
		public void onUpdate(float pSecondsElapsed) {
			if(OptionsLayer.getInstance().getY() > ResourceManager.getInstance().cameraHeight / 2f) {
				OptionsLayer.getInstance().setPosition(OptionsLayer.getInstance().getX(), Math.max(OptionsLayer.getInstance().getY() - (3600 * (pSecondsElapsed)),ResourceManager.getInstance().cameraHeight / 2f));
			} else {
				OptionsLayer.getInstance().unregisterUpdateHandler(this);
			}
		}
		@Override public void reset() {}
	};
	
	/**
	 * Add an "slide out to the bottom" effect when the
	 * layer is removed from the scene
	 */
	IUpdateHandler slideOut = new IUpdateHandler() {
		@Override
		public void onUpdate(float pSecondsElapsed) {
			if(OptionsLayer.getInstance().getY() < ResourceManager.getInstance().cameraHeight / 2f + 480f) {
				OptionsLayer.getInstance().setPosition(OptionsLayer.getInstance().getX(), Math.min(OptionsLayer.getInstance().getY() + (3600 * (pSecondsElapsed)),ResourceManager.getInstance().cameraHeight / 2f + 480f));
			} else {
				OptionsLayer.getInstance().unregisterUpdateHandler(this);
				SceneManager.getInstance().hideLayer();
			}
		}
		@Override public void reset() {}
	};
	
	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedLayer#onLoadLayer()
	 */
	@Override
	public void onLoadLayer() {
		Rectangle optionsContainer = new Rectangle(0, 0, 500, 400,
				ResourceManager.getInstance().engine.getVertexBufferObjectManager());
		
		optionsContainer.setColor(Color.BLACK);
		optionsContainer.setAlpha(0.75f);
		this.attachChild(optionsContainer);
		
		float optionsStep = 60;
		float initialHeight = optionsContainer.getHeight() - 30;
		
		Text optionsTitle = new Text(optionsContainer.getWidth() / 2, initialHeight,
				ResourceManager.getInstance().defaultFont60White,
				AndroidApplicationContextHolder.getInstance().getContext().getText(R.string.main_menu_options),
				ResourceManager.getInstance().engine.getVertexBufferObjectManager());
		optionsContainer.attachChild(optionsTitle);
		initialHeight -= optionsStep;
		
		//Exit game option
		optionExitGame = new Text(optionsContainer.getWidth() / 2, initialHeight,
				ResourceManager.getInstance().defaultFont42White,
				AndroidApplicationContextHolder.getInstance().getContext().getText(R.string.options_exit_game),
				ResourceManager.getInstance().engine.getVertexBufferObjectManager()) {

					@Override
					public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
						if(pSceneTouchEvent.isActionDown()) {
							SceneManager.getInstance().isLayerShown = false;
							SceneManager.getInstance().showScene(new MyGames());
						}
						return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
					}
		};
		
		initialHeight -= optionsStep;
		optionsContainer.attachChild(optionExitGame);
		this.registerTouchArea(optionExitGame);
		
		this.setPosition(ResourceManager.getInstance().cameraWidth/2f, ResourceManager.getInstance().cameraHeight/2f+480f);
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedLayer#onShowLayer()
	 */
	@Override
	public void onShowLayer() {
		optionExitGame.setVisible(SceneManager.getInstance().currentScene.getClass().equals(GameLevel.class));
		this.registerUpdateHandler(slideIn);
	}

	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedLayer#onHideLayer()
	 */
	@Override
	public void onHideLayer() {
		this.registerUpdateHandler(slideOut);
	}
	
	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedLayer#onUnloadLayer()
	 */
	@Override
	public void onUnloadLayer() {
		//Do nothing. Keep resources loaded.
	}
}
