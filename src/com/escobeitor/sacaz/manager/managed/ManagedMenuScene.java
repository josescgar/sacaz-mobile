package com.escobeitor.sacaz.manager.managed;

/**
 * Common class to all menu scenes. 
 * Avoid to unload the menu resources when hiding it.
 * @author Originally created by Brian Broyles - IFL Game Studio
 * @author Modified by Escobeitor
 */
public abstract class ManagedMenuScene extends ManagedScene {
	
	/**
	 * Constructor setting a minimum time for the loading screen
	 * @param loadingMinimum
	 */
	public ManagedMenuScene(float loadingMinimum) {
		super(loadingMinimum);
	}
	
	/**
	 * Default constructor
	 */
	public ManagedMenuScene() {}

	/**
	 * Avoid setting the "loaded" flag to false
	 * @see com.escobeitor.sacaz.manager.managed.ManagedScene#onUnloadManagedScene()
	 */
	@Override
	public void onUnloadManagedScene() {
		if(isLoaded) {
			onUnloadScene();
		}
	}

}
