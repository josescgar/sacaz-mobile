package com.escobeitor.sacaz.manager.managed;

import org.andengine.entity.scene.Scene;

import com.escobeitor.sacaz.manager.SceneManager;

/**
 * Game scene wrapper to make it possible for the
 * SceneManager to change scenes. Implements common 
 * behavior for all managed scenes.
 * @author Originally created by Brian Broyles - IFL Game Studio
 * @author Modified by Escobeitor
 */
public abstract class ManagedScene extends Scene {
	
	/**
	 * Flag to indicate if the scene must show a loading screen
	 */
	public final boolean hasLoadingScreen;	
	
	/**
	 * Minimum amount of seconds that the loading screen
	 * must be shown
	 */
	public final float minLoadingScreenTime;
	
	/**
	 * Keep track of how log the loading screen has been visible
	 */
	public float elapsedLoadingScreenTime = 0f;
	
	/**
	 * Flag to indicate if the scene is loaded
	 */
	public boolean isLoaded = false;
	
	// Convenience constructor that disables the loading screen.
	/**
	 * Constructor without loading screen
	 */
	public ManagedScene() {
		this(0f);
	}
	
	/**
	 * Constructor with loading screen
	 * @param minimumTime Minimum amount of showing the loading screen
	 */
	public ManagedScene(final float minimumTime) {
		minLoadingScreenTime = minimumTime;
		hasLoadingScreen = (minLoadingScreenTime > 0f);
	}
	
	/**
	 * Called from the {@link SceneManager} when loading the scene.
	 * It pauses the scene while it is loading and 
	 * starts the scene load operations.
	 */
	public void onLoadManagedScene() {
		if(!isLoaded) {
			onLoadScene();
			isLoaded = true;
			this.setIgnoreUpdate(true);
		}
	}
	
	/**
	 * Called from the {@link SceneManager} when unloading the scene.
	 * It sets the isLoaded flag to false and starts the scene unload operations.
	 */
	public void onUnloadManagedScene() {
		if(isLoaded) {
			isLoaded = false;
			onUnloadScene();
		}
	}
	
	/**
	 * Called from the {@link SceneManager} when the scene has been
	 * loaded and is going to be shown
	 */
	public void onShowManagedScene() {
		this.setIgnoreUpdate(false);
		onShowScene();
	}
	
	/**
	 * Called from the {@link SceneManager} when the scene
	 * is going to be hidden
	 */
	public void onHideManagedScene() {
		this.setIgnoreUpdate(true);
		onHideScene();
	}

	/**
	 * Called by {@link SceneManager} when the "loading"
	 * screen is going to be shown. It creates the scene
	 * @return
	 */
	public abstract Scene onLoadingScreenLoadAndShown();
	
	/**
	 * Called by {@link SceneManager} when the "loading"
	 * screen is going to be hidden
	 */
	public abstract void onLoadingScreenUnloadAndHidden();
	
	/**
	 * Method for creating the actual scene
	 */
	public abstract void onLoadScene();
	
	/**
	 * Called by {@link SceneManager} when the scene
	 * is going to be shown.
	 */
	public abstract void onShowScene();
	
	/**
	 * Called by {@link SceneManager} when the scene
	 * is going to be hidden.
	 */
	public abstract void onHideScene();
	
	/**
	 * Called after the screen has been hidden. 
	 * Performs cleaning operations.
	 */
	public abstract void onUnloadScene();
	
}