package com.escobeitor.sacaz.manager.managed;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.text.Text;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.manager.ResourceManager;

/**
 * Common class to all in-game scenes. 
 * It takes care of creating and handling the Game HUD. 
 * @author Originally created by Brian Broyles - IFL Game Studio
 * @author Modified by Escobeitor
 */
public abstract class ManagedGameScene extends ManagedScene {
	
	/**
	 * Scene game hud instance
	 */
	public HUD gameHud = new HUD();
	
	/**
	 * Reference to self 
	 */
	public ManagedGameScene scene = this;

	/**
	 * Loading scene text message
	 */
	private Text loadingText;
	
	/**
	 * Loading scene
	 */
	private Scene loadingScene;
	
	/**
	 * New instance setting a default minimum loading screen time
	 */
	public ManagedGameScene() {
		this(Float.parseFloat(GameConfiguration.properties.getProperty(GameConfiguration.LOADING_SCREEN_MINIMUM_KEY)));
	};
	
	/**
	 * Creates a new instance and scale its content
	 * @param loadingMinimum
	 */
	public ManagedGameScene(float loadingMinimum) {
		super(loadingMinimum);
		// Setup the touch attributes for the Game Scenes.
		this.setOnSceneTouchListenerBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionMoveEnabled(true);
		// Scale the Game Scenes according to the Camera's scale factor.
		this.setScale(ResourceManager.getInstance().cameraScaleX, ResourceManager.getInstance().cameraScaleY);
		this.setPosition(0, ResourceManager.getInstance().cameraHeight/2f);
		gameHud.setScaleCenter(0f, 0f);
		gameHud.setScale(ResourceManager.getInstance().cameraScaleX,ResourceManager.getInstance().cameraScaleY);
	}

	/**
	 * Creates the loading screen
	 * @see com.escobeitor.sacaz.manager.managed.ManagedScene#onLoadingScreenLoadAndShown()
	 */
	@Override
	public Scene onLoadingScreenLoadAndShown() {
		String message = AndroidApplicationContextHolder.getInstance().getContext().getString(R.string.screen_loading_message);
		ResourceManager.getInstance().defaultFont42White.prepareLetters(message.toCharArray());
		// Setup and return the loading screen.
		loadingScene = new Scene();
		loadingScene.setBackgroundEnabled(true);
		loadingText = new Text(ResourceManager.getInstance().cameraWidth / 2,
				ResourceManager.getInstance().cameraHeight / 2,
				ResourceManager.getInstance().defaultFont42White,
				message,
				ResourceManager.getInstance().engine.getVertexBufferObjectManager());
		loadingText.setPosition(loadingText.getWidth()/2f, ResourceManager.getInstance().cameraHeight-loadingText.getHeight()/2f);
		loadingScene.attachChild(loadingText);
		return loadingScene;
	}

	/**
	 * Unloads the loading screen
	 * @see com.escobeitor.sacaz.manager.managed.ManagedScene#onLoadingScreenUnloadAndHidden()
	 */
	@Override
	public void onLoadingScreenUnloadAndHidden() {
		// detach the loading screen resources.
		loadingText.detachSelf();
		loadingText = null;
		loadingScene = null;
	}
	
	/**
	 * @see com.escobeitor.sacaz.manager.managed.ManagedScene#onLoadScene()
	 */
	@Override
	public void onLoadScene() {
		//TODO Create game HUD, buttons, etc
	}
	
	/**
	 * Set the camera HUD
	 * @see com.escobeitor.sacaz.manager.managed.ManagedScene#onShowScene()
	 */
	@Override
	public void onShowScene() {
		// We want to wait to set the HUD until the scene is shown because otherwise it will appear on top of the loading screen.
		ResourceManager.getInstance().engine.getCamera().setHUD(gameHud);
	}
	
	/**
	 * Remove the camera HUD
	 * @see com.escobeitor.sacaz.manager.managed.ManagedScene#onHideScene()
	 */
	@Override
	public void onHideScene() {
		ResourceManager.getInstance().engine.getCamera().setHUD(null);
	}
	
	/**
	 * Clear scene engine elements
	 * @see com.escobeitor.sacaz.manager.managed.ManagedScene#onUnloadScene()
	 */
	@Override
	public void onUnloadScene() {
		// detach and unload the scene.
		ResourceManager.getInstance().engine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				scene.detachChildren();
				scene.clearEntityModifiers();
				scene.clearTouchAreas();
				scene.clearUpdateHandlers();
		}});
	}
}
