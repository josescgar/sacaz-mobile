package com.escobeitor.sacaz.manager.managed;

import org.andengine.engine.camera.hud.HUD;

import com.escobeitor.sacaz.manager.SceneManager;

/**
 * Common class to all layers. 
 * @author Originally created by Brian Broyles - IFL Game Studio
 * @author Modified by Escobeitor
 */
public abstract class ManagedLayer extends HUD {
	
	/**
	 * Flag to indicate if the layer has been loaded
	 */
	public boolean hasLoaded = false;
	
	/**
	 * Flag to indicate if layer resources must be
	 * unloaded when the layer is hidden
	 */
	public boolean unloadOnHidden;

	/**
	 * Default constructor. Resources are not
	 * unloaded when the layer is hidden.
	 */
	public ManagedLayer() {
		this(false);
	}
	
	/**
	 * Creates a new instance setting the
	 * "unload when hidden" flag
	 * @param unloadOnHide
	 */
	public ManagedLayer(boolean unloadOnHide) {
		unloadOnHidden = unloadOnHide;
		this.setBackgroundEnabled(false);
	}
	
	/**
	 * Called by {@link SceneManager} when the layer is
	 * going to be shown. Ensures that the layer is not paused.
	 */
	public void onShowManagedLayer() {
		if(!hasLoaded) {
			hasLoaded = true;
			onLoadLayer();
		}
		this.setIgnoreUpdate(false);
		onShowLayer();
	}
	
	/**
	 * Called by {@link SceneManager} when the layer is going
	 * to be hidden. Resources are unloaded depending on the flag.
	 */
	public void onHideManagedLayer() {
		this.setIgnoreUpdate(true);
		onHideLayer();
		if(unloadOnHidden) {
			onUnloadLayer();
		}
	}
	
	/**
	 * Creates the layer
	 */
	public abstract void onLoadLayer();
	
	/**
	 * Called before the layer is shown
	 */
	public abstract void onShowLayer();
	
	/**
	 * Called when the layer is going to be hidden
	 */
	public abstract void onHideLayer();
	
	/**
	 * Called if the layer resources must be unloaded
	 */
	public abstract void onUnloadLayer();
}
