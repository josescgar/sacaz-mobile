package com.escobeitor.sacaz.manager;

import java.lang.reflect.Field;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.text.Text;
import org.andengine.extension.tmx.TMXLoader;
import org.andengine.extension.tmx.TMXTiledMap;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.bitmap.BitmapTextureFormat;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.adt.color.Color;

import android.content.Context;
import android.util.Log;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.enums.EElement;
import com.escobeitor.sacaz.exceptions.GameConfigurationInitializationException;
import com.escobeitor.sacaz.global.AndroidApplicationContextHolder;
import com.escobeitor.sacaz.global.GameConfiguration;
import com.escobeitor.sacaz.global.GameContextHolder;
import com.escobeitor.sacaz.model.Game;

/**
 * The ResourceManager loads all textures, sounds and fonts
 * that the game needs in different scenes. Additionaly,
 * it keeps a reference to the game camera and engine.
 * Implements the Singleton design pattern.
 * @author Escobeitor
 *
 */
public class ResourceManager {
	
	/**
	 * Texture suffix added to property names
	 */
	public static final String TEXTURE_SUFFIX = "Texture";
	
	/**
	 * Game camera object
	 */
	public Camera camera;
	
	/**
	 * Game engine
	 */
	public Engine engine;
	
	/**
	 * Game camera width in pixels
	 */
	public float cameraWidth;
	
	/**
	 * Game camera height in pixels
	 */
	public float cameraHeight;
	
	/**
	 * Camera vertical scale applied to match
	 * the current device vertical resolution
	 */
	public float cameraScaleY;
	
	/**
	 * Camera horizontal scale applied to match
	 * the current device horizontal resolution
	 */
	public float cameraScaleX;
	
	/**
	 * Default system font
	 */
	public Font defaultFont42White;
	
	/**
	 * Default system font
	 */
	public Font defaultFont28Black;
	
	/**
	 * Default system font
	 */
	public Font defaultFont28White;
	
	/**
	 * Default system font
	 */
	public Font defaultFont60White;
	
	/**
	 * Game config assets loaded flag
	 */
	private boolean gameConfigAssetsInitialized = false;
	
	/**
	 * Unique instance of the class
	 */
	private static ResourceManager instance = null;
	
	/**
	 * Make constructor private
	 */
	private ResourceManager() {}
	
	/**
	 * Singleton getInstance method
	 * @return
	 */
	public static synchronized ResourceManager getInstance() {
		if(instance == null) {
			instance = new ResourceManager();
		}
		
		return instance;
	}
	
	/**
	 * Initialize all ResourceManager global parameters
	 * @param engine
	 * @param camera
	 * @param scaleY
	 * @param scaleX
	 * @param cameraWidth
	 * @param cameraHeight
	 */
	public void initialize(Engine engine, Camera camera, float scaleY, float scaleX, float cameraWidth, float cameraHeight) {
		this.engine = engine;
		this.camera = camera;
		this.cameraScaleY = scaleY;
		this.cameraScaleX = scaleX;
		this.cameraWidth = cameraWidth;
		this.cameraHeight = cameraHeight;
		this.defaultFont42White = FontFactory.createFromAsset(engine.getFontManager(), 
				engine.getTextureManager(), 
				256, 
				256, 
				AndroidApplicationContextHolder.getInstance().getContext().getAssets(), 
				GameConfiguration.ASSETS_DEFAULT_FONT_PATH, 
				42f, 
				true, 
				Color.WHITE_ABGR_PACKED_INT);
		this.defaultFont28Black = FontFactory.createFromAsset(engine.getFontManager(), 
				engine.getTextureManager(), 
				256, 
				256, 
				AndroidApplicationContextHolder.getInstance().getContext().getAssets(), 
				GameConfiguration.ASSETS_DEFAULT_FONT_PATH, 
				28f, 
				true, 
				Color.BLACK_ABGR_PACKED_INT);
		this.defaultFont28White = FontFactory.createFromAsset(engine.getFontManager(), 
				engine.getTextureManager(), 
				256, 
				256, 
				AndroidApplicationContextHolder.getInstance().getContext().getAssets(), 
				GameConfiguration.ASSETS_DEFAULT_FONT_PATH, 
				28f, 
				true, 
				Color.WHITE_ARGB_PACKED_INT);
		this.defaultFont60White = FontFactory.createFromAsset(engine.getFontManager(), 
				engine.getTextureManager(), 
				256, 
				256, 
				AndroidApplicationContextHolder.getInstance().getContext().getAssets(), 
				GameConfiguration.ASSETS_DEFAULT_FONT_PATH, 
				60f, 
				true, 
				Color.WHITE_ABGR_PACKED_INT);
		this.defaultFont28Black.load();
		this.defaultFont28White.load();
		this.defaultFont42White.load();
		this.defaultFont60White.load();
	}
	
	/**
	 * Read and load game config assets (characters, weapons, etc.)
	 */
	public static void loadGameConfigAssets() {
		if(getInstance().gameConfigAssetsInitialized) {
			return;
		}
		
		try {
			
			getInstance().gameConfigAssetsInitialized = true;
			
			EElement.initializeCharacters();
			EElement.initializeItems();
			EElement.initializeWeapons();
			EElement.initializeNPC();
			
		} catch(GameConfigurationInitializationException e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error initializing game config assets: " + e.getMessage());
			getInstance().gameConfigAssetsInitialized = false;
		}
	}
	
	/////////////////////////////////
	//       GAME RESOURCES       //
	///////////////////////////////
	
	//Menu resources
	private boolean menuLoaded = false;
	public static ITextureRegion menuBackgroundSky;
	public static ITextureRegion menuBackgroundBuildings;
	public static ITextureRegion menuBackgroundLamps;
	public static TiledTextureRegion menuBackgroundZombie;
	
	/**
	 * Load resources for the game menu
	 * @throws ResourcesLoadException
	 */
	public static void loadMenuResources() {
		if(!getInstance().menuLoaded) {
			try {
				final BuildableBitmapTextureAtlas menuBackgroundAtlas = new BuildableBitmapTextureAtlas(getInstance().engine.getTextureManager(), 
						820, 940, BitmapTextureFormat.RGBA_4444, TextureOptions.NEAREST);
				
				menuBackgroundSky = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuBackgroundAtlas, 
						AndroidApplicationContextHolder.getInstance().getContext(), 
						"gfx/menu/background_stars.png");
				
				menuBackgroundBuildings = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuBackgroundAtlas, 
						AndroidApplicationContextHolder.getInstance().getContext(), 
						"gfx/menu/background_city.png");
				
				menuBackgroundLamps = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuBackgroundAtlas, 
						AndroidApplicationContextHolder.getInstance().getContext(), 
						"gfx/menu/background_lamps.png");
				
				menuBackgroundZombie = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(menuBackgroundAtlas,
						AndroidApplicationContextHolder.getInstance().getContext(),
						"gfx/menu/zombie3.png", 3, 4);
				
				menuBackgroundAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
				
				menuBackgroundAtlas.load();
				
				getInstance().menuLoaded = true;
			} catch(Exception e) {
				Log.e(GameConfiguration.TAG_ERROR, "Error loading scene resources: " + e.getMessage());
			}
		}
	}
	
	//HUD
	public static Text hudAPLabel;
	public static Text hudAP;
	public static Text hudTurnLabel;
	public static Text hudTurn;
	public static Text hudFoodLabel;
	public static Text hudFood;
	public static Text hudElectricityLabel;
	public static Text hudElectricity;
	public static Text hudMaterialsLabel;
	public static Text hudMaterials;
	public static Text hudWaterLabel;
	public static Text hudWater;
	public static ITextureRegion hudCreateCharacterButton;
	public static ITextureRegion hudFinishTurnButton;
	public static ITextureRegion hudAttackMeleeButton;
	public static ITextureRegion hudAttackFirearmButton;
	
	/**
	 * Load game HUD resources
	 */
	public static void loadGameHUD() {
		Context context = AndroidApplicationContextHolder.getInstance().getContext();
		
		try {
		
			hudAPLabel = new Text(0, 0, 
					ResourceManager.getInstance().defaultFont28Black, 
					context.getString(R.string.hud_ap_label), 
					ResourceManager.getInstance().engine.getVertexBufferObjectManager());
			
			hudAP = new Text(0, 0, 
					ResourceManager.getInstance().defaultFont28Black, 
					"99999",
					ResourceManager.getInstance().engine.getVertexBufferObjectManager());
			
			hudTurnLabel = new Text(0, 0, 
					ResourceManager.getInstance().defaultFont28Black, 
					context.getString(R.string.hud_turn_label), 
					ResourceManager.getInstance().engine.getVertexBufferObjectManager());
			
			hudTurn = new Text(0, 0, 
					ResourceManager.getInstance().defaultFont28White, 
					"",
					Integer.parseInt(GameConfiguration.properties.getProperty(GameConfiguration.USERNAME_MAX_LENGTH_KEY)),
					ResourceManager.getInstance().engine.getVertexBufferObjectManager());
			
			hudFoodLabel = new Text(0, 0, 
					ResourceManager.getInstance().defaultFont28Black, 
					context.getString(R.string.hud_food_label), 
					ResourceManager.getInstance().engine.getVertexBufferObjectManager());
			
			hudFood = new Text(0, 0, 
					ResourceManager.getInstance().defaultFont28Black, 
					"99999", 
					ResourceManager.getInstance().engine.getVertexBufferObjectManager());
			
			hudElectricityLabel = new Text(0, 0, 
					ResourceManager.getInstance().defaultFont28Black, 
					context.getString(R.string.hud_electricity_label), 
					ResourceManager.getInstance().engine.getVertexBufferObjectManager());
			
			hudElectricity = new Text(0, 0, 
					ResourceManager.getInstance().defaultFont28Black, 
					"99999", 
					ResourceManager.getInstance().engine.getVertexBufferObjectManager());
			
			hudMaterialsLabel = new Text(0, 0, 
					ResourceManager.getInstance().defaultFont28Black, 
					context.getString(R.string.hud_materials_label), 
					ResourceManager.getInstance().engine.getVertexBufferObjectManager());
			
			hudMaterials = new Text(0, 0, 
					ResourceManager.getInstance().defaultFont28Black, 
					"99999", 
					ResourceManager.getInstance().engine.getVertexBufferObjectManager());
			
			hudWaterLabel = new Text(0, 0, 
					ResourceManager.getInstance().defaultFont28Black, 
					context.getString(R.string.hud_water_label), 
					ResourceManager.getInstance().engine.getVertexBufferObjectManager());
			
			hudWater = new Text(0, 0, 
					ResourceManager.getInstance().defaultFont28Black, 
					"99999", 
					ResourceManager.getInstance().engine.getVertexBufferObjectManager());
			
			final BuildableBitmapTextureAtlas hudAtlas = new BuildableBitmapTextureAtlas(getInstance().engine.getTextureManager(), 
					200, 50, BitmapTextureFormat.RGBA_4444, TextureOptions.NEAREST);
			
			hudCreateCharacterButton = BitmapTextureAtlasTextureRegionFactory.createFromAsset(hudAtlas, context, "gfx/CreateCharacterButton.png");
			hudFinishTurnButton = BitmapTextureAtlasTextureRegionFactory.createFromAsset(hudAtlas, context, "gfx/FinishTurnButton.png");
			hudAttackMeleeButton = BitmapTextureAtlasTextureRegionFactory.createFromAsset(hudAtlas, context, "gfx/AttackMeleeButton.png");
			hudAttackFirearmButton = BitmapTextureAtlasTextureRegionFactory.createFromAsset(hudAtlas, context, "gfx/AttackFirearmButton.png");
			
			hudAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
			hudAtlas.load();
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error loading HUD resources: " + e.getMessage());
		}
	}
	
	/**
	 * Unload game HUD resources
	 */
	public static void unloadGameHUD() {
		hudAPLabel = null;
		hudAP = null;
		hudTurnLabel = null;
		hudTurn = null;
		hudFoodLabel = null;
		hudFood = null;
		hudElectricityLabel = null;
		hudElectricity = null;
		hudMaterialsLabel = null;
		hudMaterials = null;
		hudWaterLabel = null;
		hudWater = null;
		hudCreateCharacterButton.getTexture().unload();
		hudCreateCharacterButton = null;
		hudFinishTurnButton.getTexture().unload();
		hudFinishTurnButton = null;
		hudAttackMeleeButton.getTexture().unload();
		hudAttackMeleeButton = null;
		hudAttackFirearmButton.getTexture().unload();
		hudAttackFirearmButton = null;
	}
	
	//Game resources
	public static TMXTiledMap gameMap;
	public static TiledTextureRegion zombiebobTexture;
	public static TiledTextureRegion zombielohanTexture;
	public static TiledTextureRegion zombierobTexture;
	public static TiledTextureRegion zombieanneTexture;
	public static TiledTextureRegion mcclaneTexture;
	public static TiledTextureRegion sammyTexture;
	public static TiledTextureRegion rickTexture;
	public static TiledTextureRegion charlieTexture;
	public static TiledTextureRegion elisabethTexture;
	public static TiledTextureRegion kurtzTexture;
	public static TiledTextureRegion brodyTexture;
	public static TiledTextureRegion clemenceTexture;
	public static TiledTextureRegion mickeyTexture;
	public static TiledTextureRegion richardTexture;
	public static TiledTextureRegion generatorTexture;
	
	/**
	 * Load resources for the game scene
	 */
	public static void loadGameResources() {
		final Context context = AndroidApplicationContextHolder.getInstance().getContext();
		final Game game = GameContextHolder.getInstance().game;
		
		try {
			//LOAD MAP
			final TMXLoader tmxLoader = new TMXLoader(context.getAssets(),
					ResourceManager.getInstance().engine.getTextureManager(), 
					TextureOptions.NEAREST,
					ResourceManager.getInstance().engine.getVertexBufferObjectManager());
			
			StringBuffer mapPath = new StringBuffer("map/").append(game.map.toString().toLowerCase()).append(".tmx");
			
			gameMap = tmxLoader.loadFromAsset(mapPath.toString());
			
			//LOAD CHARACTERS
			final BuildableBitmapTextureAtlas npcAtlas = new BuildableBitmapTextureAtlas(getInstance().engine.getTextureManager(), 
					390, 130, BitmapTextureFormat.RGBA_4444, TextureOptions.NEAREST);
			
			zombiebobTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(npcAtlas, context, "gfx/walker/zombie1.png", 3, 4);
			zombielohanTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(npcAtlas, context, "gfx/walker/zombie2.png", 3, 4);
			zombierobTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(npcAtlas, context, "gfx/walker/zombie3.png", 3, 4);
			zombieanneTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(npcAtlas, context, "gfx/walker/zombie4.png", 3, 4);
			
			npcAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
			npcAtlas.load();
			
			final BuildableBitmapTextureAtlas charactersAtlas = new BuildableBitmapTextureAtlas(getInstance().engine.getTextureManager(), 
					980, 150, BitmapTextureFormat.RGBA_4444, TextureOptions.NEAREST);
			
			mcclaneTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(charactersAtlas, context, "gfx/walker/McClane.png", 3, 4);
			sammyTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(charactersAtlas, context, "gfx/walker/Sammy.png", 3, 4);
			rickTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(charactersAtlas, context, "gfx/walker/Rick.png", 3, 4);
			charlieTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(charactersAtlas, context, "gfx/walker/Charlie.png", 3, 4);
			elisabethTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(charactersAtlas, context, "gfx/walker/Elisabeth.png", 3, 4);
			kurtzTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(charactersAtlas, context, "gfx/walker/Kurtz.png", 3, 4);
			brodyTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(charactersAtlas, context, "gfx/walker/Brody.png", 3, 4);
			clemenceTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(charactersAtlas, context, "gfx/walker/Clemence.png", 3, 4);
			mickeyTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(charactersAtlas, context, "gfx/walker/Mickey.png", 3, 4);
			richardTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(charactersAtlas, context, "gfx/walker/Richard.png", 3, 4);
			
			charactersAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
			charactersAtlas.load();
			
			final BuildableBitmapTextureAtlas itemAtlas = new BuildableBitmapTextureAtlas(getInstance().engine.getTextureManager(), 
					64, 32, BitmapTextureFormat.RGBA_4444, TextureOptions.NEAREST);
			
			generatorTexture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(itemAtlas, context, "gfx/item/generator.png", 2, 1);
			
			itemAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
			itemAtlas.load();
	
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error loading game resources: " + e.getMessage());
		}
	}
	
	/**
	 * Unload game scene resources
	 */
	public static void unloadGameResources() {
		zombiebobTexture.getTexture().unload();
		zombiebobTexture = null;
		
		zombielohanTexture.getTexture().unload();
		zombielohanTexture = null;
		
		zombierobTexture.getTexture().unload();
		zombierobTexture = null;
		
		zombieanneTexture.getTexture().unload();
		zombieanneTexture = null;
		
		mcclaneTexture.getTexture().unload();
		mcclaneTexture = null;
		
		sammyTexture.getTexture().unload();
		sammyTexture = null;
		
		rickTexture.getTexture().unload();
		rickTexture = null;
		
		charlieTexture.getTexture().unload();
		charlieTexture = null;
		
		elisabethTexture.getTexture().unload();
		elisabethTexture = null;
		
		kurtzTexture.getTexture().unload();
		kurtzTexture = null;
		
		brodyTexture.getTexture().unload();
		brodyTexture = null;
		
		clemenceTexture.getTexture().unload();
		clemenceTexture = null;
		
		mickeyTexture.getTexture().unload();
		mickeyTexture = null;
		
		richardTexture.getTexture().unload();
		richardTexture = null;
		
		generatorTexture.getTexture().unload();
		generatorTexture = null;
		
		System.gc();
	}
	
	/**
	 * Returns the associated texture field for the given element
	 * @param elementName
	 * @return
	 */
	public static ITextureRegion getTextureForElement(String elementName) {
		try {
			StringBuffer spriteName = new StringBuffer(elementName.toLowerCase()).append(ResourceManager.TEXTURE_SUFFIX);
			Field f = ResourceManager.class.getField(spriteName.toString());
			return (ITextureRegion) f.get(ResourceManager.getInstance());
		} catch (Exception e) {
			return null;
		}
	}
}
