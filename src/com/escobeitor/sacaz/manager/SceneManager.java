package com.escobeitor.sacaz.manager;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.scene.Scene;

import com.escobeitor.sacaz.layer.OptionsLayer;
import com.escobeitor.sacaz.manager.managed.ManagedLayer;
import com.escobeitor.sacaz.manager.managed.ManagedScene;
import com.escobeitor.sacaz.scene.MainMenu;

/**
 * Scene manager class. This class is in charge
 * of switching scenes (showing a loading screen if set)
 * and performing all necessary actions for achiving this. Singleton.
 * @author Originally created by Brian Broyles - IFL Game Studio
 * @author Modified by Escobeitor
 */
public class SceneManager {
	
	/**
	 * SeceneManager unique instance
	 */
	private static SceneManager instance = null;
			
	/**
	 * Make constructor private
	 */
	private SceneManager() {}

	/**
	 * Singleton method
	 * @return instance
	 */
	public static SceneManager getInstance() {
		if(instance == null) {
			instance = new SceneManager();
		}
		
		return instance;
	}

	/**
	 * Current scene being shown
	 */
	public ManagedScene currentScene;
	
	/**
	 * Scene to be shown
	 */
	private ManagedScene nextScene;

	/**
	 * Game engine
	 */
	private Engine engine = ResourceManager.getInstance().engine;
	
	/**
	 * Number of frames passed since the loading screen was showed
	 */
	private int numFramesPassed = -1;
		
	/**
	 * Flag to avoid registering several update handlers
	 */
	private boolean loadingScreenHandlerRegistered = false;
	

	/**
	 * When showing a layer, set the flag to remember if the camera
	 * had a previously existing HUD
	 */
	private boolean cameraHadHud = false;
	
	/**
	 * Flag to know if there is a layer being shown in the screen 
	 */
	public boolean isLayerShown = false;
	
	/**
	 * Empty placeholder to show modal-like properties in the current scene
	 */
	private Scene placeholderModalScreen;
	
	/**
	 * Current layer in case there is one
	 */
	public ManagedLayer currentLayer;
	
	/**
	 * Handler to show the loading screen before showing next scene
	 */
	private IUpdateHandler loadingScreenHandler = new IUpdateHandler() {
		@Override
		public void onUpdate(float pSecondsElapsed) {
			// Increment the mNumFamesPassed
			numFramesPassed++;
			
			// And increment the amount of time that the loading screen has been visible.
			nextScene.elapsedLoadingScreenTime += pSecondsElapsed;
			// On the first frame AFTER the loading screen has been shown.
			if(numFramesPassed == 1) {
				// Hide and unload the previous scene if it exists.
				if(currentScene != null) {
					currentScene.onHideManagedScene();
					currentScene.onUnloadManagedScene();
				}
				// Load the new scene.
				nextScene.onLoadManagedScene();
			}
			// On the first frame AFTER the scene has been completely loaded and the loading screen has been shown for its minimum limit.
			if(numFramesPassed > 1 && nextScene.elapsedLoadingScreenTime >= nextScene.minLoadingScreenTime) {
				// Remove the loading screen that was set as a child in the showScene() method.
				nextScene.clearChildScene();
				// Tell the new scene to unload its loading screen.
				nextScene.onLoadingScreenUnloadAndHidden();
				// Tell the new scene that it is shown.
				nextScene.onShowManagedScene();
				// Set the new scene to the current scene.
				currentScene = nextScene;
				// Reset the handler & loading screen variables to be ready for another use.
				nextScene.elapsedLoadingScreenTime = 0f;
				numFramesPassed = -1;
				engine.unregisterUpdateHandler(this);
				loadingScreenHandlerRegistered = false;
			}
		}
		@Override public void reset() {}
	};
	
	/**
	 * Method to switch between two scenes
	 * (the current and the one received as parameter)
	 * @param newScene
	 */
	public void showScene(final ManagedScene newScene) {
		// Reset the camera. This is automatically overridden by any calls to alter the camera from within a managed scene's onShowScene() method.
		engine.getCamera().set(0f, 0f, ResourceManager.getInstance().cameraWidth, ResourceManager.getInstance().cameraHeight);
		// If the new managed scene has a loading screen.
		if(newScene.hasLoadingScreen) {
			// Set the loading screen as a modal child to the new managed scene.
			newScene.setChildScene(newScene.onLoadingScreenLoadAndShown(),true,true,true);
			// This if/else block assures that the LoadingScreen Update Handler is only registered if necessary.
			if(loadingScreenHandlerRegistered){
				numFramesPassed = -1;
				nextScene.clearChildScene();
				nextScene.onLoadingScreenUnloadAndHidden();
			} else {
				engine.registerUpdateHandler(loadingScreenHandler);
				loadingScreenHandlerRegistered = true;
			}
			// Set pManagedScene to mNextScene which is used by the loading screen update handler.
			nextScene = newScene;
			// Set the new scene as the engine's scene.
			engine.setScene(newScene);
			// Exit the method and let the LoadingScreen Update Handler finish the switching.
			return;
		}
		// If the new managed scene does not have a loading screen.
		// Set pManagedScene to mNextScene and apply the new scene to the engine.
		nextScene = newScene;
		engine.setScene(nextScene);
		// If a previous managed scene exists, hide and unload it.
		if(currentScene!=null)
		{
			currentScene.onHideManagedScene();
			currentScene.onUnloadManagedScene();
		}
		// Load and show the new managed scene, and set it as the current scene.
		nextScene.onLoadManagedScene();
		nextScene.onShowManagedScene();
		currentScene = nextScene;
	}
	
	/**
	 * Method to show the main menu of the game
	 */
	public void showMainMenu() {
		showScene(MainMenu.getInstance());
	}

	/**
	 * Method to show the option layers
	 * @param suspendUpdates
	 */
	public void showOptionsLayer(final boolean suspendUpdates) {
		showLayer(OptionsLayer.getInstance(),false,suspendUpdates,true);
	}

	/**
	 * Method to show a new layer
	 * @param newLayer
	 * @param suspendDrawing
	 * @param suspendUpdates
	 * @param suspendTouchEvents
	 */
	public void showLayer(final ManagedLayer newLayer, final boolean suspendDrawing, final boolean suspendUpdates, final boolean suspendTouchEvents) {
		// If the camera already has a HUD, we will use it.
		if(engine.getCamera().hasHUD()){
			cameraHadHud = true;
		} else {
			// Otherwise, we will create one to use.
			cameraHadHud = false;
			HUD placeholderHud = new HUD();
			engine.getCamera().setHUD(placeholderHud);
		}
		// If the managed layer needs modal properties, set them.
		if(suspendDrawing || suspendUpdates || suspendTouchEvents) {
			// Apply the managed layer directly to the Camera's HUD
			engine.getCamera().getHUD().setChildScene(newLayer, suspendDrawing, suspendUpdates, suspendTouchEvents);
			// Create the place-holder scene if it needs to be created.
			if(placeholderModalScreen == null) {
				placeholderModalScreen = new Scene();
				placeholderModalScreen.setBackgroundEnabled(false);
			}
			// Apply the place-holder to the current scene.
			currentScene.setChildScene(placeholderModalScreen, suspendDrawing, suspendUpdates, suspendTouchEvents);
		} else {
			// If the managed layer does not need to be modal, simply set it to the HUD.
			engine.getCamera().getHUD().setChildScene(newLayer);
		}
		// Set the camera for the managed layer so that it binds to the camera if the camera is moved/scaled/rotated.
		newLayer.setCamera(engine.getCamera());
		// Scale the layer according to screen size.
		newLayer.setScale(ResourceManager.getInstance().cameraScaleX, ResourceManager.getInstance().cameraScaleY);
		// Let the layer know that it is being shown.
		newLayer.onShowManagedLayer();
		// Reflect that a layer is shown.
		isLayerShown = true;
		// Set the current layer to pLayer.
		currentLayer = newLayer;
	}

	/**
	 * Hides the current layer
	 */
	public void hideLayer() {
		if(isLayerShown) {
			// Clear the HUD's child scene to remove modal properties.
			engine.getCamera().getHUD().clearChildScene();
			// If we had to use a place-holder scene, clear it.
			if(currentScene.hasChildScene())
				if(currentScene.getChildScene() == placeholderModalScreen)
					currentScene.clearChildScene();
			// If the camera did not have a HUD before we showed the layer, remove the place-holder HUD.
			if(!cameraHadHud)
				engine.getCamera().setHUD(null);
			// Reflect that a layer is no longer shown.
			isLayerShown = false;
			// Remove the reference to the layer.
			currentLayer = null;
		}
	}
}
