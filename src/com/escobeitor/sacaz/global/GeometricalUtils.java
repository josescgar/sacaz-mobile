package com.escobeitor.sacaz.global;

import java.util.ArrayList;
import java.util.List;

import org.andengine.entity.primitive.DrawMode;
import org.andengine.entity.primitive.Mesh;
import org.andengine.util.adt.array.ArrayUtils;

import com.escobeitor.sacaz.manager.ResourceManager;
import com.escobeitor.sacaz.model.game.Position;

/**
 * Utility class for creating complex
 * geometrical figures
 * @author Escobeitor
 *
 */
public class GeometricalUtils {
	
	/**
	 * Returns a filled cross to represent possible tiles
	 * where the character can move. The method assumes that the
	 * current position is the bottom left corner of the current tile (0,0)
	 * and that the tile size if divisible by 2.
	 * @param currentPosition Current character position in scene coordinates
	 * @param maxMovement Number of tiles able to move
	 * @return
	 */
	public static Mesh createMovementGrid(Position currentPosition, int maxMovement) {
		
		maxMovement = Math.min(maxMovement, GameConfiguration.MAX_TILE_MOVEMENT);
		
		int tileDimension = GameConfiguration.TILE_DIMENSION;
		
		int initialX = (int) (currentPosition.x - (maxMovement * tileDimension));
		int initialY = (int) currentPosition.y;
		
		int minX = initialX;
		int maxX = (int) (currentPosition.x + ((maxMovement + 1) * tileDimension));
		
		int minY = initialY - (maxMovement * tileDimension);
		int maxY = initialY + ((maxMovement + 1) * tileDimension);
		
		int currentX = initialX;
		int currentY = initialY;
		
		int directionX = 1;
		int directionY = 1;
		
		List<Float> meshBufferData = new ArrayList<Float>();
		
		int turn = -1; //Flag to indicate if it is turn to increase the X (1) or Y (-1) coordinate
		int nextX;
		int nextY;
		boolean finished = false;
		
		
		while(!finished) {
			meshBufferData.add((float) currentX);
			meshBufferData.add((float) currentY);
			meshBufferData.add((float) 0);
			
			if(turn == 1) {
				nextX = currentX + (tileDimension * directionX);
				if(nextX > maxX || nextX < minX) {
					directionX *= -1;
					nextX = currentX + (tileDimension * directionX);
				}
				
				currentX = nextX;
			}
			
			if(turn == -1) {
				nextY = currentY + (tileDimension * directionY);
				if(nextY > maxY || nextY < minY) {
					directionY *= -1;
					nextY = currentY + (tileDimension * directionY);
				}
				
				currentY = nextY;
			}
			
			turn *= -1;
			finished = currentX == initialX && currentY == initialY;
		}
		
		return new Mesh(0, 0, ArrayUtils.toFloatArray(meshBufferData), meshBufferData.size() / 3, DrawMode.LINE_LOOP, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
	}
}
