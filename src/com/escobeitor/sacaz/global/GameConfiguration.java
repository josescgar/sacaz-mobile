package com.escobeitor.sacaz.global;

import java.util.Properties;

import com.escobeitor.sacaz.exceptions.GameConfigurationInitializationException;

/**
 * Global game constants: field references, action points cost, etc.
 * @author Escobeitor
 *
 */
public class GameConfiguration {
	
	/////////////////////////////
	///  SHARED PREFERENCES  ///
	///////////////////////////
	/**
	 * Shared preferences group name
	 */
	public static final String SHARED_PREFERENCES_KEY = "sacaz_game";
	
	/**
	 * Network Shared preferences group name
	 */
	public static final String SHARED_PREFERENCES_NETWORK_KEY = "sacaz_network";
	
	/**
	 * User's selected account
	 */
	public static final String SHARED_PREFS_USER_ACCOUNT_KEY = "selectedUserAccount";
	
	/**
	 * User's google account authorization key
	 */
	public static final String SHARED_PREFS_AUTH_TOKEN_KEY = "userAuthToken";
	
	/**
	 * Allow SACAZ GCM notifications key
	 */
	public static final String SHARED_PREFS_ALLOW_NOTIFICATIONS_KEY = "allowNotifications";
	
	/**
	 * Mute game sounds key
	 */
	public static final String SHARED_PREFS_MUTE_SOUND_KEY = "muteSound";
	
	/////////////////////////
	///  DEBUGGING TAGS  ///
	///////////////////////
	/**
	 * General debug tag
	 */
	public static final String TAG_DEBUG = "SZ_DEBUG";
	
	/**
	 * General error tag
	 */
	public static final String TAG_ERROR = TAG_DEBUG;
	
	/**
	 * Configuration initialization error tag
	 */
	public static final String TAG_ERROR_CONFIG_INIT = TAG_ERROR;
	
	/**
	 * Network error
	 */
	public static final String TAG_NETWORK_REQUEST_ERROR = TAG_ERROR;
	
	/////////////////////////
	///   GCM MESSAGES   ///
	///////////////////////
	
	/**
	 * GCM message action key
	 */
	public static final String GCM_MESSAGE_ACTION_KEY = "action";
	
	/**
	 * GCM message type key
	 */
	public static final String GCM_MESSAGE_TYPE = "type";
	
	/**
	 * GCM message lobby type key
	 */
	public static final String GCM_MESSAGE_TYPE_LOBBY = "LOBBY";
	
	/**
	 * GCM message game type key
	 */
	public static final String GCM_MESSAGE_TYPE_GAME = "GAME";
	
	/**
	 * GCM message delete action key
	 */
	public static final String GCM_MESSAGE_ACTION_DELETE = "DELETE";
	
	/**
	 * GCM message delete action key
	 */
	public static final String GCM_MESSAGE_ACTION_TURN_START = "TURN_START";
	
	/**
	 * GCM message lobby ID key
	 */
	public static final String GCM_MESSAGE_MESSAGE_LOBBY_ID = "lobbyId";
	
	/**
	 * GCM message game ID key
	 */
	public static final String GCM_MESSAGE_MESSAGE_GAME_ID = "gameId";
	
	/**
	 * GCM message text key
	 */
	public static final String GCM_MESSAGE_TEXT_KEY = "message";	
	
	/////////////////////////////
	///  CONFIG FILES PATHS  ///
	///////////////////////////
	/**
	* Weapons configuration file
	*/
	public static final String ASSETS_CONFIG_WEAPONS_FILE_PATH = "config/weapons.json";
	
	/**
	* Abilities configuration file
	*/
	public static final String ASSETS_CONFIG_ABILITIES_FILE_PATH = "config/abilities.json";
	
	/**
	* Characters configuration file
	*/
	public static final String ASSETS_CONFIG_CHARACTERS_FILE_PATH = "config/characters.json";
	
	/**
	* Items configuration file
	*/
	public static final String ASSETS_CONFIG_ITEMS_FILE_PATH = "config/items.json";
	
	/**
	 * NPC configuration file
	 */
	public static final String ASSETS_CONFIG_NPC_FILE_PATH = "config/npcs.json";
	
	/**
	* Global configuration values file
	*/
	public static final String ASSETS_CONFIG_GLOBAL_FILE_PATH = "config/game.properties";
	
	/**
	 * Network configuration files
	 */
	public static final String ASSETS_CONFIG_NETWORK_FILE_PATH = "config/network.properties";
	
	/**
	 * Path to the application default font
	 */
	public static final String ASSETS_DEFAULT_FONT_PATH = "font/visitor2.ttf";
	
	
	/////////////////////////
	///  ARRAYS INDEXES  ///
	///////////////////////
	/**
	 * Electricity resource array position
	 */
	public static final int RESOURCES_ELECTRICITY = 0;
	
	/**
	 * Food resource array position
	 */
	public static final int RESOURCES_FOOD = 1;
	
	/**
	 * Materials resource array position
	 */
	public static final int RESOURCES_MATERIALS = 2;
	
	/**
	 * Water resource array position
	 */
	public static final int RESOURCES_WATER = 3;
	
	/**
	 * Electricity cost array position
	 */
	public static final int COST_ELECTRICITY = 0;
	
	/**
	 * Food cost array position
	 */
	public static final int COST_FOOD = 1;
	
	/**
	 * Materials cost array position
	 */
	public static final int COST_MATERIALS = 2;
	
	/**
	 * Water cost array position
	 */
	public static final int COST_WATER = 3;
	
	/**
	 * Energy distribution for ranger array position
	 */
	public static final int ENERGY_DISTRIBUTION_GENERATORS = 0;
	
	/**
	 * Energy distribution for character creation array position
	 */
	public static final int ENERGY_DISTRIBUTION_CHARACTERS = 1;
	
	
	/////////////////////////////////
	///  GLOBAL PROPERTIES KEYS  ///
	///////////////////////////////
	/**
	 * Log ActionPool object pool size
	 */
	public static final String ACTION_POOL_SIZE_KEY = "actions_pool_length";
	
	/**
	 * Local log serialized file name suffix
	 */
	public static final String LOCAL_LOG_FILE_SUFFIX = "LOG";
	
	/**
	 * Max concurrent NPCs
	 */
	public static final String MAX_NPC_KEY = "npc_max_number";
	
	/**
	 * Cost in Action Points of creating a new character
	 */
	public static final String COST_AP_CREATE_CHARACTER_KEY = "cost_ap_create_character";
	
	/**
	 * Cost in Action Points of disabling a generator
	 */
	public static final String COST_AP_DISABLE_GENERATOR_KEY = "cost_ap_disable_generator";
	
	/**
	 * Cost in Action Points of changing the energy distribution
	 */
	public static final String COST_AP_ENERGY_DISTRIBUTION_KEY = "cost_ap_energy_distribution";
	
	/**
	 * Cost in Action Points of changing the ranger resources allocation
	 */
	public static final String COST_AP_RANGER_ALLOCATION_KEY = "cost_ap_ranger_allocation";
	
	/**
	 * Cost in Action Points of moving a character (base cost)
	 */
	@Deprecated
	public static final String COST_AP_MOVE_CHARACTER_KEY = "cost_ap_move_character";
	
	/**
	 * Cost in Action Points of picking up an object
	 */
	public static final String COST_AP_PICK_UP_KEY = "cost_ap_pick_up";
	
	/**
	 * Cost in Action Points of striking a target
	 */
	public static final String COST_AP_STRIKE_KEY = "cost_ap_strike";
	
	/**
	 * Cost in Action Points of turning the ranger on or off
	 */
	public static final String COST_AP_SWITCH_RANGER_KEY = "cost_ap_switch_ranger";
	
	/**
	 * Action Points obtained by turn
	 */
	public static final String AP_PER_TURN_KEY = "action_points_per_turn";
	
	/**
	 * Energy obtained by turn
	 */
	public static final String ENERGY_PER_TURN_KEY = "energy_per_turn";
	
	/**
	 * Food obtained by turn
	 */
	public static final String FOOD_PER_TURN_KEY = "food_per_turn";
	
	/**
	 * Water obtained by turn
	 */
	public static final String WATER_PER_TURN_KEY = "water_per_turn";
	
	/**
	 * Materials obtained by turn
	 */
	public static final String MATERIALS_PER_TURN_KEY = "materials_per_turn";
	
	/**
	 * Base damage power of a booby trapped NPC
	 */
	public static final String BOOBY_TRAP_BASE_POWER_KEY = "booby_trap_base_power";
	
	/**
	 * Generator default health
	 */
	public static final String GENERATOR_DEFAULT_HEALTH_KEY = "generator_default_health";
	
	/**
	 * Username maximum number of characters
	 */
	public static final String USERNAME_MAX_LENGTH_KEY = "username_max_length";
	
	/**
	 * Username minimum number of characters
	 */
	public static final String USERNAME_MIN_LENGTH_KEY = "username_min_length";
	
	/**
	 * Minimum amount of time that the loading screen will be shown (in seconds)
	 * when changing scenes
	 */
	public static final String LOADING_SCREEN_MINIMUM_KEY = "loading_screen_min_time";

	/**
	 * Minimum number of characters for the game title
	 */
	public static final String GAME_TITLE_MIN_LENGTH_KEY = "game_title_min_length";
	
	/**
	 * Number of action points for NPC movement
	 */
	public static final String NPC_MOVEMENT_AP_KEY = "npc_movement_action_points";
	
	/**
	 * Stored game files extension
	 */
	public static final String GAME_FILE_EXTENSION = ".json";
	
	/**
	 * Game map tile dimension in Pixels
	 */
	public static final int TILE_DIMENSION = 32;

	/**
	 * Max number of tiles that a character can move
	 * in the best case possible
	 */
	public static final int MAX_TILE_MOVEMENT = 15;

	/**
	 * Game properties container
	 */
	public static Properties properties;
	
	/**
	 * Read game properties file
	 * @param constantsFile
	 * @throws GameConfigurationInitializationException
	 */
	public static void initializeConstants() throws GameConfigurationInitializationException {
		try {
			properties = new Properties();
			properties.load(AndroidApplicationContextHolder.getInstance().getContext().getAssets().open(GameConfiguration.ASSETS_CONFIG_GLOBAL_FILE_PATH));
		} catch (Exception e) {
			throw new GameConfigurationInitializationException();
		}
	}
	
}
