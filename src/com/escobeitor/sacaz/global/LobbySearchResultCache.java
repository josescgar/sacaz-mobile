package com.escobeitor.sacaz.global;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

/**
 * Keep the last lobby search result. Singleton.
 * @author Escobeitor
 *
 */
public class LobbySearchResultCache {
	
	/**
	 * Singleton instance
	 */
	private static LobbySearchResultCache instance = null;
	
	/**
	 * Make constructor private
	 */
	private LobbySearchResultCache() {}
	
	/**
	 * Search results
	 */
	public Map<String, JSONObject> searchResult = new HashMap<String, JSONObject>();
	
	/**
	 * User joined or created lobbies
	 */
	public Map<String, JSONObject> myLobbies = new HashMap<String, JSONObject>();
	
	/**
	 * Singleton method
	 * @return
	 */
	public static LobbySearchResultCache getInstance() {
		if(instance == null) {
			instance = new LobbySearchResultCache();
		}
		
		return instance;
	}
}
