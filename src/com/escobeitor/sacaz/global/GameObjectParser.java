package com.escobeitor.sacaz.global;

import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.escobeitor.sacaz.R;
import com.escobeitor.sacaz.activity.GameActivity;
import com.escobeitor.sacaz.enums.EElement;
import com.escobeitor.sacaz.enums.EFaction;
import com.escobeitor.sacaz.enums.ELevel;
import com.escobeitor.sacaz.json.JSONFileUtils;
import com.escobeitor.sacaz.model.Game;
import com.escobeitor.sacaz.model.factory.ItemFactory;
import com.escobeitor.sacaz.model.factory.WalkerFactory;
import com.escobeitor.sacaz.model.factory.WeaponFactory;
import com.escobeitor.sacaz.model.game.Character;
import com.escobeitor.sacaz.model.game.Generator;
import com.escobeitor.sacaz.model.game.Item;
import com.escobeitor.sacaz.model.game.NPC;
import com.escobeitor.sacaz.model.game.Player;
import com.escobeitor.sacaz.model.game.Position;
import com.escobeitor.sacaz.model.game.Ranger;
import com.escobeitor.sacaz.model.game.Warehouse;
import com.escobeitor.sacaz.model.game.Weapon;

/**
 * Utility class for parsing game data from
 * JSON into a Game object
 * @author Escobeitor
 *
 */
public class GameObjectParser {

	/**
	 * Parse a game object into game context
	 * @param filename
	 */
	public static void parseGameObject(String filename) {
		final Context context = AndroidApplicationContextHolder.getInstance().getContext();
		Game game = GameContextHolder.getInstance().game;
		
		try {
			
			JSONObject object = JSONFileUtils.getJSONFromFile(context.openFileInput(filename));
			
			GameContextHolder.getInstance().gameJSON = object;
			
			game.players = new Player[2];
			
			parsePlayer(object.getJSONObject("player1"), 0, game);
			
			parsePlayer(object.getJSONObject("player2"), 1, game);
			
			game.creationDate = new Date(object.getLong("creationDate"));
			game.currentTurn = object.getString("currentTurn");
			game.title = object.getString("title");
			game.lastTurnDate = new Date(object.getLong("lastTurnDate"));
			game.turns = object.getInt("turns");
			game.map = ELevel.valueOf(object.getString("map"));
			game.numNPC = object.getInt("numNPC");
			
			parseNPCs(game, object.getJSONArray("npcs"));
			
		} catch(Exception e) {
			Log.e(GameConfiguration.TAG_ERROR, "Error parsing game object: " + e.getMessage());
			
			//Show error message to the user
			((GameActivity) context).runOnUiThread(new Runnable() {

				@Override
				public void run() {
					AndroidApplicationContextHolder.toastOnUiThread(context.getString(R.string.error_starting_game), Toast.LENGTH_LONG);
				}
				
			});
			
		}
	}

	/**
	 * Parse a Player object
	 * @param object
	 * @param numPlayer
	 * @param game
	 * @throws JSONException 
	 */
	private static void parsePlayer(JSONObject object, int numPlayer, Game game) throws JSONException {
		boolean isCurrentPlayer = object.getString("userId").equals(GameContextHolder.getInstance().user.id);
		
		Player p = new Player();
		p.warehouse = new Warehouse(p);
		p.userId = object.getString("userId");
		p.username = object.getString("username");
		p.faction = EFaction.valueOf(object.getString("faction"));

		if(isCurrentPlayer) {
			p.warehouse.energyDistribution = new double[]{object.getJSONArray("energyDistribution").getDouble(0), object.getJSONArray("energyDistribution").getDouble(1)};
			p.actionPoints = object.getInt("actionPoints");
			
			JSONObject resources = object.getJSONObject("resources");
			p.electricity = resources.getInt("electricity");
			p.food = resources.getInt("food");
			p.materials = resources.getInt("materials");
			p.water = resources.getInt("water");
			
			parsePlayerItems(p, object.getJSONArray("items"));
		}

		parsePlayerCharacters(p, object.getJSONArray("characters"));
		
		parsePlayerGenerators(p, object.getJSONArray("generators"));
		
		p.ranger = new Ranger();
		p.ranger.active = object.getJSONObject("ranger").getBoolean("active");
		
		if(p.ranger.active && isCurrentPlayer) {
			JSONArray allocation = object.getJSONObject("ranger").getJSONArray("allocation");
			p.ranger.allocation = new int[]{allocation.getInt(0), allocation.getInt(1), allocation.getInt(2)};
		}
		
		game.players[numPlayer] = p;
	}

	/**
	 * Parse player generators
	 * @param p
	 * @param generators
	 * @throws JSONException 
	 */
	private static void parsePlayerGenerators(Player p, JSONArray generators) throws JSONException {
		p.generators = new ArrayList<Generator>(generators.length());
		
		for(int i = 0; i < generators.length(); i++) {
			JSONObject generator = generators.getJSONObject(i);
			Generator g = new Generator(p, generator.getString("id"));
			g.position = new Position(generator.getJSONObject("position").getInt("x"), generator.getJSONObject("position").getInt("y"));
			g.health = generator.getInt("health");
			g.player = p;
			
			p.generators.add(g);
		}
	}

	/**
	 * Parse player characters
	 * @param p
	 * @param characters
	 * @throws JSONException 
	 */
	private static void parsePlayerCharacters(Player p, JSONArray characters) throws JSONException {
		p.characters = new ArrayList<Character>();
		
		for(int i = 0 ; i < characters.length(); i++) {
			JSONObject character = characters.getJSONObject(i);
			Character c = (Character) WalkerFactory.getInstance().createElement(EElement.valueOf(character.getString("type")));
			
			c.player = p;
			c.id = character.getString("id");
			c.health = character.getInt("health");
			c.position = new Position(character.getJSONObject("position").getInt("x"), character.getJSONObject("position").getInt("y"));

			c.weapons = new ArrayList<Weapon>();
			JSONArray weapons = character.getJSONArray("weapons");
			for(int j = 0; j < weapons.length(); j++) {
				Weapon weapon = (Weapon) WeaponFactory.getInstance().createElement(EElement.valueOf(weapons.getJSONObject(j).getString("type")));
				weapon.ammo = weapons.getJSONObject(j).getInt("ammo");
				c.weapons.add(weapon);
			}
			
			p.characters.add(c);
		}
	}

	/**
	 * Parse player items
	 * @param p
	 * @param items
	 * @throws JSONException 
	 */
	private static void parsePlayerItems(Player p, JSONArray items) throws JSONException {
		p.warehouse.items = new ArrayList<Item>();
		
		for(int i = 0; i < items.length(); i++) {
			JSONObject JSONitem = items.getJSONObject(i);
			Item item = (Item) ItemFactory.getInstance().createElement(EElement.valueOf(JSONitem.getString("type")));
			
			item.id = JSONitem.getString("id");
			item.numUses = JSONitem.getInt("numUses");
			if(JSONitem.has("ability")) {
				item.ability.numUses = JSONitem.getJSONObject("ability").getInt("left");
			}
			
			p.warehouse.items.add(item);
		}
	}

	/**
	 * Parse game NPCs
	 * @param game
	 * @param npcs
	 * @throws JSONException 
	 */
	private static void parseNPCs(Game game, JSONArray npcs) throws JSONException {
		game.npcs = new ArrayList<NPC>();
		
		for(int i = 0; i < npcs.length(); i++) {
			JSONObject JSONnpc = npcs.getJSONObject(i);
			NPC npc = (NPC) WalkerFactory.getInstance().createElement(EElement.valueOf(JSONnpc.getString("type")));
			
			npc.id = JSONnpc.getString("id");
			npc.position = new Position(JSONnpc.getJSONObject("position").getInt("x"), JSONnpc.getJSONObject("position").getInt("y"));
			npc.boobyTrapped = JSONnpc.getBoolean("boobyTrapped");
			if(JSONnpc.has("target")) {
				npc.target = game.findCharacterById(JSONnpc.getString("target"));
			}
			npc.health = JSONnpc.getInt("health");
			
			game.npcs.add(npc);
		}
	}
}
