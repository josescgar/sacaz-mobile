package com.escobeitor.sacaz.global;

import org.json.JSONObject;

import com.escobeitor.sacaz.model.Game;
import com.escobeitor.sacaz.model.User;
import com.escobeitor.sacaz.scene.GameLevel;

/**
 * Class to store the current game reference, avoiding
 * to store the reference to it in different classes. Singleton.
 * @author Escobeitor
 *
 */
public class GameContextHolder {
	
	/**
	 * Instance
	 */
	private static GameContextHolder instance = null;
	
	/**
	 * Referenced game
	 */
	public Game game;
	
	/**
	 * Current user
	 */
	public User user;
	
	/**
	 * Game file JSON content reference for
	 * updating purposes during gameplay
	 */
	public JSONObject gameJSON;
	
	/**
	 * Referecence to the game level scene
	 */
	public GameLevel gameLevelScene;
	
	/**
	 * Make constructor private
	 */
	private GameContextHolder() {}
	
	/**
	 * Singleton method
	 * @return
	 */
	public static GameContextHolder getInstance() {
		if(instance == null) {
			instance = new GameContextHolder();
		}
		
		return instance;
	}
}
