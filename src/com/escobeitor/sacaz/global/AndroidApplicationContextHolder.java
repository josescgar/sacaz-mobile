package com.escobeitor.sacaz.global;

import org.andengine.ui.activity.BaseActivity;
import org.andengine.util.ActivityUtils;

import com.escobeitor.sacaz.activity.GameActivity;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

/**
 * Keep a reference to the current android application
 * context for using it in several points of the game
 * @author Escobeitor
 *
 */
public class AndroidApplicationContextHolder {
	
	/**
	 * Instance
	 */
	private static AndroidApplicationContextHolder instance = null;
	
	/**
	 * Reference Android application context
	 */
	private Context context;
	
	/**
	 * Make constructor private
	 */
	private AndroidApplicationContextHolder() {}
	
	/**
	 * Singleton method
	 * @return
	 */
	public static AndroidApplicationContextHolder getInstance() {
		if(instance == null) {
			instance = new AndroidApplicationContextHolder();
		}
		
		return instance;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
	
	/**
	 * Generates a TOAST in the UI thread
	 * @param pText
	 * @param pDuration
	 */
	public static void toastOnUiThread(final CharSequence pText, final int pDuration) {
		final GameActivity context = (GameActivity) getInstance().context;
		
		if (ActivityUtils.isOnUiThread()) {
			Toast.makeText(context, pText, pDuration).show();
		} else {
			context.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(context, pText, pDuration).show();
				}
			});
		}
	}
}
